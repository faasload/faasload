#! /usr/bin/env python3

"""Test client for the Docker monitor.

This script is a client for FaaSLoad's Docker monitor that mimics operations done by FaaSLoad's loader during normal
operation.

It can be used in two ways:

1. to fetch resource usage measurements and performance counters from the monitor, for an invocation ID;
2. to fetch the log of Docker operations from the monitor.

It is useful to test the Docker monitor's behavior.

It is also capable of writing to FaaSLoad's database similarly to the loader.
"""

# Copyright Toulouse INP / IRIT / CNRS, 2019 - 2020
# Copyright Télécom SudParis / IP Paris, 2021 - 2025
# Mathieu Bacou and other contributors
#
# mathieu.bacou@telecom-sudparis.eu
#
# This software is a computer program whose purpose is to inject a workload
# to a Function-as-a-Service platform, and to gather data about its execution.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software. You can use,
# modify and/ or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.

import logging
import os
import random
import sys
import time
from abc import abstractmethod
from argparse import ArgumentParser, RawDescriptionHelpFormatter
from datetime import datetime
from textwrap import dedent
from typing import Optional, Any

from configuration_reader import read as read_configuration

from faasload.lib import prepare_and_read_configuration, configure_logging
from faasload.loader import DEFAULTS_OPENWHISK, FaaSLoadGlobalConfiguration, DEFAULTS, FaaSPlatform, \
    FAASPLATFORM_PRETTY_NAMES, DockerMonitorConfiguration, DatabaseConfiguration
from faasload.loader.__main__ import FAASLOAD_PLATFORM_ENVAR
from faasload.loader.activation_monitor import get_resource_usage, get_docker_operations_log
from faasload.loader.database import FaaSLoadDatabase, FaaSLoadDatabaseException

NB_PRINTED_RECORDS = 5


class DockerMonitorClient:
    @abstractmethod
    def get_activation(self, namespace: str, act_id: str) -> Any:
        raise NotImplementedError

    @abstractmethod
    def get_docker_id(self, act_id: str) -> str:
        raise NotImplementedError


class DockerMonitorClientError(Exception):
    pass


class DockerMonitorClientOpenWhisk(DockerMonitorClient):
    from pywhisk.client import (
        APIConfiguration as OpenWhiskConfiguration,
        AdministrationConfiguration as OpenWhiskAdministrationConfiguration)
    from pywhisk.models import Activation

    def __init__(self, cfg: OpenWhiskConfiguration, admin_cfg: OpenWhiskAdministrationConfiguration):
        self.cfg = cfg
        self.admin_cfg = admin_cfg

    def get_docker_id(self, act_id: str) -> str:
        from pywhisk.admin.docker import get_docker_id
        from pywhisk.admin import OpenWhiskAdminException

        try:
            return get_docker_id(act_id, self.admin_cfg)
        except OpenWhiskAdminException as err:
            raise DockerMonitorClientError from err

    def get_activation(self, namespace: str, act_id: str) -> Activation:
        from pywhisk.activation import get
        from pywhisk.client import OpenWhiskException

        try:
            return get(act_id, namespace, self.cfg)
        except OpenWhiskException as err:
            raise DockerMonitorClientError from err


class DockerMonitorClientKnative(DockerMonitorClient):
    from pyknative.models import Activation
    from pyknative.client import ApiConfiguration as KnativeConfiguration

    def __init__(self, cfg: KnativeConfiguration):
        self.cfg = cfg

        # Used as a cache of all invocations
        self.invocations = {}

    def get_docker_id(self, act_id: str) -> str:
        return self.get_activation('default', act_id).docker_id

    def get_activation(self, namespace: str, act_id: str) -> Activation:
        from pyknative.activation import get_executed_activations

        try:
            return self.invocations[act_id]
        except KeyError:
            self.invocations = {act.activation_id: act for act in get_executed_activations(self.cfg)}


def do_measurements(*,
                    mon_cli: DockerMonitorClient,
                    namespace: str,
                    invocation_id: str,
                    docker_mon_cfg: DockerMonitorConfiguration,
                    write_db: bool = False,
                    db_cfg: Optional[DatabaseConfiguration] = None):
    logger = logging.getLogger('main')

    logging.debug('fetching measurements for invocation %s of user %s',
                  invocation_id, namespace)

    try:
        act = mon_cli.get_activation(namespace, invocation_id)
    except DockerMonitorClientError as err:
        if logger.isEnabledFor(logging.DEBUG):
            logger.critical('failed fetching invocation data for invocation %s of user %s', invocation_id, namespace,
                            exc_info=True)
        else:
            logger.critical('failed fetching invocation data for invocation %s of user %s', invocation_id, namespace,
                            err)
        sys.exit(1)
    else:
        logger.info('fetched data about invocation %s of user %s from the FaaS platform',
                    invocation_id, namespace)

    try:
        cont_id = mon_cli.get_docker_id(invocation_id)
    except ValueError as err:
        logger.critical('failed getting container ID of invocation %s of user %s',
                        invocation_id, namespace)
        logger.critical('reason: %s', err)
        sys.exit(1)
    else:
        logger.info('got ID of Docker container for invocation %s of user %s: %s',
                    invocation_id, namespace, cont_id)

    for msrv in docker_mon_cfg["measurementservers"]:
        try:
            meas = get_resource_usage(msrv, cont_id, act.start, act.end)
            logger.info('fetched %d measurements for activation %s of user %s',
                        len(meas), invocation_id, namespace)
            break
        except ConnectionError as err:
            logger.warning('measurement server %s:%s is unreachable: %s', msrv['address'], msrv['port'], err)
            continue
        except ValueError:
            # Not found on this measurement server
            continue
    else:
        logger.critical('failed fetching resource usage measurements of container %s', cont_id)
        logger.critical('reason: the container was not found on any measurement server')
        sys.exit(1)

    if write_db:
        db = FaaSLoadDatabase(db_cfg)

        func = db.select_function(act.name)

        rec_id = db.insert_partial_run({
            'function_id':   func.id,
            'namespace':     namespace,
            'activation_id': invocation_id,
        })

        logger.info('stored partial run #%d for invocation %s of user %s',
                    rec_id, invocation_id, namespace)

        # We cannot get parameters from activation record.
        db.insert_parameters(rec_id, {})

        logger.info('stored empty parameters (this is expected) of invocation %s of user %s',
                    rec_id, invocation_id, namespace)

        try:
            act_error_msg = act.response.result['faasload']['error_msg']
            del act.response.result['faasload']
        except KeyError:
            act_error_msg = None

        db.update_activation(invocation_id, {
            'failed':    not act.response.success,
            'start':     act.start,
            'end':       act.end,
            'wait_time': next((a for a in act.annotations if a['key'] == 'waitTime'), {'value': None})['value'],
            'init_time': next((a for a in act.annotations if a['key'] == 'initTime'), {'value': None})['value'],
            'error_msg': act_error_msg,
        })

        logger.info('completed the partial run #%d for invocation %s of user %s',
                    rec_id, invocation_id, namespace)

        try:
            db.insert_result(invocation_id, act.response.result)
        except FaaSLoadDatabaseException:
            logger.critical('failed storing result of invocation %s of user %s into the database')
        else:
            logger.info('stored result of invocation %s of user %s',
                        invocation_id, namespace)

        try:
            db.insert_resources(act.activationId, meas)
        except FaaSLoadDatabaseException as err:
            if logger.isEnabledFor(logging.DEBUG):
                logger.critical('failed storing measurements of invocation %s of user %s into the database',
                                act.activationId, namespace, exc_info=True)
            else:
                logger.critical('failed storing measurements of invocation %s of user %s into the database',
                                act.activationId, namespace, err)
        else:
            logger.info('stored %d measurements of invocation %s of user %s',
                        len(meas), invocation_id, namespace)

    # And then, do something with the fetched measurements. There are probably a lot of records, so printing them all is
    # a bad idea, and it is better to display a few.
    print(
        f'Fetched measurements for Docker container {cont_id[:12]} between {datetime.fromtimestamp(act.start / 1000)} '
        f'and {datetime.fromtimestamp(act.end / 1000)}')
    print(
        f'{len(meas)} records, first at {datetime.fromtimestamp(meas[0].date)}, last at '
        f'{datetime.fromtimestamp(meas[-1].date)}')
    print(f'Random sample of {NB_PRINTED_RECORDS} records per counter:')
    for counter in set(rec.counter for rec in meas):
        for rec in random.choices(list(rec for rec in meas if rec.counter == counter), k=NB_PRINTED_RECORDS):
            print(f'{rec.date} | {rec.counter} | {rec.value}')


def do_operationslog(
        docker_mon_cfg: DockerMonitorConfiguration,
        start: float = 0.0,
        end: float = None,
        write_db: bool = False,
        db_cfg: Optional[DatabaseConfiguration] = None,
        mon_cli: DockerMonitorClient = None):
    # mon_cli is actually unused here.

    if end is None:
        end = time.time()

    logger = logging.getLogger('main')

    for mon in docker_mon_cfg["dockeropslogservers"]:
        try:
            log = get_docker_operations_log(mon, int(start * 1000), int(end))
        except ConnectionError as err:
            logger.critical('failed fetching Docker operations log from server %s:%s: %s',
                            mon['address'], mon['port'], err)
            continue
        except ValueError as err:
            logger.critical('server %s:%s responded in error when fetching Docker operations log: %s',
                            mon['address'], mon['port'], err)
            continue
        else:
            logger.info('fetched log of %d Docker operations from server %s:%s',
                        len(log), mon['address'], mon['port'])

        if write_db:
            db = FaaSLoadDatabase(db_cfg)

            try:
                db.insert_docker_operations_log(mon, log)
            except FaaSLoadDatabaseException as err:
                if logger.isEnabledFor(logging.DEBUG):
                    logger.critical('failed storing Docker operations log of monitor %s:%s',
                                    mon['address'], mon['port'], exc_info=True)
                else:
                    logger.critical('failed storing Docker operations log of monitor %s:%s',
                                    mon['address'], mon['port'], err)
                continue
            else:
                logger.info('stored log of %d Docker operations of server %s:%s',
                            len(log), mon['address'], mon['port'])

        print(f'Fetched log of Docker operations from monitor {mon["address"]}:{mon["port"]} between '
              f'{datetime.fromtimestamp(start)} and {datetime.fromtimestamp(end)}')
        print(f'{len(log)} operations, first at {datetime.fromtimestamp(log[0].date)}, last at '
              f'{datetime.fromtimestamp(log[-1].date)}')
        print(f'Random sample of {NB_PRINTED_RECORDS} operations:')
        for rec in random.choices(log, k=NB_PRINTED_RECORDS):
            print(f'{rec.date} | {rec.docker_name} | {rec.operation}')


def main():
    argparser = ArgumentParser(
        description=dedent("""\
        Query the Docker monitor, to fetch the monitoring data about an invocation, or the log of Docker operations.
        
        The client can also write the fetched data to the database.
        """),
        formatter_class=RawDescriptionHelpFormatter)
    argparser.add_argument(
        '--write-db', action='store_true', default=False,
        help='also write the data to FaaSLoad\'s database')

    subparsers = argparser.add_subparsers(
        required=True,
        help=dedent("""\
        The client can fetch measurements of resource usage and perf counters for an activation, or the log of Docker
        operations.
        """))

    argparser_measurements = subparsers.add_parser(
        'measurements',
        help='Query resource usage and perf counters measurements of an activation.',
    )
    argparser_measurements.add_argument(
        'namespace', metavar='NAMESPACE',
        help='The namespace (usually, the username) that owns the invocation.')
    argparser_measurements.add_argument(
        'invocation_id', metavar='INVOCATION_ID',
        help='The ID of the invocation')
    argparser_measurements.set_defaults(func=do_measurements)

    argparser_dockeropslog = subparsers.add_parser(
        'operationslog',
        help='Query the complete log of Docker operations up to now.')
    argparser_dockeropslog.set_defaults(func=do_operationslog)

    args = vars(argparser.parse_args())
    func = args['func']
    del args['func']

    configure_logging(os.path.expanduser('~/.config/faasload/loader.yml'))
    logger = logging.getLogger('main')

    general_conf: FaaSLoadGlobalConfiguration = read_configuration(
        os.path.expanduser('~/.config/faasload/loader.yml'),
        prepare_and_read_configuration,
        default_mask=DEFAULTS,
        keep_unexpected_keys=True)

    try:
        platform_name = os.environ[FAASLOAD_PLATFORM_ENVAR]
    except KeyError:
        # Default platform is OpenWhisk for backward compatibility
        platform = FaaSPlatform.OpenWhisk
        logger.warning('environment variable "%s" to define FaaS platform not set; '
                       'using "%s" (%s) as a default, but consider setting it',
                       FAASLOAD_PLATFORM_ENVAR,
                       platform.value,
                       FAASPLATFORM_PRETTY_NAMES[platform])
    else:
        try:
            platform = FaaSPlatform(platform_name)
        except KeyError:
            logger.error('unsupported FaaS platform "%s" set in environment variable "%s"; '
                         'choose one of %s; aborting', platform_name, FAASLOAD_PLATFORM_ENVAR,
                         ', '.join(f'"{pf.value}"' for pf in FaaSPlatform))
            sys.exit(1)
    logger.info('running on FaaS platform %s', FAASPLATFORM_PRETTY_NAMES[platform])

    db_cfg = general_conf['database']
    logger.debug('database configuration: %s', db_cfg)

    docker_mon_cfg = general_conf['dockermonitor']
    logger.debug('monitor access configuration: %s', docker_mon_cfg)

    # Specialized initializations per platform
    if platform is FaaSPlatform.OpenWhisk:
        from faasload.loader import FaasloadOpenwhiskConfiguration

        conf: FaasloadOpenwhiskConfiguration = read_configuration(
            os.path.expanduser('~/.config/faasload/loader.yml'),
            prepare_and_read_configuration, DEFAULTS_OPENWHISK,
            keep_unexpected_keys=True)

        cfg = conf['openwhisk']
        logger.debug('OpenWhisk API configuration: %s', cfg)

        admin_cfg = conf['openwhiskadmin']
        logger.debug('OpenWhisk admin configuration: %s', admin_cfg)

        if not cfg['cert']:
            # This local import is only used to suppress warnings related to disabled certificate checking.
            import urllib3
            urllib3.disable_warnings()
            logger.warning('disabling certificate verification of OpenWhisk API gateway')

        monitor_client = DockerMonitorClientOpenWhisk(cfg, admin_cfg)
    elif platform is FaaSPlatform.Knative:
        from pyknative.client import get_api_configuration

        cfg = get_api_configuration()

        logger.debug('Knative API configuration: %s', cfg)

        monitor_client = DockerMonitorClientKnative(cfg)
    else:
        # Not possible: at this point, platform is a member of the bound enum FaaSPlatform
        logger.error('unsupported FaaS platform %s', platform)
        sys.exit(1)

    func(
        mon_cli=monitor_client,
        docker_mon_cfg=docker_mon_cfg,
        db_cfg=db_cfg,
        **args)


if __name__ == '__main__':
    main()
