#! /usr/bin/env python3

"""Create OpenWhisk assets to run FaaSLoad.

 1. load functions to FaaSLoad's database
 2. create OpenWhisk users for the generated workload injection (one per function to run)
 3. create actions in OpenWhisk under each user

For the injector mode, call with the flag `--injector` to only execute step 1.

This is a standalone runnable script (running the method `main`).

Constants:

 * `AUTHKEY_DIR`: output directory where authentication tokens of generated users are written
 * `LIMIT_NAMES`: a mapping between the limit names in the Manifest, and the names of the limit fields in an Action

Methods:

 * `read_functions`: read functions (as instances of Action) from the Manifest
 * `load_to_database`: load the Action instances into the database
 * `load_functions_faasload`: load functions from the Manifest to FaaSLoad's database
 * `create_users_wsk`: create OpenWhisk users to run the functions
 * `load_actions_wsk`: load functions to OpenWhisk
"""

# Copyright Toulouse INP / IRIT / CNRS, 2019 - 2020
# Copyright Télécom SudParis / IP Paris, 2021 - 2025
# Mathieu Bacou and other contributors
#
# mathieu.bacou@telecom-sudparis.eu
#
# This software is a computer program whose purpose is to inject a workload
# to a Function-as-a-Service platform, and to gather data about its execution.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software. You can use,
# modify and/ or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
import dataclasses
import json
import logging
import os
import shutil
import sys
from argparse import ArgumentParser, RawDescriptionHelpFormatter
from collections import defaultdict
from glob import glob
from textwrap import dedent

import yaml
from configuration_reader import read as read_configuration
from pywhisk import action as wsk_action, package as wsk_package
from pywhisk.admin import user as wskadm_user
from pywhisk.client import read_manifest
from pywhisk.models import Package

from faasload.lib import configure_logging, prepare_and_read_configuration
from faasload.loader import DEFAULTS_OPENWHISK, FaaSLoadGlobalConfiguration, DEFAULTS, FaasloadOpenwhiskConfiguration
from faasload.loader.database import FaaSLoadDatabase


def load_to_database(functions, db_cfg):
    """Load the functions to the database.

    :param functions: functions to load to database (mapping {pkg: {name: Action}})
    :type functions: dict[str, dict[str, Action]]
    :param db_cfg: configuration to connect to the database
    :type db_cfg: DatabaseConfiguration

    `functions` can be read from the Manifest using `read_functions()` and passed directly to this function.
    """

    db = FaaSLoadDatabase(db_cfg)
    db.create_functions_table()

    for package_name, package in functions.items():
        for function_name, function in package.items():
            try:
                parameters = [a['value'] for a in function.annotations if a['key'] == 'parameters'][0]
            except IndexError:
                # no parameters specified in the manifest
                parameters = ''
            input_kind = [a['value'] for a in function.annotations if a['key'] == 'input_kind'][0]

            function_dict = {
                'name':       package_name + '/' + function_name,
                'runtime':    function.exec.kind,
                'image':      function.exec.image if function.exec.kind == 'blackbox' else None,
                'parameters': yaml.dump(parameters),
                'input_kind': input_kind,
            }

            db.insert_function(function_dict)

            logging.info('\tloaded function %s (runtime: %s%s, input kind: %s)',
                         function_dict['name'],
                         function_dict['runtime'],
                         function_dict['image'] if function_dict['image'] is not None else '',
                         function_dict['input_kind'])

        logging.info('\tloaded package %s', package_name)


def load_functions_faasload(manifest_path, db_cfg):
    """Load the functions to FaaSLoad's database.

    :param manifest_path: the path to the Manifest
    :type manifest_path: str
    :param db_cfg: the configuration to connect to the database
    :type db_cfg: DatabaseConfiguration

    :returns: the list of functions read from the manifest (mapping {pkg: {name: Action}})
    """
    funcs = read_manifest(manifest_path)
    nb_funcs = sum(len(package) for package in funcs.values())

    logging.info('\tread %d functions from "%s"', nb_funcs, manifest_path)

    load_to_database(funcs, db_cfg)

    logging.info('\tloaded %d functions to table `functions` of database `%s` with user \'%s\'',
                 nb_funcs, db_cfg['database'], db_cfg['user'])

    return funcs


def create_users_wsk(users, auth_dir, wsk_admin_cfg):
    """Create user in OpenWhisk to run the functions.

    :param users: the users to create
    :type users: list of user names
    :param wsk_admin_cfg: the configuration to use administrative facilities of OpenWhisk
    :type wsk_admin_cfg: AdministrationConfiguration

    :returns: the mapping of function names to usernames (mapping {pkg: {name: (username, auth)}})
    """
    auths = {}

    os.makedirs(auth_dir, exist_ok=True)

    for user_name in users:
        user_auth = wskadm_user.create(user_name, wsk_admin_cfg)
        auths[user_name] = user_auth
        with open(os.path.join(auth_dir, user_name), 'x') as auth_file:
            auth_file.write(user_auth[0] + ':' + user_auth[1] + '\n')

        logging.info('\tcreated user %s', user_name)

    return auths


def load_actions_wsk(deployment, wsk_cfg, params_path=None):
    """Load actions to OpenWhisk under their matching users.

    :param deployment: the deployment plan to load actions (see below)
    :type deployment: dict[str, dict[str, dict[str, Action]]]
    :param wsk_cfg: the configuration to use OpenWhisk
    :type wsk_cfg: OpenWhiskConfiguration
    :param params_path: the path to the file with parameter values to bind at the package level
    :type params_path: str

    deployment is the deployment plan: nested dictionaries describing associating
    * a user name to packages for a user
    * a package name to functions in a package
    * a function name to the Action
    """
    params = _read_params(params_path) if params_path is not None else None
    for user_name, user_dep in deployment.items():
        for package_name, package in user_dep.items():
            wsk_package.create(Package(parameters=params), package_name, user_name, wsk_cfg)

            for func_name, func in package.items():
                wsk_action.create(func, package_name + '/' + func_name, user_name, wsk_cfg)

                logging.info('\tloaded action %s/%s to OpenWhisk under user %s', package_name, func_name, user_name)


def _read_params(path):
    with open(path, 'r') as param_file:
        return [{'key': key, 'value': value} for key, value in json.load(param_file).items()]


def revert_load_functions_faasload(db_cfg):
    try:
        db = FaaSLoadDatabase(db_cfg)
        db.drop_functions_table()
    except Exception as err:
        if logging.getLogger().isEnabledFor(logging.DEBUG):
            logging.warning('failed dropping functions table', exc_info=True)
        else:
            logging.warning('failed dropping functions table: %s', err)


# noinspection PyBroadException
def revert_create_users_wsk(users, auth_dir, wsk_admin_cfg):
    for user_name in users:
        try:
            wskadm_user.delete(user_name, wsk_admin_cfg)
        except Exception as err:
            if logging.getLogger().isEnabledFor(logging.DEBUG):
                logging.warning('failed deleting user %s', user_name, exc_info=True)
            else:
                logging.warning('failed deleting user %s: %s', user_name, err)
        else:
            shutil.rmtree(auth_dir, ignore_errors=True)


# noinspection PyBroadException
def revert_load_actions_wsk(deployment, wsk_cfg):
    for user_name, user in deployment.items():
        for pkg_name, pkg in user.items():
            for func_name, _ in pkg.items():
                try:
                    wsk_action.delete(pkg_name + '/' + func_name, user_name, wsk_cfg)
                except Exception as err:
                    if logging.getLogger().isEnabledFor(logging.DEBUG):
                        logging.warning('failed deleting action %s/%s of user %s', pkg_name, func_name, user_name,
                                        exc_info=True)
                    else:
                        logging.warning('failed deleting action %s/%s of user %s: %s', pkg_name, func_name, user_name,
                                        err)

            try:
                wsk_package.delete(pkg_name, user_name, wsk_cfg)
            except Exception as err:
                if logging.getLogger().isEnabledFor(logging.DEBUG):
                    logging.warning('failed deleting package %s of user %s', pkg_name, user_name,
                                    exc_info=True)
                else:
                    logging.warning('failed deleting package %s of user %s: %s', pkg_name, user_name, err)


def make_deployment(funcs, args):
    if args.generator:
        deployment = {
            'user-' + func_name: {
                pkg_name: {
                    func_name: func
                }
            }
            for pkg_name, pkg in funcs.items()
            for func_name, func in pkg.items()
        }
    else:
        deployment = defaultdict(lambda: defaultdict(dict))
        for trace_filename in glob(os.path.join(args.trace_dir, '*.faas')):
            with open(trace_filename, 'r') as trace_file:
                header = next(trace_file).rstrip()

            user_name, pkg_func_name, memory = tuple(header.split('\t'))
            pkg_name, func_name = tuple(pkg_func_name.split('/'))

            func = dataclasses.replace(funcs[pkg_name][func_name],
                                       limits=dataclasses.replace(funcs[pkg_name][func_name].limits,
                                                                  memory=int(memory)))
            deployment[user_name][pkg_name][func_name] = func
        if not deployment:
            raise ValueError(f'no traces found in "{args.trace_dir}": aborting')

    return deployment


# noinspection PyBroadException
def _load_assets(args, auth_dir, db_cfg, wsk_cfg, wsk_admin_cfg):
    logging.info('Loading functions to FaaSLoad\'s database...')

    try:
        funcs = load_functions_faasload(args.manifest_path, db_cfg)
    except Exception as err:
        if logging.getLogger().isEnabledFor(logging.DEBUG):
            logging.error('failed loading functions to FaaSLoad\'s database', exc_info=True)
        else:
            logging.error('failed loading functions to FaaSLoad\'s database: %s', err)

        if args.revert:
            logging.error('reverting...')

            try:
                revert_load_functions_faasload(db_cfg)
            except Exception as err:
                if logging.getLogger().isEnabledFor(logging.DEBUG):
                    logging.critical('failed reverting loading functions to FaaSLoad\'s database', exc_info=True)
                else:
                    logging.critical('failed reverting loading functions to FaaSLoad\'s database: %s', err)

        sys.exit(1)

    logging.info('Done')

    try:
        deployment = make_deployment(funcs, args)
    except Exception as err:
        if logging.getLogger().isEnabledFor(logging.DEBUG):
            logging.error('failed building deployment plan', exc_info=True)
        else:
            logging.error('failed building deployment plan: %s', err)

        if args.revert:
            logging.error('reverting...')

            try:
                revert_load_functions_faasload(db_cfg)
            except Exception as err:
                if logging.getLogger().isEnabledFor(logging.DEBUG):
                    logging.critical('failed reverting loading functions to FaaSLoad\'s database', exc_info=True)
                else:
                    logging.critical('failed reverting loading functions to FaaSLoad\'s database: %s', err)

        sys.exit(1)

    if logging.getLogger().isEnabledFor(logging.DEBUG):
        logging.debug('deployment plan:')
        for user_name, user in deployment.items():
            logging.debug('user %s:', user_name)
            for pkg_name, pkg in user.items():
                logging.debug('\t%s:', pkg_name)
                for func_name, func in pkg.items():
                    logging.debug('\t\t%s: %s', func_name, func)

    logging.info('Creating OpenWhisk users...')

    try:
        user_auths = create_users_wsk(deployment.keys(), auth_dir, wsk_admin_cfg)
    except Exception as err:
        if logging.getLogger().isEnabledFor(logging.DEBUG):
            logging.error('failed creating OpenWhisk users', exc_info=True)
        else:
            logging.error('failed creating OpenWhisk users: %s', err)

        if args.revert:
            logging.error('reverting...')

            try:
                revert_create_users_wsk(deployment.keys(), auth_dir, wsk_admin_cfg)
            except Exception as err:
                if logging.getLogger().isEnabledFor(logging.DEBUG):
                    logging.critical('failed reverting creating OpenWhisk users', exc_info=True)
                else:
                    logging.critical('failed reverting creating OpenWhisk users: %s', err)
                sys.exit(1)

            logging.error('also reverting loading functions to FaaSLoad\'s database...')

            try:
                revert_load_functions_faasload(db_cfg)
            except Exception as err:
                if logging.getLogger().isEnabledFor(logging.DEBUG):
                    logging.critical('failed reverting loading functions to FaaSLoad\'s database', exc_info=True)
                else:
                    logging.critical('failed reverting loading functions to FaaSLoad\'s database: %s', err)

        sys.exit(1)

    logging.info('Done')

    # update our WSK config with authentication of new users
    wsk_cfg['auth'].update(user_auths)

    logging.info('Loading actions to OpenWhisk...')

    try:
        load_actions_wsk(deployment, wsk_cfg, args.param_file)
    except Exception as err:
        if logging.getLogger().isEnabledFor(logging.DEBUG):
            logging.critical('failed loading actions to OpenWhisk:', exc_info=True)
        else:
            logging.critical('failed loading actions to OpenWhisk: %s', err)

        if args.revert:
            logging.error('reverting...')

            try:
                revert_load_actions_wsk(deployment, wsk_cfg)
            except Exception as err:
                if logging.getLogger().isEnabledFor(logging.DEBUG):
                    logging.critical('failed reverting loading actions to OpenWhisk', exc_info=True)
                else:
                    logging.critical('failed reverting loading actions to OpenWhisk: %s', err)

                sys.exit(1)

            logging.error('also reverting creating OpenWhisk users...')

            try:
                revert_create_users_wsk(deployment.keys(), auth_dir, wsk_admin_cfg)
            except Exception as err:
                if logging.getLogger().isEnabledFor(logging.DEBUG):
                    logging.critical('failed reverting creating OpenWhisk users', exc_info=True)
                else:
                    logging.critical('failed reverting creating OpenWhisk users: %s', err)
                sys.exit(1)

            logging.error('also reverting loading functions to FaaSLoad\'s database...')

            try:
                revert_load_functions_faasload(db_cfg)
            except Exception as err:
                if logging.getLogger().isEnabledFor(logging.DEBUG):
                    logging.critical('failed reverting loading functions to FaaSLoad\'s database', exc_info=True)
                else:
                    logging.critical('failed reverting loading functions to FaaSLoad\'s database: %s', err)

        sys.exit(1)

    logging.info('Done')


# noinspection PyBroadException
def _unload_assets(args, auth_dir, db_cfg, wsk_cfg, wsk_admin_cfg):
    funcs = read_manifest(args.manifest_path)
    try:
        deployment = make_deployment(funcs, args)
    except Exception as err:
        if logging.getLogger().isEnabledFor(logging.DEBUG):
            logging.error('failed building deployment plan', exc_info=True)
        else:
            logging.error('failed building deployment plan: %s', err)
        sys.exit(1)

    if logging.root.isEnabledFor(logging.DEBUG):
        logging.debug('unloading deployment plan:')
        for user_name, user in deployment.items():
            logging.debug('user %s:', user_name)
            for pkg_name, pkg in user.items():
                logging.debug('\t%s:', pkg_name)
                for func_name, func in pkg.items():
                    logging.debug('\t\t%s: %s', func_name, func)

    logging.info('Unloading actions from OpenWhisk...')

    try:
        revert_load_actions_wsk(deployment, wsk_cfg)
    except Exception as err:
        if logging.getLogger().isEnabledFor(logging.DEBUG):
            logging.error('failed unloading actions from OpenWhisk', exc_info=True)
        else:
            logging.error('failed unloading actions from OpenWhisk: %s', err)
        sys.exit(1)

    logging.info('Done')

    logging.info('Deleting OpenWhisk users...')

    try:
        revert_create_users_wsk(deployment.keys(), auth_dir, wsk_admin_cfg)
    except Exception as err:
        if logging.getLogger().isEnabledFor(logging.DEBUG):
            logging.error('failed deleting OpenWhisk users', exc_info=True)
        else:
            logging.error('failed deleting OpenWhisk users: %s', err)
        sys.exit(1)

    logging.info('Done')

    logging.info('Unloading functions from FaaSLoad\'s database...')

    try:
        revert_load_functions_faasload(db_cfg)
    except Exception as err:
        if logging.getLogger().isEnabledFor(logging.DEBUG):
            logging.error('failed unloading functions from FaaSLoad\'s database', exc_info=True)
        else:
            logging.error('failed unloading functions from FaaSLoad\'s database: %s', err)
        sys.exit(1)


def main():
    """Main method of the script."""
    import urllib3

    argparser = ArgumentParser(
        description=dedent("""\
        Create OpenWhisk assets to run FaaSLoad in workload trace injector or in dataset generator mode.
        
        1. load functions to FaaSLoad's database
        2. create OpenWhisk users for the workload injection
        3. create actions in OpenWhisk under each user
        
        The general steps above differ in details depending on the mode: injector or generator.
        
        With --unload/-U, do the opposite: unload/delete the assets. See also --revert."""),
        epilog=dedent("""\
        For more details, see "docs/injector.md" or "docs/generator.md"."""),
        formatter_class=RawDescriptionHelpFormatter)
    argparser.add_argument(
        'manifest_path', metavar='MANIFEST',
        help=dedent("""\
        Path to the manifest file describing the OpenWhisk actions to load.
        Note: while many packages may be specified in the manifest, only having one is supported (arbitrary name)."""))
    mode_group = argparser.add_mutually_exclusive_group(required=True)
    mode_group.add_argument(
        '--generator', '-g', action='store_true',
        help='Load OpenWhisk assets for dataset generator mode.')
    mode_group.add_argument(
        '--injector', '-i', metavar='TRACE_DIR', dest='trace_dir',
        help=dedent("""\
        Load OpenWhisk assets for workload injector mode.
        %(metavar)s is the directory of workload traces, as produced by the trace builder."""))
    argparser.add_argument(
        '--param-file', '-P', metavar='FILE',
        help=dedent("""\
        FILE containing parameter values in JSON format to bind at the package level.
        Not needed with --unload/-U."""))
    argparser.add_argument(
        '--revert', action='store_true',
        help=dedent("""\
        On failure, try to revert effects of successful previous steps.
        WARNING: revert all CALCULATED effects of the failed steps, so may revert more than what was exactly done. For
        instance, if trying to create users A, B and C in this order, but user B already exists, the step fails after
        having created user A. However, it planned to create users B and C afterwards, so when reverting, users B and C
        are also deleted after user A."""))
    argparser.add_argument(
        '--unload', '-U', action='store_true',
        help=dedent("""\
        Unload mode.
        Instead of loading assets, they are unloaded. In other words, revert all the effects that would occur when
        loading assets from the specified MANIFEST. This is done in a "best effort" way: try to unload as much as
        possible without complaining of missing assets. May be combined with --injector/-i."""))
    argparser.add_argument(
        '--verbose', '-v', action='store_true',
        help='Increase verbosity level'
    )
    args = argparser.parse_args()

    configure_logging(os.path.expanduser('~/.config/faasload/loader.yml'))
    # note: override root logger configuration loaded from configuration file above
    logging.root.setLevel(logging.DEBUG if args.verbose else logging.INFO)

    general_conf: FaaSLoadGlobalConfiguration = read_configuration(
        os.path.expanduser('~/.config/faasload/loader.yml'),
        prepare_and_read_configuration,
        default_mask=DEFAULTS,
        keep_unexpected_keys=True)

    db_cfg = general_conf['database']

    conf: FaasloadOpenwhiskConfiguration = read_configuration(
        os.path.expanduser('~/.config/faasload/loader.yml'),
        prepare_and_read_configuration, DEFAULTS_OPENWHISK,
        keep_unexpected_keys=True)

    cfg = conf['openwhisk']

    urllib3.disable_warnings()

    wsk_admin_cfg = conf['openwhiskadmin']

    # The configuration file includes authkeys, but it is replaced by parsing the actual authentication keys in the
    # folder.
    # But here we need the actual folder, to create and remove the authentication keys.
    # So get it manually from the configuration file.
    # At this point, the configuration was parsed successfully, so there can be no error except authkeys is actually
    # missing, which is permitted in the general case.
    try:
        with open(os.path.expanduser('~/.config/faasload/loader.yml'), 'r') as cfg_f:
            auth_dir = yaml.full_load(cfg_f)['openwhisk']['authkeys']
    except KeyError:
        logging.critical('missing configuration key openwhisk.authkeys')
        sys.exit(2)

    if args.unload:
        _unload_assets(args, auth_dir, db_cfg, cfg, wsk_admin_cfg)
    else:
        _load_assets(args, auth_dir, db_cfg, cfg, wsk_admin_cfg)

    logging.info('And all done!')


if __name__ == '__main__':
    main()
