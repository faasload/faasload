#! /usr/bin/env python3

# Copyright Toulouse INP / IRIT / CNRS, 2019 - 2020
# Copyright Télécom SudParis / IP Paris, 2021 - 2025
# Mathieu Bacou and other contributors
#
# mathieu.bacou@telecom-sudparis.eu
#
# This software is a computer program whose purpose is to inject a workload
# to a Function-as-a-Service platform, and to gather data about its execution.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software. You can use,
# modify and/ or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.

import argparse
import logging
import os
import re
import sys
from collections import namedtuple
from collections.abc import Iterator
from enum import Enum
from random import Random
from textwrap import dedent

import yaml

from pywhisk.client import read_manifest

TRACES_PARENT_DIR = os.path.expanduser(os.path.expandvars('~/faasload/injection-traces'))

INPUT_EXTENSIONS = {
    'image': 'jpg',
    'audio': 'wav',
    'video': 'avi',
}


class MemoryProfile(Enum):
    MAXIMUM = 'maximum'
    SMART = 'smart'
    AVERAGE = 'average'


MEMORY_MAXIMUM = 2048
MEMORY_AVERAGE_MULT = 1.6

MEMORY_PROFILE_COMPUTE = {
    MemoryProfile.MAXIMUM: lambda mem: MEMORY_MAXIMUM,
    MemoryProfile.SMART: lambda mem: mem,
    MemoryProfile.AVERAGE: lambda mem: min(MEMORY_AVERAGE_MULT * mem, MEMORY_MAXIMUM)
}


class IatDistribution(Enum):
    """Recognized distributions of inter-arrival times to build traces.

    * uniform: regular distribution
    * poisson: Poisson distribution of number of invocations, i.e. exponential distribution of IATs
    """
    UNIFORM = 'uniform'
    POISSON = 'poisson'


def _iat_gen_uniform(rate, rng):
    shift = rng.uniform(0, 1 / rate)

    yield round(shift + 1 / rate, 3)

    while True:
        yield round(1 / rate, 3)


def _iat_gen_poisson(rate, rng):
    while True:
        yield round(rng.expovariate(rate), 3)


IAT_GENERATORS = {
    IatDistribution.UNIFORM: _iat_gen_uniform,
    IatDistribution.POISSON: _iat_gen_poisson
}


class Trace(Iterator):
    def __init__(self, action_name, action_memory, points):
        self.action_name = action_name
        self.action_memory = action_memory
        self.points = points

    def __iter__(self):
        return self

    def __next__(self):
        return next(self.points)


TracePoint = namedtuple('TracePoint', ['wait', 'input', 'parameters'])


def read_user_specs(specs_path):
    with open(specs_path, 'r') as specs_file:
        specs = yaml.full_load(specs_file)

    for _, user_spec in specs['users'].items():
        user_spec['memory-profile'] = MemoryProfile(user_spec['memory-profile'])
        user_spec['nb-inputs'] = {input_kind: int(nb_inputs) for input_kind, nb_inputs in
                                  user_spec['nb-inputs'].items()}

    return specs['users']


def select_user_actions(user_action_specs, actions, rng):
    # This dict hold the selected actions, mapping full action names to their records.
    # The dict keys are used as a set to collapse doubly-selected actions (this also explains why the full name is
    # redundantly both the key and a part of the value)
    selected_actions = {}
    for i, action_spec in enumerate(user_action_specs['matches']):
        actions_matches = _find_actions(action_spec['match'], actions)

        try:
            actions_matches = rng.sample(actions_matches, int(action_spec['number']))
        except KeyError:
            # no 'number' key: use all
            pass
        except ValueError:
            # not enough matches
            raise ValueError(f'not enough actions were matched for match #{i}') from None

        selected_actions.update({pkg_name + '/' + action_name: {
            'name': pkg_name + '/' + action_name,
            'activity-window': action_spec['activity-window'] if 'activity-window' in action_spec else None,
            'distribution': IatDistribution(action_spec['distribution']),
            'rate': float(action_spec['rate']),
        } for pkg_name, action_name in actions_matches})

    try:
        return [selected_actions[act_fullname]
                for act_fullname in rng.sample(selected_actions.keys(), int(user_action_specs['number']))]
    except KeyError:
        # no 'number' key: use all
        pass
    except ValueError:
        # not enough matches
        raise ValueError(f'not enough actions were matched for the user') from None


def _find_actions(spec, actions):
    # make a copy of actions to filter actions out
    pool = dict((pkg_name, dict(pkg)) for pkg_name, pkg in actions.items())

    if 'package' in spec:
        pool = {pkg_name: pkg for pkg_name, pkg in pool.items() if re.search(spec['package'], pkg_name) is not None}
    if 'name' in spec:
        for pkg_name in pool:
            pool[pkg_name] = {action_name: action
                              for action_name, action in pool[pkg_name].items()
                              if re.search(spec['name'], action_name) is not None}

    if 'runtime' in spec:
        for pkg_name in pool:
            pool[pkg_name] = {action_name: action
                              for action_name, action in pool[pkg_name].items()
                              if action.exec.kind is not None
                              and re.search(spec['runtime'], action.exec.kind) is not None}
    if 'docker' in spec:
        for pkg_name in pool:
            pool[pkg_name] = {action_name: action
                              for action_name, action in pool[pkg_name].items()
                              if (action.exec.kind == 'blackbox'
                                  and action.exec.image is not None
                                  and re.search(spec['docker'], action.exec.image) is not None)}

    if 'annotations' in spec:
        for pkg_name in pool:
            pool[pkg_name] = {action_name: action
                              for action_name, action in pool[pkg_name].items()
                              if _match_annotations(action, spec['annotations'])}

    return [(pkg_name, action_name) for pkg_name, pkg in pool.items() for action_name in pkg]


def _match_annotations(action, spec_annots):
    # convert annotations in the Action model to a proper mapping
    action_annots = {annot['key']: annot['value'] for annot in action.annotations}

    return all(spec_annot_key in action_annots and
               re.search(spec_annot_value, action_annots[spec_annot_key]) is not None
               for spec_annot_key, spec_annot_value in spec_annots.items())


def build_traces(duration, user_action_specs, memory_profile, nb_inputs, rng):
    traces = []

    for spec in user_action_specs:
        memory = MEMORY_PROFILE_COMPUTE[memory_profile](spec['action'].limits.memory)

        # preprocessing of arguments to _get_trace_point:
        # * we may have an activity window, otherwise just generate point from 0 to the given duration
        # * inter-arrival times are either given by the spec (key 'times') or generated from a statistical distribution
        points = _get_trace_point(
            spec['activity-window']['from'] if spec['activity-window'] is not None else 0,
            min(duration, spec['activity-window']['to']) if spec['activity-window'] is not None else duration,
            iter(spec['times']) if 'times' in spec else IAT_GENERATORS[spec['distribution']](spec['rate'], rng),
            _get_action_annotation(spec['action'], 'input_kind'),
            _get_action_annotation(spec['action'], 'parameters'),
            nb_inputs,
            rng)

        traces.append(Trace(
            spec['name'],
            memory,
            points,
        ))

    return traces


def _prepare_parameter(param, rng):
    if param['type'] == 'range_float':
        value = rng.uniform(param['min'], param['max'])
    elif param['type'] == 'range_int':
        value = rng.randint(param['min'], param['max'])
    elif param['type'] == 'ensemble':
        value = rng.choice(param['values'])
    else:
        raise ValueError(f'unknown parameter type "{param["type"]}"')

    return value


def _get_trace_point(start_time, end_time, iat_gen, action_input_kind, action_params, nb_inputs, rng):
    wait = next(iat_gen) + start_time
    cur_duration = wait
    while cur_duration < end_time:
        if action_input_kind != 'none':
            input_id = rng.randint(1, nb_inputs[action_input_kind])
            input_objectname = str(input_id) + '.' + INPUT_EXTENSIONS[action_input_kind]
        else:
            input_objectname = None

        params = {
            param['name']: _prepare_parameter(param, rng)
            for param in action_params if param['name'] != 'object'
        }

        yield TracePoint(wait=wait, input=input_objectname, parameters=params)

        try:
            wait = next(iat_gen)
        except StopIteration:
            # we reached the end of our IATs generator before reaching the end of the trace duration-wise
            break

        cur_duration += wait


def _get_action_annotation(action, annotation_key):
    return [annot for annot in action.annotations
            if annot['key'] == annotation_key][0]['value']


def main():
    argparser = argparse.ArgumentParser(
        description=dedent(f"""\
        Build workload traces to inject using FaaSLoad in injector mode.
        
        Given:
        * user specifications: models of users of the FaaS platform
        * a manifest: description of actions that can be invoked by the traces
        * a trace duration
        the script produces traces readable by FaaSLoad that represent a FaaS workload, by simulating different
        users that deploy different functions.
        
        The traces can be fully synthetic, or based on existing inter-arrival times (IATs). Actions are invoked with
        varying input files and parameter values randomized according to the specifications.
        
        The traces are written under {TRACES_PARENT_DIR}, in a directory named after the filename of the user
        specifications.
        """),
        epilog=dedent("""\
        For more details, see "docs/injector.md" or "docs/trace-builder-user-specs.md" for the user
        specifications."""),
        formatter_class=argparse.RawDescriptionHelpFormatter)
    argparser.add_argument(
        'manifest_path', metavar='MANIFEST',
        help=dedent("""\
        Path to the manifest file describing the OpenWhisk actions to use in the traces.
        Note: while many packages may be specified in the manifest, only having one is supported (arbitrary name)."""))
    argparser.add_argument(
        '--duration', '-d', metavar='SECONDS', dest='duration', required=True, type=float,
        help='Duration of the traces (seconds).')
    argparser.add_argument(
        '--user-specs', '-u', metavar='SPECS_FILE', dest='specs_path', required=True,
        help='File of user specifications.')
    argparser.add_argument(
        '--random-seed', '-r', metavar='SEED', dest='random_seed', type=int, default=None,
        help=dedent("""\
            Seed (integer) for the random number generator.
            The same traces will be generated for the same seed."""))
    argparser.add_argument(
        '--verbose', '-v', action='store_true',
        help='Increase verbosity level')
    args = argparser.parse_args()

    logging.basicConfig(level=(logging.DEBUG if args.verbose else logging.INFO))

    logging.info('Loading user specifications...')
    try:
        user_specs = read_user_specs(args.specs_path)
    except (FileNotFoundError, ValueError):
        logging.exception('failed reading user specifications')
        sys.exit(1)

    logging.debug(user_specs)

    logging.info('Done')

    traces_dir = os.path.join(TRACES_PARENT_DIR, os.path.splitext(os.path.basename(args.specs_path))[0])
    os.makedirs(traces_dir, exist_ok=False)

    logging.info('Reading functions from manifest...')

    try:
        funcs = read_manifest(args.manifest_path)
    except (FileNotFoundError, yaml.YAMLError):
        logging.exception('failed reading functions from manifest')
        sys.exit(1)

    if logging.root.isEnabledFor(logging.DEBUG):
        logging.debug('read %d packages:', len(funcs))
        for pkg_name, pkg in funcs.items():
            logging.debug('\t%s (%d functions):', pkg_name, len(pkg))
            for func_name, func in pkg.items():
                logging.debug('\t\t%s: %s', func_name, func)

    logging.info('Done')

    rng = Random(args.random_seed)

    logging.info('Building injection traces...')

    for user_name, user_spec in user_specs.items():
        if isinstance(user_spec['actions'], list):
            logging.info('User %s: using given list of actions...', user_name)

            user_action_specs = user_spec['actions']
            for action_spec in user_action_specs:
                if 'activity-window' not in action_spec:
                    action_spec['activity-window'] = None
                try:
                    action_spec['distribution'] = IatDistribution(action_spec['distribution'])
                except KeyError:
                    # no key 'distribution': it should be a fixed list of IATs under key 'times'
                    pass
                else:
                    action_spec['rate'] = float(action_spec['rate'])
        else:
            logging.info('User %s: selecting matching actions...', user_name)

            user_action_specs = select_user_actions(user_spec['actions'], funcs, rng)

            logging.debug('User %s: selected %d actions', user_name, len(user_action_specs))
            logging.debug(user_action_specs)

        for action_spec in user_action_specs:
            pkg_name, action_name = tuple(action_spec['name'].split('/'))
            action_spec['action'] = funcs[pkg_name][action_name]

        logging.info('User %s: building traces...', user_name)

        user_traces = build_traces(
            args.duration,
            user_action_specs,
            user_spec['memory-profile'],
            user_spec['nb-inputs'],
            rng)

        logging.info('User %s: writing traces to "%s"...', user_name, traces_dir)

        for i, trace in enumerate(user_traces):
            with open(os.path.join(traces_dir, f'{user_name}_{i + 1}.faas'), 'w') as trace_file:
                trace_file.write('\t'.join(
                    (user_name, f'{trace.action_name}', str(trace.action_memory))) + '\n')
                # in the tuples, point.input may be None, and point.parameters may be the empty dict, thus the
                # expression around it to produce None instead
                point_lines = [(str(point.wait),
                                point.input,
                                *([f'{param_name}:{param_value}'
                                   for param_name, param_value in point.parameters.items()] or (None,)))
                               for point in trace]
                trace_file.writelines(['\t'.join((elem for elem in line if elem is not None)) + '\n'
                                       for line in point_lines])

    logging.info('All done')


if __name__ == '__main__':
    main()
