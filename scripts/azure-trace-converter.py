#! /bin/env python3

# Copyright Toulouse INP / IRIT / CNRS, 2019 - 2020
# Copyright Télécom SudParis / IP Paris, 2021 - 2024
# Mathieu Bacou and other contributors
#
# mathieu.bacou@telecom-sudparis.eu
#
# This software is a computer program whose purpose is to inject a workload
# to a Function-as-a-Service platform, and to gather data about its execution.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software. You can use,
# modify and/ or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.

import logging
import os
import sys
from argparse import ArgumentParser
from dataclasses import dataclass
from math import floor
from os import path

import numpy as np
import pandas as pd
import scipy as sp
import yaml
from pandas import DataFrame, Series

TEST_MODE = False

INVOCATIONS_FN_TMPL = "invocations_per_function_md.anon.d{day:02d}.csv"
DURATIONS_FN_TMPL = "function_durations_percentiles.anon.d{day:02d}.csv"
MEMORY_FN_TMPL = "app_memory_percentiles.anon.d{day:02d}.csv"

ALL_DAYS = list(range(1, 13))


@dataclass
class FaaSLoadTrace:
    user: str
    function: str
    memory: int
    points: pd.DataFrame


FaaSLoadTraces = dict[tuple[str, str], FaaSLoadTrace]


class AzureTraces:
    def __init__(
        self,
        traces_dp: str,
        sampling_win: tuple[int, int] = (1, ALL_DAYS[-1] * 1440),
        min_invocs_per_day: float = 0.0,
        rng_seed: int = 20191108,
        sample_down: float = 1.0,
    ):
        self.logger = logging.getLogger("azuretraces")

        if TEST_MODE:
            self.logger.info("TEST MODE")

        self.traces_dp = traces_dp
        self.days = list(range((sampling_win[0] - 1) // 1440 + 1, (sampling_win[1] - 1) // 1440 + 2))
        self.mins = list(range(sampling_win[0], sampling_win[1] + 1))
        # Filter functions with no invocation in the sampling window.
        self.min_invocs = max(floor(min_invocs_per_day * (sampling_win[1] - sampling_win[0] + 1) / 1440), 1.0)
        self.sample_down = sample_down

        self.rng = np.random.default_rng(rng_seed)

        self.functions_diff_triggers = 0

        self.processed = False
        self.data = None

        self.iats = None
        self.fn_memory = None

        self.report = dict(
            durations=dict(
                mu=None,
                sigma=None,
            ),
            iats=dict(
                frac_lessthan_oncehour=None,
                frac_lessthan_onceminute=None,
            ),
            memory=dict(
                c=None,
                k=None,
                lambd=None,
            ),
            functions=None,
        )

    def load_and_process(self):
        self.logger.info("loading data for days %s", self.days)

        invocations_dtypes = {str(mn): pd.UInt32Dtype() for mn in self.mins}
        invocations_dtypes.update(
            {
                "HashOwner": pd.StringDtype(),
                "HashApp": pd.StringDtype(),
                "HashFunction": pd.StringDtype(),
                "Trigger": pd.CategoricalDtype(
                    ["http", "timer", "event", "queue", "storage", "orchestration", "others"]
                ),
            }
        )
        invocations = pd.concat(
            [
                pd.read_csv(
                    fp,
                    header=0,
                    names=(
                        ["HashOwner", "HashApp", "HashFunction", "Trigger"]
                        + [str(mn + 1440 * (day - 1)) for mn in range(1, 1441)]
                    ),
                    usecols=["HashOwner", "HashApp", "HashFunction", "Trigger"]
                    + [str(mn) for mn in self.mins if (mn - 1) // 1440 >= day - 1 and (mn - 1) // 1440 < day],
                    index_col=["HashOwner", "HashApp", "HashFunction"],
                    nrows=10 if TEST_MODE else None,
                    dtype=invocations_dtypes,
                )
                for day, fp in self.make_daily_fps(INVOCATIONS_FN_TMPL)
            ],
            axis="columns",
            keys=self.days,
            names=["Day", "Function ID"],
        ).fillna({(day, str(mn)): 0 for day in self.days for mn in self.mins})
        self.logger.info("loaded invocations data")

        # Drop percentiles: they cannot be combined across days.
        durations = pd.concat(
            [
                self.deduplicate_daily_functions(
                    pd.read_csv(
                        fp,
                        usecols=["HashOwner", "HashApp", "HashFunction", "Average", "Count"],
                        nrows=10 if TEST_MODE else None,
                        dtype={
                            "HashOwner": pd.StringDtype(),
                            "HashApp": pd.StringDtype(),
                            "HashFunction": pd.StringDtype(),
                            "Average": pd.UInt32Dtype(),
                            "Count": pd.UInt32Dtype(),
                        },
                    )
                ).set_index(["HashOwner", "HashApp", "HashFunction"])
                for day, fp in self.make_daily_fps(DURATIONS_FN_TMPL)
            ],
            axis="columns",
            keys=self.days,
            names=["Day", "Function ID"],
        ).fillna(0)
        self.logger.info("loaded durations data")

        # Drop percentiles: they cannot be combined across days.
        memory = pd.concat(
            [
                self.deduplicate_daily_applications(
                    pd.read_csv(
                        fp,
                        usecols=["HashOwner", "HashApp", "SampleCount", "AverageAllocatedMb"],
                        nrows=10 if TEST_MODE else None,
                        dtype={
                            "HashOwner": pd.StringDtype(),
                            "HashApp": pd.StringDtype(),
                            "SampleCount": pd.UInt32Dtype(),
                            "AverageAllocatedMb": pd.UInt16Dtype(),
                        },
                    )
                ).set_index(["HashOwner", "HashApp"])
                for day, fp in self.make_daily_fps(MEMORY_FN_TMPL)
            ],
            axis="columns",
            keys=self.days,
            names=["Day", "Application ID"],
        ).fillna(0)
        self.logger.info("loaded memory data")

        if self.logger.isEnabledFor(logging.DEBUG):
            invocations.info(buf=sys.stderr)
            self.logger.debug("10 rows of invocations:\n%s", invocations.head(10))
            durations.info(buf=sys.stderr)
            self.logger.debug("10 rows of durations:\n%s", durations.head(10))
            memory.info(buf=sys.stderr)
            self.logger.debug("10 rows of memory:\n%s", memory.head(10))

        self.logger.info("filtering functions with fewer than %d invocations total", self.min_invocs)
        invocations[(0, "Invocations")] = invocations.loc[:, (slice(None), [str(mn) for mn in self.mins])].sum(
            axis="columns"
        )
        invocations = invocations[invocations.loc[:, (0, "Invocations")] >= self.min_invocs]
        self.logger.info(
            "filtered functions with fewer than %d invocations total (now down to %d)",
            self.min_invocs,
            invocations.shape[0],
        )
        self.logger.info("filtering functions triggered by timer, orchestration or others")
        invocations = invocations[
            ~invocations.loc[:, (slice(None), "Trigger")].isin(["timer", "orchestration", "others"]).all(axis="columns")
        ]
        self.logger.info("filtered functions triggered by timer")
        self.logger.info("filtering functions with 0.0s average execution time")
        durations = durations[durations.loc[:, (slice(None), "Average")].sum(axis="columns") > 0.0]
        self.logger.info("filtered functions with 0.0s average execution time")

        self.logger.info("joining durations to invocations")
        invocs_durs = invocations.join(durations, how="inner")
        self.logger.info("joined durations to invocations")
        if self.logger.isEnabledFor(logging.DEBUG):
            invocs_durs.info(buf=sys.stderr)
            self.logger.debug("10 rows of invocations+durations:\n%s", invocs_durs.head(10))
        if invocs_durs.shape[0] < invocations.shape[0]:
            self.logger.warning(
                "resulting joined dataframe has fewer functions than the filtered invocations data (%d instead of %d), "
                "functions were missing duration data or had 0.0s average execution time",
                invocs_durs.shape[0],
                invocations.shape[0],
            )

        self.logger.info("joining memory to invocations+durations")
        self.data = invocs_durs.join(memory, on=["HashOwner", "HashApp"], how="inner")
        self.logger.info("joined memory to invocations+durations")
        if self.logger.isEnabledFor(logging.DEBUG):
            self.data.info(buf=sys.stderr)
            self.logger.debug("10 rows of full data:\n%s", self.data.head(10))
        if self.data.shape[0] < invocs_durs.shape[0]:
            self.logger.warning(
                "resulting joined dataframe has fewer functions than the filtered invocations+durations "
                "data (%d instead of %d), applications were missing memory data",
                self.data.shape[0],
                invocs_durs.shape[0],
            )

        if self.sample_down < 1.0:
            self.logger.info("sampling down to %.0f%% of users", 100 * self.sample_down)
            self.data = self.data.loc[
                (
                    self.data.index.get_level_values("HashOwner")
                    .to_series()
                    .sample(frac=self.sample_down, random_state=self.rng),
                    slice(None),
                    slice(None),
                ),
                :,
            ]
            self.logger.info("sampled down to %.0f%% of users", 100 * self.sample_down)

        if len(self.days) > 1:
            self.logger.info("combining invocation triggers")
            self.combine_invocations_triggers()
            self.logger.info("combined invocation triggers")
            if self.functions_diff_triggers != 0:
                self.logger.info("there were %d functions with different triggers", self.functions_diff_triggers)

            self.logger.info("combining durations stats")
            self.combine_durations_stats()
            self.logger.info("combined durations stats")

            self.logger.info("combining memory stats")
            self.combine_memory_stats()
            self.logger.info("combined memory stats")

        self.data = self.data.droplevel("Day", axis="columns")

        self.data.drop([str(mn) for mn in self.mins], axis="columns", inplace=True)
        self.data.drop("Trigger", axis="columns", inplace=True)

        self.logger.info("full data is ready, it has %d functions", len(self.data))
        if self.logger.isEnabledFor(logging.DEBUG):
            self.data.info(buf=sys.stderr)
            self.logger.debug("10 rows of ready full data:\n%s", self.data.head(10))

        self.processed = True

        self.report_duration()

    def report_duration(self):
        # As in the reference paper: log-normal fit (of the CDF) of the data via MLE.
        durations = self.data["Average"] / 1000

        self.report["durations"]["mu"] = np.log(durations).mean().item()
        self.report["durations"]["sigma"] = np.sqrt(
            np.square(np.log(durations) - self.report["durations"]["mu"]).mean()
        ).item()

    def make_daily_fps(self, fn_tmpl: str) -> list[tuple[int, str]]:
        return [(day, path.join(self.traces_dp, fn_tmpl.format(day=day))) for day in self.days]

    def combine_invocations_triggers(self):
        """Combine triggers of different days under one trigger under day 0."""

        def combine_triggers(inv: Series) -> str:
            ts_nona = inv.dropna().drop_duplicates()

            if ts_nona.empty:
                self.logger.error("function %.8s has no triggers", ts_nona.name[2])
                raise ValueError("no triggers")

            if not (ts_nona.iloc[-1] == ts_nona).all():
                self.logger.warning(
                    "function %.8s has different triggers: %s", ts_nona.name[2], ", ".join(ts_nona["Trigger"])
                )
                self.functions_diff_triggers += 1

            return ts_nona.iloc[0]

        self.data[(0, "Trigger")] = (
            self.data.droplevel("Day", axis="columns")["Trigger"]
            .aggregate(func=combine_triggers, axis="columns")
            .astype("category")
        )
        self.data.drop([(day, "Trigger") for day in self.days], axis="columns", inplace=True)

    def deduplicate_daily_functions(self, d: DataFrame) -> DataFrame:
        # Just drop first daily duplicates, keeping the last one.
        return d.drop_duplicates(subset="HashFunction", keep="last")

    def combine_durations_stats(self):
        """Combine duration stats of different days under day 0."""
        count = Series(self.data.loc[:, (slice(None), "Count")].sum(axis="columns"), name="Count")

        self.data[(0, "Average")] = (
            self.data.loc[:, (slice(None), ["Average", "Count"])]
            .aggregate(
                lambda per_day: per_day[(slice(None), "Average")] * per_day[(slice(None), "Count")], axis="columns"
            )
            .sum(axis="columns")
            / count
        )
        self.data.drop(
            [(day, stat) for day in self.days for stat in ["Average", "Count"]],
            axis="columns",
            inplace=True,
        )

    def deduplicate_daily_applications(self, d: DataFrame) -> DataFrame:
        # Just drop first daily duplicates, keeping the last one.
        return d.drop_duplicates(subset="HashApp", keep="last")

    def combine_memory_stats(self):
        """Combine memory stats of different days under day 0."""
        count = Series(self.data.loc[:, (slice(None), "SampleCount")].sum(axis="columns"), name="SampleCount")

        self.data[(0, "AverageAllocatedMb")] = (
            self.data.loc[:, (slice(None), ["AverageAllocatedMb", "SampleCount"])]
            .aggregate(
                lambda per_day: per_day[(slice(None), "AverageAllocatedMb")] * per_day[(slice(None), "SampleCount")],
                axis="columns",
            )
            .sum(axis="columns")
            / count
        )

        self.data.drop(
            [(day, stat) for day in self.days for stat in ["AverageAllocatedMb", "SampleCount"]],
            axis="columns",
            inplace=True,
        )

    def generate_IATs(self):
        """

        Expects that functions in self.data all have at least 1 invocation.
        """
        if not self.processed:
            raise ValueError("process the traces first")

        self.logger.info("generating IATs")

        # Generate IATs assuming they follow a Poisson distribution, with average computed from the invocations in the
        # sampled window.
        self.iats = {
            (fn[0], fn[1], fn[2]): (
                pd.Series(self.rng.exponential(60 * len(self.mins) / invocs, int(invocs)), dtype=pd.Float32Dtype())
            )
            for fn, invocs in self.data["Invocations"].items()
        }
        self.logger.info("generated IATs for %d functions", len(self.iats))

        self.logger.info("cutting down IATs to make traces that really are the same duration as the sampling window")
        for fn in list(self.iats.keys()):
            valids = self.iats[fn].cumsum() <= (60 * (self.mins[-1] - self.mins[0] + 1))
            if not valids.any():
                del self.iats[fn]
            else:
                self.iats[fn] = self.iats[fn][valids]
        self.logger.info(
            "cut down IATs series to the sampling window of %ds, now down to %d functions",
            60 * (self.mins[-1] - self.mins[0] + 1),
            len(self.iats),
        )

        for fn, iat in self.iats.items():
            if iat.sum() > 60 * (self.mins[-1] - self.mins[0] + 1):
                print(f"function {fn} runs longer than the sampling window")

        if self.logger.isEnabledFor(logging.DEBUG):
            first_fn = list(self.iats.keys())[0]
            self.iats[first_fn].info(buf=sys.stderr)
            self.logger.debug("10 IATs from the first function:\n%s", self.iats[first_fn].head(10))

        self.report_IATs()

    def report_IATs(self):
        # Number of invocations, if the function was invoked once per hour.
        nb_invocs_once_hour = 1 * (self.mins[-1] - self.mins[0] + 1) / 60.0
        # Number of invocations, if the function was invoked once per minute.
        nb_invocs_once_minute = self.mins[-1] - self.mins[0] + 1

        nb_invocs = (
            pd.Series(
                [len(iats) for iats in self.iats.values()],
                index=pd.MultiIndex.from_tuples(self.iats.keys(), names=("HashOwner", "HashApp", "HashFunction")),
            )
            .groupby("HashApp")
            .sum()
        )

        self.report["iats"]["frac_lessthan_oncehour"] = len(nb_invocs[nb_invocs < nb_invocs_once_hour]) / len(nb_invocs)
        self.report["iats"]["frac_lessthan_onceminute"] = len(nb_invocs[nb_invocs < nb_invocs_once_minute]) / len(
            nb_invocs
        )
        self.report["functions"] = len(self.iats)

    def generate_memory_per_function(self):
        if not self.processed:
            raise ValueError("process the traces first")

        self.logger.info("generating memory per function")

        self.fn_memory = (
            self.data["AverageAllocatedMb"] * self.data["Average"] / self.data.groupby("HashApp")["Average"].sum()
        )

        self.logger.info("generated memory per function")
        if self.logger.isEnabledFor(logging.DEBUG):
            self.fn_memory.info(buf=sys.stderr)
            self.logger.debug("10 rows of data with memory per function:\n%s", self.fn_memory.head(10))

        self.report_memory_per_function()

    def report_memory_per_function(self):
        mem_per_app = self.fn_memory.groupby("HashApp").sum()
        fit_res = sp.stats.burr12.fit(mem_per_app)
        self.report["memory"]["c"], self.report["memory"]["k"], _, self.report["memory"]["lambd"] = (
            p.item() for p in fit_res
        )

    def make_FaaSLoad_traces(self, dummy_function: str = "faasload/go_dummy") -> FaaSLoadTraces:
        self.logger.info("making FaaSLoad traces")

        if not self.processed or self.iats is None or self.fn_memory is None:
            raise ValueError("process the traces first, then generate IATs and memory per function")

        ret = {
            (user, fn): FaaSLoadTrace(
                user=user,
                function=dummy_function,
                memory=64 * ((self.data.loc[(user, app, fn), "AverageAllocatedMb"] + 64) // 64),
                points=pd.DataFrame(
                    {
                        "IAT": iats,
                        "toWait": self.data.loc[(user, app, fn), "Average"],
                        "toAllocate": self.fn_memory.loc[(user, app, fn)] * 1024,
                    }
                ),
            )
            for (user, app, fn), iats in self.iats.items()
        }

        self.logger.info("made %d FaaSLoad traces", len(ret))

        return ret

    def print_report(self):
        print(
            f"Parameters of log-normal fit for function durations: µ = {self.report['durations']['mu']:.2f}, σ = {self.report['durations']['sigma']:.2f}"
        )
        print("Reference from Microsoft paper: log mean = -0.38, σ = 2.36")

        print(f"Generated traces for {self.report['functions']} functions")
        print(
            f"Apps invoked once per hour or less on average: {self.report['iats']['frac_lessthan_oncehour']:.0%} "
            "(reference: 45%)"
        )
        print(
            f"Apps invoked once per minute or less on average: {self.report['iats']['frac_lessthan_onceminute']:.0%} "
            "(reference: 81%)"
        )

        print(
            f"Parameters of Burr fit for memory per application: c = {self.report['memory']['c']:.3f}, "
            f"k = {self.report['memory']['k']:.3f}, λ = {self.report['memory']['lambd']:.3f}"
        )
        print("Reference: c = 11.652, k = 0.221, λ = 107.083")

    def write_report(self, dest_dp: str):
        with open(path.join(dest_dp, "stats_report.yaml"), "w") as report_f:
            yaml.dump(self.report, report_f)


def write_FaaSLoad_traces(traces: FaaSLoadTraces, dest_dp: str):
    for (user, fn), trace in traces.items():
        with open(path.join(dest_dp, f"{user}_{fn}.faas"), "w") as trace_f:
            trace_f.write(f"{user}\t{trace.function}\t{trace.memory}\n")
            trace_f.writelines(
                f"{p.IAT}\ttoWait:{p.toWait}\ttoAllocate:{p.toAllocate}\n" for p in trace.points.itertuples(index=False)
            )


def write_params(args, dest_dp: str):
    with open(path.join(dest_dp, "conversion.yaml"), "w") as params_f:
        yaml.dump(
            {
                "window_from": args.window_start,
                "window_to": args.window_end,
                "mininvocs_perday": args.min_invocs_per_day,
                "rngseed": args.rng_seed,
                "dummyfunction": args.dummy_function,
            },
            params_f,
        )


def main():
    argparser = ArgumentParser()
    argparser.add_argument("traces_dp", help="Path to directory of Azure traces")
    argparser.add_argument(
        "window_start",
        help=f"Start of sampling time window (minutes, between 1 and {ALL_DAYS[-1]*1440}, inclusive)",
        type=int,
        default=1,
    )
    argparser.add_argument(
        "window_end",
        help=f"End of sampling time window (minutes, between 1 and {ALL_DAYS[-1]*1440}, inclusive)",
        type=int,
        default=ALL_DAYS[-1] * 1440,
    )
    argparser.add_argument(
        "dest_dp",
        help="Destination (directory path) to write output traces; it may exist but must be empty",
    )
    argparser.add_argument(
        "--min-invocs-per-day",
        help="Minimum number of invocations per day to sample a function",
        type=float,
        default=0.0,
    )
    argparser.add_argument(
        "-v",
        "--verbosity",
        help="Set logging level (default: WARNING)",
        default="WARNING",
        choices=("DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"),
    )
    argparser.add_argument(
        "-R",
        "--rng-seed",
        help="Seed for all Random Number Generators",
        type=int,
        default=20191108,
    )
    argparser.add_argument(
        "-D",
        "--dummy-function",
        help="Fully-qualified name of the dummy function",
        default="faasload/go_dummy",
    )
    argparser.add_argument(
        "-S",
        "--sample-down",
        help="Sample down selected functions by selecting a percentage of applications",
        type=float,
        default=1.0,
    )
    argparser.add_argument(
        "-n",
        "--report-only",
        action="store_true",
        help="Do not write FaasLoad traces, only write the report",
    )

    args = argparser.parse_args()

    logging.basicConfig(level=args.verbosity, format="[%(asctime)s][%(levelname)s] %(message)s")

    az_traces = AzureTraces(
        traces_dp=args.traces_dp,
        sampling_win=(args.window_start, args.window_end),
        min_invocs_per_day=args.min_invocs_per_day,
        rng_seed=args.rng_seed,
        sample_down=args.sample_down,
    )
    az_traces.load_and_process()
    az_traces.generate_IATs()
    az_traces.generate_memory_per_function()

    az_traces.print_report()

    dest_dp = path.normpath(args.dest_dp)
    os.makedirs(dest_dp, exist_ok=True)

    if os.listdir(dest_dp):
        print(f"destination directory `{dest_dp}` not empty", file=sys.stderr)
        sys.exit(1)

    if not args.report_only:
        fl_traces = az_traces.make_FaaSLoad_traces(dummy_function=args.dummy_function)

        write_FaaSLoad_traces(fl_traces, dest_dp)

    write_params(args, dest_dp)
    az_traces.write_report(dest_dp)


if __name__ == "__main__":
    main()
