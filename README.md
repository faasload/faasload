# FaaSLoad: Function-as-a-Service workload injector

The project's goal is to inject a workload of serverless function invocations to a Function-as-a-Service (FaaS) platform, and to gather data about the execution of the workload.

FaaSLoad currently supports [Apache OpenWhisk](https://openwhisk.apache.org/) and [Knative](https://knative.dev/docs/).
_FaaSLoad was first developed to support Apache OpenWhisk, so the support for Knative may be less stable._

Read below for first instructions on how to install and use FaaSLoad on a FaaS platform deployed with Kubernetes over multiple nodes.

Keep in mind the document [Troubleshooting](docs/troubleshooting.md), that may be useful in time.

## Overview

This project has two usage modes:

 1. as a **workload injector**: run traces describing invocations of serverless functions of many users in the FaaS platform, optionally monitoring their execution time, resource usage, hardware performance counters, etc.;
 2. as a **dataset generator**: run serverless functions in the FaaS platform and monitor their execution, generating a dataset of execution time, resource usage, hardware performance counters, etc.

The injector mode can be used to evaluate the FaaS platform, by observing its behavior under real and synthesized workloads of varying profiles.
The generator mode can be used to produce a dataset of function invocations, for example to train a machine learning model.

## Installation

FaaS platforms are preferably deployed on Kubernetes, and so is FaaSLoad.

**Warning:** FaaSLoad's monitor component requires Docker to work, so your Kubernetes worker nodes must use Docker (instead of direct CRI invocations).
See [Kubernetes documentation on creating a cluster](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/) for more information.
It means that **FaaSLoad's monitor is not available on kind clusters.**

It can be deployed on [kind](https://kind.sigs.k8s.io/) for test purposes, or on a production Kubernetes cluster.

### Quick overview

1. deploy the FaaS platform using the deployment script appropriate to your Kubernetes cluster, from [FaaSLoad's platform configurations repository](https://gitlab.com/faasload/platform-configurations);
2. setup a PostgreSQL database for FaaSLoad;
3. if enabled, configure and deploy FaaSLoad's monitor on all Kubernetes worker nodes used to invoke cloud functions using the deployment script appropriate to your Kubernetes cluster, from [FaaSLoad's platform configurations repository](https://gitlab.com/faasload/platform-configurations).

### FaaS platform setup

First you need a working deployment of the FaaS platform you want to target.

While FaaSLoad does not require any functional change to the platform, some runtime configuration tweaks are needed.
Fetch the custom configuration from [FaaSLoad's repository of platform configurations](https://gitlab.com/faasload/platform-configurations), and run the appropriate script as shown below:

```shell
# Export FAASLOAD_PLATFORM to set the targeted platform.
# This is used during the setup, and when running FaaSLoad itself.
# THE_PLATFORM can be "openwhisk", or "knative".
export FAASLOAD_PLATFORM=THE_PLATFORM
git clone https://gitlab.com/faasload/platform-configurations.git
# See the help message of the deployment script with the flag --help for further information and customization.
pushd "platform-configurations/$FAASLOAD_PLATFORM" && ./deploy.sh && popd
```

Please refer to the [README of FaaSLoad's repository of platform configurations](https://gitlab.com/faasload/platform-configurations/-/blob/main/README.md) for more details and troubleshooting.

### Database setup

FaaSLoad needs a [TimeScale database](https://www.timescale.com/), which is a [PostgreSQL](https://www.postgresql.org/) database server to store the data it produces, with an optimisation to store timeseries (the resource usage of the functions obtained by the monitor).
You are free to set it up as you wish, the only requirement is to prepare a database and a user for FaaSLoad.
The server host, the database name, and the user credentials are configured in [loader.yml](config/loader.yml).

A quick way to set it up is to use a Docker container:

```shell
# Reachable at localhost:5432, at the database "faasload", with the user "faasload", password "faasload".
# Uses a managed voume "db_data".
# Disable the telemetry, see: https://docs.timescale.com/self-hosted/latest/configuration/telemetry/#disable-telemetry.
docker run --name faasload-db --detach\
    --volume db_data:/var/lib/postgresql/data \
    --publish 5432:5432 \
    --env "POSTGRES_USER=faasload" --env "POSTGRES_PASSWORD=faasload" --env "POSTGRES_DB=faasload" \
    timescale/timescaledb:latest-pg14 -c timescaledb.telemetry_level=off
```

### FaaSLoad's monitor

**FaaSLoad's monitor is not available on a kind cluster.**
Check the warning at the beginning of this "Installation" section for more information.

FaaSLoad optionally collects resource usage, hardware performance counters, etc. via its [monitor](docs/monitor.md) (basic information such as the execution time of function invocations is always collected).

The monitor acts at the host level, so instances of the monitor must run on every Kubernetes worker node where functions will be executed. This is easy to achieve by running the instances as pods deployed on matching worker nodes.

First, build the container image of FaaSLoad's monitor:

```shell
# Execute from the base folder of the repository.
# The monitor image, tagged "local" to not use "latest" (see below).
# Notice the two build arguments (see below).
docker build . --file docker/monitor.Dockerfile --tag faasload/monitor:local\
    --build-arg kernel_release="$(uname -r | cut -d'-' -f1)"\
    --build-arg docker_group_id="$(getent group docker | cut -d":" -f3)"
```

If needed, publish the image to the image registry used by your Kubernetes cluster;
alternatively, just run the build command on every Kubernetes worker node.

**Warning:** building the monitor container image requires two building arguments that reflect the environment where they will run, i.e., _the Kubernetes worker nodes_.
In the build command above, the arguments are taken from the current environment where the image is built, so **you may need to correct them** to match those of the worker nodes.

* `kernel_release`: the release of the Linux kernel of the host worker nodes, this is used to compile the matching version of [perf](https://www.brendangregg.com/perf.html) to collect hardware performance counters;
* `docker_group_ip`: the ID of the user group "docker" on the host worker nodes, this is used to authorize the monitor to access the Docker socket to catch creation of function runtimes to monitor them.

Then, edit the configuration of the monitor as "$HOME/.config/faasload/monitor.yml";
no configuration change from the default values is required, however the file is required: simply copy the default template from [config/monitor.yml](config/monitor.yml).
See ["Monitor"](docs/monitor.md) for more information about the monitor.

The deployment script used next will read and deploy the configuration as a ConfigMap object in the Kubernetes cluster.
Any change afterward will require you to **destroy the ConfigMap**, and run the deployment script again.

Finally, deploy monitor pods on each worker node: run the appropriate script from [FaaSLoad's repository of platform configurations](https://gitlab.com/faasload/platform-configurations) as shown below:

```shell
# Assuming platform-configurations is already cloned.
# See the help message of the deployment script with the flag --help for further information and customization.
pushd "platform-configurations/$FAASLOAD_PLATFORM" && ./deploy-monitor.sh && popd
```

## Usage

Before running anything related to FaaSLoad, install and enable the Python virtualenv using pipenv:

```shell
# From FaaSLoad's directory
pipenv install
```

All Python scripts and command must be run under this virtualenv, either using `pipenv run CMD`, or by spawning a shell with the virtualenv enabled:

```shell
pipenv shell
```

Now, you are ready to go: refer to the documentation of the ["Injector mode"](docs/injector) or to the documentation of the ["Generator mode"](docs/generator) for instructions about the setup, configuration, usage and data analysis of the two modes.

The sections below give general information about the project's architecture and usage.

## Architecture overview

There are two components; they are not linked to the two usage modes, and are used in both modes:

 1. the **loader**: invoke functions in the FaaS platform, and get information about their executions from the platform and from the monitor (the other component);
 2. the **monitor**: collect supplementary data about functions' executions and Docker operations, which is retrieved by the loader.

The main component is the loader, and it is used both to inject a workload into the FaaS platform, and to generate a dataset of function executions.
The monitor exists so that the task of collecting resource usage of the invoked functions, and keeping a log of Docker operations on the nodes of the FaaS platform, is offloaded, and the resource usage and log of Docker operations can be requested after the fact.

The system is illustrated below (a deeper architecture description is given in the ["Architecture" document](docs/dev/architecture.md)).

![Architecture overview](docs/_fig/high-level-architecture.png)

The two components are implemented as Python modules, which can be `import`ed, or executed from a terminal with `python -m`.
The preferred method is to run the loader like this: `python -m faasload.loader`;
and to deploy the monitor as a pod on every node of the FaaS platform, as explained in the section "FaaSLoad's monitor" above.

The loader is configured through its YAML config file, looked up at _$HOME/.config/faasload/loader.yml_;
while the monitor reads a ConfigMap from Kubernetes, created from its configuration file at _$HOME/.config/faasload/monitor.yml_.

Moreover, in the dataset generator mode, FaaSLoad includes a checkpointing feature: when interrupted (SIGINT and SIGTERM) or after finishing its job, it stores its internal state (including random state).
It is then possible to restart the generation from where it stopped.
See "Checkpointing" in ["Dataset generator mode"](docs/generator) for more information.

## Authors

Work published under the [CeCILL-B license](http://www.cecill.info), see [license text](LICENSE) (also [in French](LICENSE.fr)).

* Copyright Toulouse INP / IRIT / CNRS, 2019 - 2020
* Copyright Télécom SudParis / IP Paris, 2020 - 2025

Main contributor: Mathieu Bacou (Télécom SudParis)

See [full list of contributors](CONTRIBUTORS).
