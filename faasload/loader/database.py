"""Facilities for access and manipulation of FaaSLoad's database.

FaaSLoad uses PostgreSQL as its DBMS.
The API defined in this file uses SQLAlchemy, with the backend psycopg2.
"""

# Copyright Toulouse INP / IRIT / CNRS, 2019 - 2020
# Copyright Télécom SudParis / IP Paris, 2021 - 2025
# Mathieu Bacou and other contributors
#
# mathieu.bacou@telecom-sudparis.eu
#
# This software is a computer program whose purpose is to inject a workload
# to a Function-as-a-Service platform, and to gather data about its execution.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software. You can use,
# modify and/ or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.

from __future__ import annotations

import enum
import json
from collections.abc import Iterable
from datetime import datetime
from typing import Any, NamedTuple, Sequence, Collection

import yaml
from sqlalchemy import Boolean, Column, create_engine, DateTime, DDL, delete, Enum, event, Float, ForeignKey, insert, \
    select, update, INT, text
from sqlalchemy.dialects.postgresql import BIGINT, CHAR, VARCHAR
from sqlalchemy.engine import Row
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm import sessionmaker, DeclarativeBase
from sqlalchemy.sql import Delete, Insert, Select, Update

from faasload.docker_monitor import DockerOpsLogServerConfiguration
from faasload.docker_monitor.docker_operations import DockerOperationLogEntry, DockerOperation
from faasload.docker_monitor.monitor import Measurement
from . import DatabaseConfiguration

class FaaSLoadBaseTable(DeclarativeBase):
    pass


class InputKind(enum.Enum):
    audio = 'audio'
    image = 'image'
    video = 'video'
    text = 'text'
    none = 'none'


class Function(FaaSLoadBaseTable):
    __tablename__ = 'functions'

    id = Column(BIGINT, nullable=False, autoincrement=True, primary_key=True)
    name = Column(VARCHAR(256), index=True)
    runtime = Column(VARCHAR(64))
    image = Column(VARCHAR(256))
    parameters = Column(VARCHAR(2048))
    input_kind = Column(Enum(InputKind))


class Run(FaaSLoadBaseTable):
    __tablename__ = 'runs'

    id = Column(BIGINT, nullable=False, autoincrement=True, primary_key=True)
    function = Column(BIGINT, ForeignKey(Function.id, ondelete='CASCADE'), nullable=False)
    namespace = Column(VARCHAR(256))
    activation_id = Column(CHAR(32), index=True)
    failed = Column(Boolean)
    start_ms = Column(BIGINT)
    end_ms = Column(BIGINT)
    wait_ms = Column(BIGINT)
    init_ms = Column(BIGINT)
    error_msg = Column(VARCHAR(64))


class Parameter(FaaSLoadBaseTable):
    __tablename__ = 'parameters'

    id = Column(BIGINT, nullable=False, autoincrement=True, primary_key=True)
    run = Column(BIGINT, ForeignKey(Run.id, ondelete='CASCADE'), nullable=False)
    name = Column(VARCHAR(256), nullable=False)
    value = Column(VARCHAR(2048))


class Result(FaaSLoadBaseTable):
    __tablename__ = 'results'

    id = Column(BIGINT, nullable=False, autoincrement=True, primary_key=True)
    run = Column(BIGINT, ForeignKey(Run.id, ondelete='CASCADE'), nullable=False)
    name = Column(VARCHAR(256), nullable=False)
    value = Column(VARCHAR(2048))


class Resource(FaaSLoadBaseTable):
    __tablename__ = 'resources'

    id = Column(BIGINT, nullable=False, autoincrement=True, primary_key=True)
    run = Column(BIGINT, ForeignKey(Run.id, ondelete='CASCADE'), nullable=False)
    timestamp_ms = Column(DateTime)
    counter = Column(VARCHAR(64), nullable=False)
    value = Column(Float)


class DockerOperations(FaaSLoadBaseTable):
    __tablename__ = 'dockeroperations'

    id = Column(BIGINT, nullable=False, autoincrement=True, primary_key=True)
    address = Column(VARCHAR(15), nullable=False)
    port = Column(INT)
    timestamp_ms = Column(BIGINT)
    docker_name = Column(VARCHAR(256))
    op = Column(Enum(DockerOperation))


WIPE_TABLES = text(f"""
    TRUNCATE TABLE "{Parameter.__tablename__}", "{Resource.__tablename__}", "{Run.__tablename__}",
    "{Result.__tablename__}", "{DockerOperations.__tablename__}";
""")


# Pickle-able type reflecting result from the database.
class FunctionRecord(NamedTuple):
    id: int
    name: str
    parameters: Any
    input_kind: Any


class FaaSLoadDatabase:
    """Client facility to manipulate the database to store the monitoring data.

    Upon initialization, it creates if needed, the tables filled by the injector: "runs", "results", "parameters",
    "resources" and "dockeroperations".

    This class holds a connection to the database, and provides high-level operations for the loader.
    All the writing operations try to commit the transaction themselves, or rollback it upon failure (i.e., nothing is
    written or deleted upon error, and the tables are left unchanged).

    All methods raise :py:class:`FaaSLoadDatabaseException` when problems occur.
    """

    def __init__(self, cfg: DatabaseConfiguration):
        """Initialize a new client to the database.

        :param cfg: the database configuration
        """
        self.engine = create_engine(
            f'postgresql+psycopg2://{cfg["user"]}:{cfg["password"]}@{cfg["host"]}:5432/{cfg["database"]}',
            pool_size=20, max_overflow=100
        )
        self.session_maker: sessionmaker = sessionmaker(self.engine)

    def load_database(self):
        """Create and setup the tables."""
        FaaSLoadBaseTable.metadata.create_all(self.engine)
        event.listen(
            Function.__table__,
            'after_create',
            DDL(f"""
                SELECT create_hypertable(
                    '{Resource.__tablename__}',
                    'timestamp_ms'
                )
            """)
        )

    def create_functions_table(self):
        """Create the "functions" table.

        Used by the setup scripts to load assets before running FaaSLoad.
        """
        Function.__table__.create(self.engine, checkfirst=True)

    def drop_functions_table(self):
        """Drop the "functions" table.

        Used by the setup scripts to clean up assets.
        """
        Function.__table__.drop(self.engine)

    def delete_lost_runs(self, state: Collection['InjectorState']):
        """Delete lost runs, i.e. delete dangling partial run records.

        When interrupting the dataset generation, a checkpointed state is created, that stores the ID of the last
        complete injection, i.e., the last injection for which the activation monitor stored complete data.
        This method is used in dataset generation mode, when restarting from a checkpointed state.
        It removes partial run records that are never going to be completed, because the generation was interrupted
        before the activation monitor completed them.

        :param state: states of injectors for which to delete lost runs

        :raises FaaSLoadDatabaseException: the operation failed

        They are identified by their IDs being strictly greater than the last run of the injector states.

        There are two security measures in this method that prevent its execution and raise an exception, if:

        1. more than 2 records would be deleted for a given injector (to avoid losing too much data)
        2. the injector executed more runs than there are records (meaning the checkpointed state does not match the
           database)

        This is to prevent accidents like reloading an old state by mistake.
        """
        try:
            with self.session_maker.begin() as session:
                for inj_state in state:
                    # security to avoid wiping the table on bad state reloading
                    smt: Select = select(Run).where((Run.function == inj_state.trace_state.function.id) &
                                                    (Run.namespace == inj_state.trace_state.user))
                    nb_runs: int = len(session.execute(smt).all())

                    if inj_state.last_run > nb_runs:
                        raise FaaSLoadDatabaseException(
                            f'last run ID of injector of {inj_state.trace_state.function.name} of user '
                            f'{inj_state.trace_state.user} is greater than the number of runs of this injector (wrong state '
                            'restored?): aborting')
                    if nb_runs - inj_state.last_run > 2:
                        raise FaaSLoadDatabaseException(
                            f'deleting lost runs of injector of {inj_state.trace_state.function.name} of user '
                            f'{inj_state.trace_state.user} would delete more than 2 records: aborting')

                    stmt: Delete = delete(Run).where((Run.function == inj_state.trace_state.function.id) &
                                                         (Run.namespace == inj_state.trace_state.user) &
                                                         (Run.id > inj_state.global_last_run))
                    session.execute(stmt)
        except SQLAlchemyError as err:
            raise FaaSLoadDatabaseException from err

    def wipe(self):
        """Wipe all tables filled by the injector: runs, results, parameters, resources and dockerops.

        :raises FaaSLoadDatabaseException: the operation failed
        """
        try:
            with self.session_maker.begin() as session:
                session.execute(WIPE_TABLES)
        except SQLAlchemyError as err:
            raise FaaSLoadDatabaseException from err

    def select_functions(self) -> Sequence[FunctionRecord]:
        """Select all functions for iteration.

        :returns: the list of function records

        :raises FaaSLoadDatabaseException: the operation failed

        As a convenience, the field of parameters is parsed from its YAML string representation.
        """
        smt: Select = select(Function.id, Function.name, Function.parameters, Function.input_kind)
        try:
            with self.session_maker.begin() as session:
                return [_function_row2namedtuple(func) for func in session.execute(smt)]
        except SQLAlchemyError as err:
            raise FaaSLoadDatabaseException from err

    def select_function(self, func_name: str) -> FunctionRecord:
        """Select a function by its partial name.

        :param func_name: partial name of the function

        :returns: the record of the function

        :raises FaaSLoadDatabaseException: more than one function matched the partial name, or the operation failed

        Allows to retrieve the function by name without specifying the package name.

        As a convenience, the field of parameters is parsed from its YAML string representation.
        """
        smt: Select = select(Function.id, Function.name, Function.parameters,
                                     Function.input_kind).where(Function.name == func_name)
        try:
            with self.session_maker.begin() as session:
                results = session.execute(smt).all()
                if len(results) > 1:
                    raise FaaSLoadDatabaseException(f'ambiguous function name {func_name}')
                result = results[0]
                return _function_row2namedtuple(result)
        except SQLAlchemyError as err:
            raise FaaSLoadDatabaseException from err

    def insert_partial_run(self, run_data: dict[str, Any]) -> int:
        """Store partial metadata about a run.

        :param run_data: the data of the run

        :returns: the ID of the newly inserted run in the table 'runs'

        :raises FaaSLoadDatabaseException: the operation failed

        Expected fields of `run_data`:

        * function_id: ID of the function of the run (foreign key to table "functions")
        * namespace: namespace of the run (the user)
        * activation_id: the ID of the invocation in the FaaS platform

        See the developer documentation about the architecture for more information about inserting a partial run.
        """
        run = Run(
            function=run_data['function_id'],
            namespace=run_data['namespace'],
            activation_id=run_data['activation_id']
        )
        try:
            with self.session_maker() as session:
                with session.begin():
                    session.add(run)
                return run.id
        except SQLAlchemyError as err:
            raise FaaSLoadDatabaseException from err

    def insert_parameters(self, run_id: int, params: dict[str, Any]):
        """Store parameter values of a run.

        :param run_id: the ID of the run which parameters are stored
        :param params: the parameter values (see below)

        :raises FaaSLoadDatabaseException: the operation failed

        `params` is a dict mapping parameter names to their values.
        """
        try:
            with self.session_maker.begin() as session:
                for name, value in params.items():
                    smt: Insert = insert(Parameter).values(run=run_id, name=name, value=json.dumps(value))
                    session.execute(smt)
        except SQLAlchemyError as err:
            raise FaaSLoadDatabaseException from err

    def update_activation(self, act_id: str, run_data: dict[str, Any]):
        """Store remaining metadata of a partial run.

        :param act_id: the invocation ID of the run to complete (matching the one from the partial run)
        :param run_data: the complete data of the run

        :raises FaaSLoadDatabaseException: the operation failed

        See also :py:func:`insert_partial_run()`.

        Expected field of `run_data`:

        * failed: whether the run failed executing (failure state returned by the FaaS platform)
        * start_ms: timestamp of the start of the execution (defined by the FaaS platform, in milliseconds)
        * end_ms: timestamp of the end of the execution (defined by the FaaS platform, in milliseconds)
        * wait_ms: duration the invocation request spent in the FaaS platform before starting its execution (defined by
          the FaaS platform, in milliseconds)
        * init_ms: duration of initializing the runtime for the invocation (defined by the FaaS platform, in
          milliseconds), i.e., latency incurred during a cold start to provision a runtime
          * may be `None`, which is interpreted as the run being served as a warm start (the platform reused a runtime)
        * error_msg: a FaaSLoad-specific error message returned by the function in its error result (optional)
        """
        run_data['activation_id'] = act_id
        smt: Update = update(Run).where(Run.activation_id == act_id).values(
            failed=run_data['failed'],
            start_ms=run_data['start'],
            end_ms=run_data['end'],
            wait_ms=run_data['wait_time'],
            init_ms=run_data['init_time'],
            error_msg=run_data['error_msg']
        )
        try:
            with self.session_maker.begin() as session:
                if session.execute(smt).rowcount == 0:
                    raise FaaSLoadDatabaseException('partial record not found')
        except SQLAlchemyError as err:
            raise FaaSLoadDatabaseException from err

    def get_run_id(self, act_id: str) -> int | None:
        stmt = select(Run.id).where(Run.activation_id == act_id)
        try:
            with self.session_maker.begin() as session:
                return session.execute(stmt).scalar()
        except SQLAlchemyError as err:
            raise FaaSLoadDatabaseException from err

    def insert_result(self, act_id: str, res: dict[str, Any]):
        """Store the result returned by an invocation.

        :param act_id: the invocation ID of the run which result is stored
        :param res: the result of the run function

        :raisess FaaSLoadDatabaseException: the operation failed

        Each value of `res` is stored as a row, converted to a string.
        """
        try:
            run_id = self.get_run_id(act_id)
        except FaaSLoadDatabaseException as err:
            raise FaaSLoadDatabaseException("failed getting run ID from activation ID") from err
        if run_id is None:
            raise FaaSLoadDatabaseException('run not found')

        try:
            with self.session_maker.begin() as session:
                for res_name, res_val in res.items():
                    stmt = insert(Result).values(
                        run=run_id,
                        name=str(res_name),
                        value=json.dumps(res_val))
                    session.execute(stmt)
        except SQLAlchemyError as err:
            raise FaaSLoadDatabaseException from err

    def insert_function(self, fn: dict[str, Any]):
        """Insert a function to run with FaaSLoad.

        Used during the setup of FaaSLoad (e.g. by the script _load-wsk-assets.py_).

        :param fn: the function to insert

        Expected fields of `fn`:

        * name: the name of the function (including a package)
        * runtime: the runtime of the function (defined by the FaaS platform)
          * may be `blackbox` if it is a custom runtime image
        * image: the Docker image of the function's runtime if it is a custom image (defined by the FaaS platform)
          * may be `None` if the runtime is not `blackbox`
        * parameters: a dict describing the function's parameters (a read from the manifest of functions)
        * input_kind: the kind of inputs the function accepts (`image`, `video`, `audio`, etc., or `none` if no input)
        """
        smt: Insert = insert(Function).values(
            name=fn['name'],
            runtime=fn['runtime'],
            image=fn['image'],
            parameters=fn['parameters'],
            input_kind=fn['input_kind'])
        try:
            with self.session_maker.begin() as session:
                session.execute(smt)
        except SQLAlchemyError as err:
            raise FaaSLoadDatabaseException from err

    def insert_resources(self, act_id: str, res: Iterable[Measurement]):
        """Store resource usage measurements of a run.

        :param act_id: the invocation ID of the run which resource usage measurements are stored
        :param res: the resource usage measurements (see below)

        :raises FaaSLoadDatabaseException: the operation failed

        `res` is a list of tuples with three fields:

        * date: the timestamp of the measurement, in seconds, rounded to the millisecond
        * counter: the name of the resource or performance counter
        * value: the value for the counter at this date
        """
        try:
            run_id = self.get_run_id(act_id)
        except FaaSLoadDatabaseException as err:
            raise FaaSLoadDatabaseException("failed getting run ID from activation ID") from err
        if run_id is None:
            raise FaaSLoadDatabaseException('run not found')

        try:
            with self.session_maker.begin() as session:
                for date, counter, value in res:
                    smt: Insert = insert(Resource).values(
                        run=run_id,
                        timestamp_ms=datetime.fromtimestamp(date),
                        counter=counter,
                        value=value,
                    )
                    session.execute(smt)
        except SQLAlchemyError as err:
            raise FaaSLoadDatabaseException from err

    def insert_docker_operations_log(self,
                                     mon: DockerOpsLogServerConfiguration,
                                     log: Sequence[DockerOperationLogEntry]):
        """Store the log of Docker operations of a workload injection.

        :param mon: the configuration of the Docker operations log server that provided the log
        :param log: the list of log entries to store

        `mon` is used to store the address and port of the monitor, to identify the Kubernetes node where the logged
        Docker operations happened.
        """
        try:
            with self.session_maker.begin() as session:
                for date, docker_name, op in log:
                    smt = insert(DockerOperations).values(
                        address=mon['address'],
                        port=mon['port'],
                        timestamp_ms=date,
                        docker_name=docker_name,
                        op=op.name,
                    )
                    session.execute(smt)
        except SQLAlchemyError as err:
            raise FaaSLoadDatabaseException from err


def _function_row2namedtuple(func_rec: Row):
    return FunctionRecord(id=func_rec[0], name=func_rec[1], parameters=yaml.full_load(func_rec[2]),
                          input_kind=func_rec[3].value)


class FaaSLoadDatabaseException(Exception):
    """A database operation failed.

    Used as a wrapper around the low-level Error raised by the connector to MySQL databases.
    """
    pass
