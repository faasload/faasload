"""Activation monitor - Monitor termination of invocations.

The main class is :py:class:`ActivationMonitor`: the abstract base class to implement platform-dependent activation
monitors.

Then, the platform-dependent activation monitors are:

* :py:class:`OpenwhiskActivationMonitor`: for Apache OpenWhisk
* :py:class:`KnativeActivationMonitor`: for Knative

Two functions exist for the activation monitors to fetch data from the Docker monitors:

* :py:func:`get_resource_usage()`: fetch resource usage of a container from a Docker monitor
* :py:func:`get_get_docker_operations_log()`: fetch log of Docker operations from a Docker monitor

The activation monitor mainly manipulates instances of :py:class:`Activation`, i.e., invocations of functions.

The activation monitor notifies the injector thread with an instance of :py:class:`ActivationMessage`, that contains a
member of the enumeration :py:class`ActivationMonitorNotification` to indicate the notification type.
"""

# Copyright Toulouse INP / IRIT / CNRS, 2019 - 2020
# Copyright Télécom SudParis / IP Paris, 2021 - 2025
# Mathieu Bacou and other contributors
#
# mathieu.bacou@telecom-sudparis.eu
#
# This software is a computer program whose purpose is to inject a workload
# to a Function-as-a-Service platform, and to gather data about its execution.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software. You can use,
# modify and/ or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.

from __future__ import annotations

import json
import logging
import socket
import time
from abc import ABC, abstractmethod
from collections.abc import Sequence
from enum import auto as auto_enum, Enum
from json.decoder import JSONDecodeError
from queue import SimpleQueue
from threading import Event, Thread
from typing import Any, List, NamedTuple, Optional, TypedDict, Collection

from pywhisk.admin import OpenWhiskAdminException
from pywhisk.client import AdministrationConfiguration as OpenWhiskAdministrationConfiguration

from faasload.docker_monitor.docker_operations import DockerOperationLogEntry, DockerOperation
from faasload.docker_monitor.monitor import Measurement
from . import DockerMonitorConfiguration, KnativeConfiguration, OpenWhiskConfiguration
from .database import FaaSLoadDatabase, FaaSLoadDatabaseException
from ..docker_monitor import MonitorServerConfiguration, DockerOpsLogServerConfiguration


class ActivationMonitorNotification(Enum):
    """Types of notification sent by the activation monitor to the injector.

    * `ActivationDataFetched`: data for an invocation has been fetched and written to database
    """
    ActivationDataFetched = auto_enum()


class ActivationMessage(TypedDict):
    """Notification message sent by the activation monitor to the injector.
    
    * `msg`: the type of notification
    * `activation`: the ID of the activation subject of this notification
    """
    msg: ActivationMonitorNotification
    activation: Activation


class ActivationAnnotation(TypedDict):
    """An annotation in an invocation."""
    key: str
    value: Any


class ActivationResponse(NamedTuple):
    """The response of the platform to an invocation (i.e., was it successful, with what result from the function)."""
    result: Any
    success: bool


class ActivationMonitorError(Exception):
    """The activation monitor encountered a critical error and cannot continue monitoring."""
    pass


class Activation(NamedTuple):
    """An invocation.
    
    * `activationId`: the ID of the activation in the platform
    * `dockerId`: the ID of the Docker container that ran the invocation
    * `namespace`: the namespace in the platform where the invocation ran
    * `response`: the response of the platform to the invocation
    * `start`: the timestamp of the start of the invocation, as registered by the platform
    * `end`: the timestamp of the end of the invocation, as registered by the platform
    * `act_error_msg`: an error message directed to FaaSLoad by the function in case of error (optional)
    * `annotations`: free-form annotations to the activation
    """
    activationId: str
    dockerId: Optional[str]
    namespace: str
    response: ActivationResponse
    start: int
    end: int
    act_error_msg: Optional[str]
    annotations: List[ActivationAnnotation]


def get_resource_usage(measurement_srv: MonitorServerConfiguration,
                       cont_id: str,
                       start: int, end: int) -> Sequence[Measurement]:
    """Fetch resource usage of a container from one Docker monitor.

    :param measurement_srv: host and port of the measurements server to request resource usage from
    :param cont_id: ID of the container
    :param start: start date of resource usage period to fetch (timestamp in milliseconds)
    :param end: end date of resource usage period to fetch (timestamp in milliseconds)

    :returns: list of measurements

    :raises ValueError: if the container ID was not found in the Docker monitor
    """
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((measurement_srv['address'], measurement_srv['port']))

    sock.sendall(json.dumps({
        'container': cont_id,
        'start':     round(start / 1000, 3),
        'end':       round(end / 1000, 3),
    }).encode())
    sock.shutdown(socket.SHUT_WR)

    meas = b''
    buff = sock.recv(4096)
    while buff:
        meas += buff
        buff = sock.recv(4096)

    sock.close()

    try:
        ret = json.loads(meas)
        ret_err = ret['error']
        ret_meas = ret['measurements']
        if ret_meas is None:
            raise ValueError('member "measurements" is None')
    except (JSONDecodeError, KeyError, ValueError) as err:
        raise ValueError('bad response from Docker monitor') from err

    if ret_err is not None:
        raise ValueError('received error response from Docker monitor: ' + ret_err)

    return [Measurement(**rec) for rec in ret_meas]


def get_docker_operations_log(dockeropslog_srv: DockerOpsLogServerConfiguration,
                              start: int, end: int) -> Sequence[DockerOperationLogEntry]:
    """Fetch log of Docker operations from one Docker monitor.

    :param dockeropslog_srv: host and port of the log server to fetch Docker operations from
    :param start: start date of resource usage period to fetch (timestamp in milliseconds)
    :param end: end date of resource usage period to fetch (timestamp in milliseconds)

    :returns: list of log entries of Docker operations

    :raises ValueError: if the container ID was not found in the Docker monitor
    """
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((dockeropslog_srv['address'], dockeropslog_srv['port']))

    sock.sendall(json.dumps({
        'start': round(start / 1000, 3),
        'end':   round(end / 1000, 3),
    }).encode())
    sock.shutdown(socket.SHUT_WR)

    meas = b''
    buff = sock.recv(4096)
    while buff:
        meas += buff
        buff = sock.recv(4096)

    sock.close()

    try:
        ret = json.loads(meas)
        ret_err = ret['error']
        ret_log = ret['log']
        if ret_log is None:
            raise ValueError('member "log" is None')
    except (JSONDecodeError, KeyError, ValueError) as err:
        raise ValueError('bad response from Docker monitor') from err

    if ret_err is not None:
        raise ValueError('received error response from Docker monitor: ' + ret_err)

    return [DockerOperationLogEntry(date=entry['date'], docker_name=entry['docker_name'],
                                    operation=DockerOperation[entry['operation']]) for entry in ret_log]


class ActivationMonitor(Thread, ABC):
    """Monitor for terminations of function invocations (abstract base class).

    Inherit from this class and implement the method :py:func:`_run()`, to implement the platform-dependent activation
    monitor thread.
    See the developer documentation on implementing a new platform for more information.

    An activation monitor is a thread that receives events of termination of function invocations, and:

    * completes the invocation record in the database;
    * fetches and stores resource usage and performance data to the database (handled by the base class);
    * signals the main injector thread that the invocation is terminated.
    """

    def __init__(self, users: Collection[str], db: FaaSLoadDatabase,
                 docker_mon_cfg: DockerMonitorConfiguration):
        """Initialize a new activation monitor thread.

        YOU SHOULD ONLY RUN ONE SUCH THREAD.

        The thread will be named "ActivationMonitor".

        This opens a connection to the database (instantiates :py:class:`FaaSLoadDatabase`) that is closed when the
        monitor terminates.

        :param db: the configuration to connect to the database
        :param docker_mon_cfg: the configuration to reach the Docker monitors
        """
        super().__init__(name='ActivationMonitor')

        self.users = users
        self.db = db
        self.docker_mon_cfg = docker_mon_cfg

        self.run_event = Event()
        self.run_event.set()

        self.notification_queue = SimpleQueue()

        # Integer number of milliseconds.
        self.start_date = int(time.time() * 1000)

        self.logger = logging.getLogger('activationmon')

    def get_notifications(self, timeout=10) -> ActivationMessage:
        """As a client to the activation monitor, receive a notification.

        This method blocks waiting for a notification.

        :return ActivationMessage: the earliest pending notification message
        """
        if not self.run_event.is_set():
            raise ActivationMonitorError('activation monitor terminated')

        return self.notification_queue.get(block=True, timeout=timeout)

    def terminate(self):
        """Terminate the monitor.

        This methods fetches from the Docker monitors, the logs of Docker operations, and write them to the database,
        before terminating.
        """
        if self.docker_mon_cfg["enabled"]:
            self._store_docker_operations_log()

        self.logger.debug('terminating')
        self.run_event.clear()

    def _get_resource_usage(self, cont_id: str, start: int, end: int) -> Sequence[Measurement]:
        for msrv in self.docker_mon_cfg["measurementservers"]:
            try:
                return get_resource_usage(msrv, cont_id, start, end)
            except (ConnectionError, OSError) as err:
                self.logger.warning('measurement server %s:%s is unreachable: %s', msrv['address'], msrv['port'], err)
                continue
            except ValueError:
                # Not found on this measurement server
                continue

        raise ValueError('all measurements servers responded in error')

    def _store_docker_operations_log(self):
        for mon in self.docker_mon_cfg["dockeropslogservers"]:
            try:
                log = get_docker_operations_log(mon, self.start_date, int(time.time() * 1000))
            except (ConnectionError, OSError) as err:
                self.logger.critical('failed fetching Docker operations log from server %s:%s: %s', mon['address'],
                                     mon['port'], err)
                continue
            except ValueError as err:
                self.logger.critical('server %s:%s responded in error when fetching Docker operations log: %s',
                                     mon['address'], mon['port'], err)
                continue

            try:
                self.db.insert_docker_operations_log(mon, log)
            except FaaSLoadDatabaseException as err:
                if self.logger.isEnabledFor(logging.DEBUG):
                    self.logger.critical('failed storing Docker operations log of monitor %s:%s',
                                         mon['address'], mon['port'], exc_info=True)
                else:
                    self.logger.critical('failed storing Docker operations log of monitor %s:%s: %s',
                                         mon['address'], mon['port'], err)

                continue

            self.logger.info('stored Docker operations log of server %s:%s', mon['address'], mon['port'])

    def run(self):
        """Main loop for the activation monitor thread.

        It runs the abstract method :py:func:`_run` that contains the platform-dependent implementation.
        It runs until :py:attr:`run_event` is cleared.
        """
        # we need to poll() for new messages in order to periodically check the termination flag
        while self.run_event.is_set():
            try:
                self._run()
            except ActivationMonitorError as err:
                if self.logger.isEnabledFor(logging.DEBUG):
                    self.logger.critical('cannot continue monitoring activations', exc_info=True)
                else:
                    self.logger.critical('cannot continue monitoring activations: %s', err)
                self.terminate()
            except Exception as err:
                self.logger.warning('activation monitor failure: %s', err)

    def _update_db(self, activation: Activation):
        # Update the activation in the db
        try:
            self.db.update_activation(activation.activationId, {
                'failed':    (not activation.response.success),
                'start':     activation.start,
                'end':       activation.end,
                'wait_time': next((a for a in activation.annotations if a['key'] == 'waitTime'), {'value': None})[
                                 'value'],
                'init_time': next((a for a in activation.annotations if a['key'] == 'initTime'), {'value': None})[
                                 'value'],
                'error_msg': activation.act_error_msg,
            })
        except FaaSLoadDatabaseException as err:
            if self.logger.isEnabledFor(logging.DEBUG):
                self.logger.critical('failed storing data of activation %s of user %s into the database',
                                     activation.activationId, activation.namespace, exc_info=True)
            else:
                self.logger.critical('failed storing data of activation %s of user %s into the database: %s',
                                     activation.activationId, activation.namespace, err)
        else:
            self.logger.debug('updated run record of activation %s', activation.activationId)

        # Insert the result of the activation in the db
        try:
            self.db.insert_result(activation.activationId, activation.response.result)
        except FaaSLoadDatabaseException as err:
            if self.logger.isEnabledFor(logging.DEBUG):
                self.logger.critical('failed storing result of activation %s of user %s into the database',
                                     activation.activationId, activation.namespace, exc_info=True)
            else:
                self.logger.critical('failed storing result of activation %s of user %s into the database: %s',
                                     activation.activationId, activation.namespace, err)
        else:
            self.logger.debug('inserted activation result of activation %s', activation.activationId)

        # if the monitoring is enable store the resources used by the container
        if self.docker_mon_cfg["enabled"] and activation.response.success:
            if activation.dockerId is None:
                self.logger.critical(
                    'not fetching resource usage measurements of activation %s of user %s because of failure fetching '
                    'its Docker ID',
                    activation.activationId, activation.namespace)
                return

            try:
                res = self._get_resource_usage(activation.dockerId,
                                               activation.start,
                                               activation.end)
            except ValueError as err:
                if self.logger.isEnabledFor(logging.DEBUG):
                    self.logger.critical('failed fetching resource usage measurements of activation %s of user %s',
                                         activation.activationId, activation.namespace, exc_info=True)
                else:
                    self.logger.critical('failed fetching resource usage measurements of activation %s of user %s: %s',
                                         activation.activationId, activation.namespace, err)
                return

            try:
                self.db.insert_resources(activation.activationId, res)
            except FaaSLoadDatabaseException as err:
                if self.logger.isEnabledFor(logging.DEBUG):
                    self.logger.critical(
                        'failed storing resource usage measurements of activation %s of user %s into the database',
                        activation.activationId, activation.namespace, exc_info=True)
                else:
                    self.logger.critical(
                        'failed storing resource usage measurements of activation %s of user %s into the database: %s',
                        activation.activationId, activation.namespace, err)
            else:
                self.logger.debug('inserted resource usage of activation %s', activation.activationId)

    @abstractmethod
    def _run(self):
        """Run one iteration of the loop of the activation monitor.

        Fetch one round of notifications of terminations of functions.

        This is the method to implement for the platform-dependent implementation of the activation monitor.

        It should return periodically, to leave a chance to terminate the activation monitor.
        """
        raise NotImplementedError


class OpenwhiskActivationMonitor(ActivationMonitor):
    """Platform-dependent implementation of the activation monitor for Apache OpenWhisk."""

    # Duration of one polling session
    # After polling for this duration, the monitor checks its termination flag and then resumes polling immediately if
    # it is not set.
    POLL_TIME_MS = 10000

    # Limit number of activations polled per query.
    # 200 is the maximum allowed by OpenWhisk.
    POLL_ACTIVATIONS_LIMIT = 200

    def __init__(self, db: FaaSLoadDatabase, docker_mon_cfg: DockerMonitorConfiguration,
                 cfg: OpenWhiskConfiguration, admin_cfg: OpenWhiskAdministrationConfiguration):

        """Initialize a new activation monitor thread for OpenWhisk.

        :param db_cfg: the configuration to connect to the database
        :param docker_mon_cfg: the configuration to reach the Docker monitors
        :param cfg: the configuration to use OpenWhisk
        :param admin_cfg: the configuration to use administrative facilities of OpenWhisk
        """
        super().__init__(users=cfg['auth'].keys(), db=db, docker_mon_cfg=docker_mon_cfg)

        self.admin_cfg = admin_cfg
        self.cfg = cfg

        # Keep already reported activations per user.
        self.reported = {user_name: set() for user_name in self.users}

    def _run(self):
        """Monitor OpenWhisk for activations.

        It polls every :py:attr:`POLL_TIME_MS` ms the platform for all activations, and handles any new activation since
        the last poll.

        Note that it polls all activations since its start, not since the last poll: an activation may have been
        requested very early and terminate very late, but activations appear at their start date, not their end date, so
        it must always poll since the beginning.
        """
        from pywhisk import activation

        for user_name in self.cfg["auth"].keys():
            # 200 activations at once is the limit.
            # They are returned most-recent first.
            # To avoid missing activations (e.g. more than 200 in between the polling interval), query more ancient
            # ones.
            acts = []
            poll_upto = int(time.time() * 1000 + 1)
            while True:
                _acts = activation.get_all(user_name, self.cfg, since=self.start_date, upto=poll_upto, limit=OpenwhiskActivationMonitor.POLL_ACTIVATIONS_LIMIT)
                if self.logger.isEnabledFor(logging.DEBUG):
                    self.logger.debug('polled activations from %d to %d for user %s: %s', self.start_date, poll_upto, user_name, [act.activationId[:8] for act in acts])
                acts.extend([act for act in _acts if act.activationId not in self.reported[user_name]])
                if len(_acts) == OpenwhiskActivationMonitor.POLL_ACTIVATIONS_LIMIT:
                    poll_upto = _acts[-1].start - 1
                    if poll_upto < self.start_date:
                        # I don't think that could happen, but who knows...
                        break
                    self.logger.warning('received the limit number of activations from OpenWhisk, querying earlier activations (up to %d)', poll_upto)
                else:
                    break
            if not acts:
                continue

            for act in acts:
                # Remove activation logs from debug output.
                logs = act.logs
                act.logs = ['...']
                self.logger.debug('received new activation for user %s: %s', user_name, act)
                act.logs = logs

                self._handle_activation_event(act, user_name)

                self.reported[user_name].add(act.activationId)

        # Idly sleep because this is called in a loop
        time.sleep(self.POLL_TIME_MS / 1000)

    def _handle_activation_event(self, act, user_name):
        from pywhisk.admin.docker import get_docker_id

        docker_id = None
        if self.docker_mon_cfg["enabled"] and act.response.success:
            try:
                docker_id = get_docker_id(act.activationId, self.admin_cfg)
            except OpenWhiskAdminException as err:
                if self.logger.isEnabledFor(logging.DEBUG):
                    self.logger.critical(
                        'failed fetching Docker ID of activation %s of user %s, the resource usage measurements will '
                        'not be stored', act.activationId, user_name, exc_info=True)
                else:
                    self.logger.critical(
                        'failed fetching Docker ID of activation %s of user %s, the resource usage measurements will '
                        'not be stored: %s', act.activationId, user_name, err)

        activation = Activation(
            activationId=act.activationId,
            namespace=user_name,
            response=ActivationResponse(
                success=act.response.success,
                result=act.response.result
            ),
            dockerId=docker_id,
            start=act.start,
            end=act.end,
            act_error_msg=(
                act.response.result['faasload']['error_msg'] if
                ('faasload' in act.response.result and
                 'error_msg' in act.response.result['faasload'])
                else None),
            annotations=act.annotations,
        )

        self.notification_queue.put(ActivationMessage(
            msg=ActivationMonitorNotification.ActivationDataFetched,
            activation=activation,
        ))

        self.logger.debug('sent notification of ActivationDataFetched for activation %s', act.activationId)

        try:
            self.logger.debug('activation %s sent FaaSLoad result: %s', activation.activationId,
                              act.response.result['faasload'])
        except KeyError:
            pass
        if activation.act_error_msg:
            self.logger.warning('activation %s returned in error: "%s"', activation.activationId,
                                activation.act_error_msg)

        self._update_db(activation=activation)


class KnativeActivationMonitor(ActivationMonitor):
    """Platform-dependent implementation of the activation monitor for Knative."""

    def __init__(self, db: FaaSLoadDatabase, docker_mon_cfg: DockerMonitorConfiguration,
                 cfg: KnativeConfiguration):

        """Initialize a new activation monitor thread for Knative.

        :param db_cfg: the configuration to connect to the database
        :param docker_mon_cfg: the configuration to reach the Docker monitors
        :param cfg: the configuration to use Knative
        """
        super().__init__(['default'], db, docker_mon_cfg)

        self.cfg = cfg

        self.read_activation_ids: List[str] = []

    def _run(self) -> None:
        """Monitor Knative for invocations.

        It polls the platform for all invocations, by requesting the logs of the queue proxy.
        """
        from pyknative.activation import get_executed_activations
        from pyknative.models import ApiConfiguration
        acts = get_executed_activations(
            cfg=ApiConfiguration(
                ip=self.cfg["ip"],
                tracer_ip=self.cfg["tracer_ip"],
                gateway_component_name=self.cfg["gateway_component_name"]
            )
        )
        for act in acts:
            act_id = act.activation_id
            if act_id in self.read_activation_ids:
                continue
            self.read_activation_ids.append(act_id)
            user_name = "default"

            activation = Activation(
                activationId=act_id,
                namespace=user_name,
                response=ActivationResponse(
                    success=True,
                    result=""
                ),
                dockerId=act.docker_id,
                start=act.start if act.start else 0,
                end=act.end if act.end else 0,
                act_error_msg="",
                annotations=[{'key': '', 'value': ''}]
            )

            self.notification_queue.put(ActivationMessage(
                msg=ActivationMonitorNotification.ActivationDataFetched,
                activation=activation,
            ))

            self.logger.debug('activation %d sent FaaSLoad result: %s', activation.activationId, activation)
            if activation.act_error_msg:
                self.logger.warning('activation %d returned in error: "%s"', activation.activationId,
                                    activation.act_error_msg)

            self._update_db(activation=activation)
