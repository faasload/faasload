"""Load workload traces into a FaaS platform.

It is **FaaSLoad's main module**, used in both injector and generator modes.

This module is executable:

.. code-block::

    python -m faasload.loader


Its implicit inputs are its configuration file *loader.yml*, and the data prepared with the script *load-wsk-assets.py*.

Architecture
------------

The loader starts at the function :py:func:`injection.inject`.
Its works with **injectors** (platform-dependent implementations based on the class :py:class:`injector.Injector`),
that inject **workload traces** (:py:class:`traces.InjectionTrace`), and an **activation monitor** (a platform-dependent
implementation of :py:class:`activation_monitor.ActivationMonitor`) that monitors invocations.
This is the case for both dataset generator and workload injector modes.
For the former, fake workload injection traces (:py:class:`generation.GeneratedInjectionTrace`) are generated.

The function :py:func:`injection.inject` starts the activation monitor, starts the injector threads, and wait for their
completion by matching injection notifications from the injector threads, with invocation termination notifications from
the activation monitor.
In other words, it constitutes the **main injection thread**.

A workload injection trace is a list of invocations for a given function under a given user.
For each injection trace, the main injection thread **creates an injector thread**.

Injectors **are not actual threads**: rather, they are a shared state between timer threads (i.e.,
threads that are started after a given delay, see :py:class:`thread.Timer`) in a daisy chain.
**Each injector timer thread is one invocation** of the function of this injector.

The activation monitor is a thread that handles the asynchronicity of the invocations done by the injectors.
It follows events of completion of invocations emitted by the FaaS platform, to:

1. notify the main injection thread that the injections are terminated;
2. complete the partial invocation record in the database with final invocation data fetched from the FaaS platform;
3. store in the database the performance data fetched from the monitor, if enabled.

The database (expected: PostGreSQL from Timescale_) is deployed independently of FaaSLoad, and the module
:py:mod:`database` is just a nice interface for database-related operations.

See the documentation about the "Architecture of FaaSLoad", and the docstrings of the described objects, for details.

.. _Timescale: https://www.timescale.com/
"""

# Copyright Toulouse INP / IRIT / CNRS, 2019 - 2020
# Copyright Télécom SudParis / IP Paris, 2021 - 2025
# Mathieu Bacou and other contributors
#
# mathieu.bacou@telecom-sudparis.eu
#
# This software is a computer program whose purpose is to inject a workload
# to a Function-as-a-Service platform, and to gather data about its execution.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software. You can use,
# modify and/ or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
from __future__ import annotations

import os
from collections.abc import Mapping
from enum import Enum
from glob import glob
from typing import TypedDict, Union

from configuration_reader import (ConfigurationRequired, ConfigurationRequiredNonEmptyError,
                                  ConfigurationError, DictKeyPathComponent, apply_default_mask)
from pywhisk.client import (AdministrationConfiguration as OpenWhiskAdministrationConfiguration,
                            KubernetesConfiguration,
                            read_api_cfg, APIConfiguration as OpenWhiskConfiguration)

from faasload.docker_monitor import MonitorServerConfiguration, DockerOpsLogServerConfiguration

# Version of the loader, independent to FaaSLoad's global version
__version__ = '2.2.0'


class FaaSPlatform(Enum):
    OpenWhisk = "openwhisk"
    Knative = "knative"


FAASPLATFORM_PRETTY_NAMES = {
    FaaSPlatform.OpenWhisk: 'Apache OpenWhisk',
    FaaSPlatform.Knative:   'Knative',
}


class LogsFetchConfiguration(TypedDict):
    timeout: float
    backofftime: float


class GeneralConfiguration(TypedDict):
    fetchlogs: LogsFetchConfiguration
    injection: InjectionConfiguration


class InjectionConfiguration(TypedDict):
    timingerror_reportthreshold: float
    timeout: float


class InjectorConfiguration(TypedDict):
    tracedir: str


class GenerationConfiguration(TypedDict):
    enabled: bool
    seed: Union[int, float, str, bytes, bytearray]
    statedir: str
    interinvocationtime: float
    nbinputs: Mapping[str, int]


class DatabaseConfiguration(TypedDict):
    user: str
    password: str
    database: str
    host: str


class DockerMonitorConfiguration(TypedDict):
    enabled: bool
    measurementservers: list[MonitorServerConfiguration]
    dockeropslogservers: list[DockerOpsLogServerConfiguration]


class OpenWhiskConfigurationFromFile(TypedDict):
    host: str
    disablecert: bool
    authkeys: str


class KnativeConfiguration(TypedDict):
    ip: str
    tracer_ip: str
    gateway_component_name: str


class FaaSLoadGlobalConfiguration(TypedDict):
    general: GeneralConfiguration
    injector: InjectorConfiguration
    generator: GenerationConfiguration
    database: DatabaseConfiguration
    dockermonitor: DockerMonitorConfiguration


class FaasloadOpenwhiskConfiguration(TypedDict):
    openwhisk: OpenWhiskConfiguration
    openwhiskadmin: OpenWhiskAdministrationConfiguration


DISABLE_CERT_DEFAULT = True


def read_openwhisk_configuration(user_value: OpenWhiskConfigurationFromFile | None, _) -> OpenWhiskConfiguration:
    def default_openwhisk_authkeys(authkeys_user_value: str | None, cfg: OpenWhiskConfigurationFromFile) -> str:
        if cfg['host'] and not authkeys_user_value:
            raise ConfigurationRequiredNonEmptyError('authkeys')

        return authkeys_user_value

    def read_auth_keys(auth_dir):
        auth = {}
        for filepath in glob(os.path.join(auth_dir, '*')):
            with open(filepath, 'r') as auth_file:
                auth[os.path.basename(filepath)] = tuple(auth_file.read().rstrip().split(':', maxsplit=1))

        return auth

    # No key "openwhisk".
    if user_value is None:
        raise ConfigurationRequiredNonEmptyError('openwhisk')

    try:
        user_value = apply_default_mask(
            user_value,
            default_mask=OpenWhiskConfigurationFromFile(
                host=ConfigurationRequired.Required,
                disablecert=True,
                authkeys=default_openwhisk_authkeys,
            ))
    except ConfigurationError as err:
        err.prepend_path_component(DictKeyPathComponent('openwhisk'))
        raise

    # Key "host" is the empty string: read configuration from OpenWhisk's configuration file.
    if user_value['host'] == '':
        cfg = read_api_cfg(cert=not user_value.get('disablecert', DISABLE_CERT_DEFAULT))
    else:
        cfg = OpenWhiskConfiguration(
            host=user_value['host'],
            cert=not user_value.get('disablecert', DISABLE_CERT_DEFAULT),
            auth=read_auth_keys(os.path.expanduser(os.path.expandvars(user_value['authkeys']))),
        )

    return cfg


class FaasloadKnativeConfiguration(TypedDict):
    knative: KnativeConfiguration


def default_injector_tracedir(user_value, cfg):
    if cfg['generator']['enabled']:
        return ''

    if not user_value:
        raise ConfigurationRequiredNonEmptyError('tracedir')

    return user_value


def default_generator_nbinputs(user_value, cfg):
    if not cfg['generator']['enabled']:
        return []

    if not user_value:
        raise ConfigurationRequiredNonEmptyError('nbinputs')

    return user_value


def default_dockermonitor_measurementservers(user_value, cfg):
    if not cfg['dockermonitor']['enabled']:
        return []

    if not user_value:
        raise ConfigurationRequiredNonEmptyError('measurementservers')

    return apply_default_mask(
        user_value,
        MonitorServerConfiguration(address=ConfigurationRequired.RequiredNonEmpty, port=11085))


def default_dockermonitor_dockeropslogservers(user_value, cfg):
    if not cfg['dockermonitor']['enabled']:
        return []

    if not user_value:
        raise ConfigurationRequiredNonEmptyError('dockeropslogservers')

    return apply_default_mask(
        user_value,
        DockerOpsLogServerConfiguration(address=ConfigurationRequired.RequiredNonEmpty, port=11086))


DEFAULTS = FaaSLoadGlobalConfiguration(
    general=GeneralConfiguration(
        fetchlogs=LogsFetchConfiguration(
            timeout=5,
            backofftime=0.5,
        ),
        injection=InjectionConfiguration(
            timingerror_reportthreshold=10.0,
            timeout=960,
        ),
    ),
    injector=InjectorConfiguration(
        tracedir=default_injector_tracedir,
    ),
    generator=GenerationConfiguration(
        enabled=False,
        seed=19940503,
        statedir='./states',
        interinvocationtime=60,
        nbinputs=default_generator_nbinputs,
    ),
    database=DatabaseConfiguration(
        host=ConfigurationRequired.RequiredNonEmpty,
        user='faasload',
        password='faasload',
        database='faasload',
    ),
    dockermonitor=DockerMonitorConfiguration(
        enabled=True,
        measurementservers=default_dockermonitor_measurementservers,
        dockeropslogservers=default_dockermonitor_dockeropslogservers,
    ),
)

DEFAULTS_OPENWHISK = FaasloadOpenwhiskConfiguration(
    openwhisk=read_openwhisk_configuration,
    openwhiskadmin=OpenWhiskAdministrationConfiguration(
        kubernetes=KubernetesConfiguration(
            helmrelease='ow',
            namespace='openwhisk',
        ),
    ),
)

DEFAULTS_KNATIVE = FaasloadKnativeConfiguration(
    knative=KnativeConfiguration(
        ip='172.17.0.1',
        tracer_ip='192.168.49.2',
        gateway_component_name='kourier'
    ),
)
