"""Workload injection traces - Description of FaaS function invocations.

Use the function :py:func:`load_trace()` to load injection traces from trace files.
See the documentation about the injector mode for more information on creating trace files.

A workload injection trace is an instance of the class :py:class:`InjectionTrace`.
FaaSLoad prepares one trace per function, to invoke it successively under a given user of the FaaS platform.

An injection trace behaves like an iterable of :py:class:`InjectionTracePoint`, each representing one invocation of the
function with its specific parameters.
"""

# Copyright Toulouse INP / IRIT / CNRS, 2019 - 2020
# Copyright Télécom SudParis / IP Paris, 2021 - 2025
# Mathieu Bacou and other contributors
#
# mathieu.bacou@telecom-sudparis.eu
#
# This software is a computer program whose purpose is to inject a workload
# to a Function-as-a-Service platform, and to gather data about its execution.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software. You can use,
# modify and/ or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.

from __future__ import annotations

import os
from collections.abc import Mapping
from datetime import timedelta
from typing import Any, Iterable, NamedTuple

from .database import FunctionRecord, FaaSLoadDatabase


class InjectionTracePoint(NamedTuple):
    """One invocation in a workload injection trace.

    The function is invoked `wait` s after the previous point, with the parameters `params`.
    """
    wait: timedelta
    params: Mapping[str, Any]


class InjectionTraceState(NamedTuple):
    user: str
    function: FunctionRecord


class InjectionTrace(Iterable[InjectionTracePoint]):
    """A workload injection trace read from a file.

    This is iterable, use it as such.

    You probably want to use :py:func:`load_trace()` instead of instantiating this class manually.
    """

    def __init__(self, user: str, function: FunctionRecord, points: Iterable[InjectionTracePoint]):
        """Build a new injection trace.

        :param user: the user name under which the function is invoked
        :param function: the function invoked by this trace
        :param points: the trace points to inject
        """
        self.user = user
        self.function = function
        self.points = points

    def get_state(self) -> InjectionTraceState:
        """Get the state of the trace.

        In practice, the state of an injection trace is not checkpointed, because it wouldn't make sense to restore
        trace states while not restoring the internal state of the FaaS platform, which is impossible.
        This method solely exists to mirror the one from :py:class:`GeneratedInjectionTrace`.

        :returns: the state of the trace
        """
        return InjectionTraceState(self.user, self.function)

    def __iter__(self):
        return iter(self.points)


def load_trace(trace_filepath: str, db: FaaSLoadDatabase) -> InjectionTrace:
    """Load a workload injection trace from a file.

    The trace file's first line is a head of tab-separated information:

        USER PACKAGE/FUNCTION MEMORY

    USER is the user that invokes the function FUNCTION in the user package PACKAGE.
    MEMORY is the memory amount allocated to the function (this is unused by the injector).

    Then, the main content of the file must follow the form:

        WAIT    INPUT   PARAM1:VALUE1   PARAM2:VALUE2   ...

    WAIT is the time to wait before invoking the function with the given INPUT and PARAM VALUEs.
    INPUT is the path to the input object, it is actually given to the function as the parameter "object".

    This function reads information about the function from FaaSLoad's database (table "functions").

    :param trace_filepath: the path to the file of the workload trace
    :param db: the client to FaaSLoad's database

    :returns: an :py:class:`InjectionTrace` representing the loaded trace

    :raises TraceParsingException: there is a format error in the trace that prevented parsing
    :raises FaaSLoadDatabaseException: there was an error finding the function's information in the database
    """
    points = []
    with open(trace_filepath, 'r', newline='') as trace_file:
        try:
            user, func_fullname, _ = tuple(next(trace_file).rstrip().split('\t'))
        except (StopIteration, ValueError):
            raise TraceParsingException('failed parsing header')

        func = db.select_function(func_fullname)

        for point_id, point in enumerate(line.rstrip().split('\t') for line in trace_file):
            try:
                wait = timedelta(seconds=float(point[0]))

                if func.input_kind != 'none':
                    params = {arg: value for arg, value in [tuple(argvalue.split(':')) for argvalue in point[2:]]}

                    params['object'] = point[1]
                else:
                    params = {arg: value for arg, value in [tuple(argvalue.split(':')) for argvalue in point[1:]]}
            except (IndexError, TypeError):
                raise TraceParsingException(f'failed parsing trace point #{point_id + 1}')

            params['incont'] = user
            params['outcont'] = os.path.splitext(os.path.basename(trace_filepath))[0] + '-out'

            points.append(InjectionTracePoint(wait=wait, params=params))

    return InjectionTrace(user=user, function=func, points=points)


class TraceParsingException(Exception):
    """Parsing a workload injection trace failed."""
    pass
