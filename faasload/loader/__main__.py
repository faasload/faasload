"""Entrypoint to run the dataset generator.

The function :py:func:`main()` handles:

1. reading the configuration (including platform-specific configuration)

   * see the module :py:mod:`loader` for the definition of the configuration
   * the configuration is read from *~/.config/faasload/loader.yml*

2. if in dataset generator mode:

   a. loading checkpointed generation state if found
   b. building injection traces for the loader

3. else if in workload injector mode: loading injection traces
4. running the loader
5. gathering the loader's state after its execution for display
6. if in dataset generation mode, write the loader's checkpointed state
"""

# Copyright Toulouse INP / IRIT / CNRS, 2019 - 2020
# Copyright Télécom SudParis / IP Paris, 2021 - 2025
# Mathieu Bacou and other contributors
#
# mathieu.bacou@telecom-sudparis.eu
#
# This software is a computer program whose purpose is to inject a workload
# to a Function-as-a-Service platform, and to gather data about its execution.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software. You can use,
# modify and/ or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.

import logging
import logging.config
import os
import pickle
import sys
from datetime import datetime, timedelta
from glob import glob
from pickle import PickleError

from configuration_reader import read as read_configuration

from faasload.lib import configure_logging, log_faasload_preamble, prepare_and_read_configuration
from . import __version__ as my_version, DEFAULTS, DEFAULTS_OPENWHISK, FaaSLoadGlobalConfiguration, \
    FaasloadOpenwhiskConfiguration, FaaSPlatform, FAASPLATFORM_PRETTY_NAMES
from .database import FaaSLoadDatabase
from .generation import build_traces as build_generator_traces, GenerationException
from .injection import inject
from .injector import InjectorExitState
from .traces import load_trace as load_injection_trace, TraceParsingException

FAASLOAD_PLATFORM_ENVAR = 'FAASLOAD_PLATFORM'


class StateLoaderError(Exception):
    """An error happened while loading a checkpointed generator state."""

    def __init__(self, msg, filename=None):
        super().__init__(msg)

        self.filename = filename


def main():
    """Main method of the loader."""
    configure_logging(os.path.expanduser('~/.config/faasload/loader.yml'))
    logger = logging.getLogger('main')

    general_conf: FaaSLoadGlobalConfiguration = read_configuration(
        os.path.expanduser('~/.config/faasload/loader.yml'),
        prepare_and_read_configuration,
        default_mask=DEFAULTS,
        keep_unexpected_keys=True)

    log_faasload_preamble(logger)
    logger.info('FaaSLoad loader %s', my_version)

    try:
        platform_name = os.environ[FAASLOAD_PLATFORM_ENVAR]
    except KeyError:
        # Default platform is OpenWhisk for backward compatibility
        platform = FaaSPlatform.OpenWhisk
        logger.warning('environment variable "%s" to define FaaS platform not set; '
                       'using "%s" (%s) as a default, but consider setting it', FAASLOAD_PLATFORM_ENVAR, platform.value,
                       FAASPLATFORM_PRETTY_NAMES[platform])
    else:
        try:
            platform = FaaSPlatform(platform_name)
        except KeyError:
            logger.error('unsupported FaaS platform "%s" set in environment variable "%s"; '
                         'choose one of %s; aborting', platform_name, FAASLOAD_PLATFORM_ENVAR,
                         ', '.join(f'"{pf.value}"' for pf in FaaSPlatform))
            sys.exit(1)
    logger.info('running on FaaS platform %s', FAASPLATFORM_PRETTY_NAMES[platform])

    common_cfg = general_conf['general']
    logger.debug('general configuration: %s', common_cfg)

    inj_cfg = general_conf['injector']
    inj_cfg['tracedir'] = os.path.expanduser(os.path.expandvars(inj_cfg['tracedir']))
    logger.debug('trace injector configuration: %s', inj_cfg)

    gen_cfg = general_conf['generator']
    gen_cfg['statedir'] = os.path.expanduser(os.path.expandvars(gen_cfg['statedir']))
    logger.debug('dataset generator configuration: %s', gen_cfg)

    db_cfg = general_conf['database']
    logger.debug('database configuration: %s', db_cfg)

    docker_mon_cfg = general_conf['dockermonitor']
    logger.debug('monitor access configuration: %s', docker_mon_cfg)

    db = FaaSLoadDatabase(db_cfg)
    db.load_database()

    if gen_cfg['enabled']:
        state = None

        try:
            # try to get the genstate file with the youngest date in its name from the configured directory
            most_recent = max((fn for fn in glob(os.path.join(gen_cfg['statedir'], '*.genstate'))),
                              key=lambda fn: datetime.fromisoformat(os.path.splitext(os.path.basename(fn))[0]))
        except ValueError:
            # max()'s arg is an empty sequence: no state loading
            logger.info('no state given to load and no state file found in "%s": fresh start', gen_cfg['statedir'])
        else:
            try:
                with open(most_recent, 'rb') as state_file:
                    # list of tuples (run_id, rng_state)
                    state = pickle.load(state_file)

                logger.info('loaded most recent state from "%s"', most_recent)
            except (OSError, EOFError, PickleError):
                logger.exception('found state file at "%s" but failed loading state: aborting', most_recent)
                sys.exit(1)

            if logger.isEnabledFor(logging.DEBUG):
                for inj_state in state:
                    logger.debug('%s/%s: last run = %d (global last run = %d)', inj_state.trace_state.user,
                                 inj_state.trace_state.function.name, inj_state.last_run, inj_state.global_last_run)

        try:
            traces = build_generator_traces(db, gen_cfg, state=state)
        except GenerationException:
            logger.exception('failed generating injection traces')
            sys.exit(1)

        if state:
            # for each injector, delete runs with ID greater than the last good ID
            # this does not delete runs for which the ActivationMonitor failed fetching data, they will remain with
            # NULL values
            db.delete_lost_runs(state)
            logger.debug('wiped lost runs after loading state')

        logger.info('generated %d injection traces', len(traces))
        if logger.isEnabledFor(logging.DEBUG):
            for trace in traces:
                logger.debug('%s', trace)
    else:
        traces = []
        for trace_filepath in glob(os.path.join(inj_cfg['tracedir'], '*.faas')):
            try:
                logger.debug('reading trace file "%s"', trace_filepath)
                traces.append(load_injection_trace(trace_filepath, db))
            except TraceParsingException:
                logger.exception('failed loading trace file "%s": aborting', trace_filepath)
                sys.exit(1)
        if not traces:
            logger.error('no traces found in "%s": aborting', inj_cfg['tracedir'])
            sys.exit(1)

        db.wipe()
        logger.debug('wiped database')

        logger.info('read %d injection traces from "%s"', len(traces), inj_cfg['tracedir'])

    # Specialized initializations per platform
    if platform is FaaSPlatform.OpenWhisk:

        from .injector import OpenwhiskInjector
        from .activation_monitor import OpenwhiskActivationMonitor

        conf: FaasloadOpenwhiskConfiguration = read_configuration(
            os.path.expanduser('~/.config/faasload/loader.yml'),
            prepare_and_read_configuration, DEFAULTS_OPENWHISK,
            keep_unexpected_keys=True)

        cfg = conf['openwhisk']
        logger.debug('OpenWhisk API configuration: %s', cfg)

        admin_cfg = conf['openwhiskadmin']
        logger.debug('OpenWhisk admin configuration: %s', admin_cfg)

        if not cfg['cert']:
            # This local import is only used to suppress warnings related to disabled certificate checking.
            import urllib3
            urllib3.disable_warnings()
            logger.warning('disabling certificate verification of OpenWhisk API gateway')

        act_mon = OpenwhiskActivationMonitor(
            db=db,
            cfg=cfg,
            admin_cfg=admin_cfg,
            docker_mon_cfg=docker_mon_cfg,
        )

        if gen_cfg['enabled'] and state:
            # Generator mode, and a state was restored: must restore the state of the injectors too.
            injectors = [OpenwhiskInjector(trace=trace, db=db, common_cfg=common_cfg, cfg=cfg,
                                           prev_ids=(inj_state.last_run, inj_state.global_last_run), timingerror_reportthreshold=timedelta(milliseconds=common_cfg['injection']['timingerror_reportthreshold'])) for
                         inj_state, trace in zip(state, traces)]
        else:
            injectors = [OpenwhiskInjector(trace=trace, db=db, common_cfg=common_cfg, cfg=cfg, timingerror_reportthreshold=timedelta(milliseconds=common_cfg['injection']['timingerror_reportthreshold'])) for trace in
                         traces]

        injectors = inject(injectors=injectors, act_mon=act_mon, timeout=common_cfg['injection']['timeout'])
    elif platform is FaaSPlatform.Knative:
        from . import KnativeConfiguration
        from .injector import KnativeInjector
        from .activation_monitor import KnativeActivationMonitor
        from pyknative.client import get_api_configuration

        cfg = get_api_configuration()

        logger.debug('Knative API configuration: %s', cfg)

        act_mon = KnativeActivationMonitor(
            db=db,
            docker_mon_cfg=docker_mon_cfg,
            cfg=KnativeConfiguration(
                ip=cfg.ip,
                tracer_ip=cfg.tracer_ip,
                gateway_component_name=cfg.gateway_component_name
            )
        )

        if gen_cfg['enabled'] and state:
            # Generator mode, and a state was restored: must restore the state of the injectors too.
            injectors = [KnativeInjector(trace=trace, db=db, cfg=cfg,
                                         prev_ids=(inj_state.last_run, inj_state.global_last_run), timingerror_reportthreshold=timedelta(milliseconds=common_cfg['injection']['timingerror_reportthreshold'])) for inj_state, trace
                         in zip(state, traces)]
        else:
            injectors = [KnativeInjector(trace=trace, db=db, cfg=cfg, timingerror_reportthreshold=timedelta(milliseconds=common_cfg['injection']['timingerror_reportthreshold'])) for trace in traces]

        injectors = inject(injectors=injectors, act_mon=act_mon, timeout=common_cfg['injection']['timeout'])
    else:
        # Not possible: at this point, platform is a member of the bound enum FaaSPlatform
        logger.error('unsupported FaaS platform %s', platform)
        sys.exit(1)

    nb_success = 0
    nb_interrupted = 0
    nb_failure = 0
    nb_stillrunning = 0
    for injector in injectors:
        inj_state = injector.get_state()

        if inj_state.exit is InjectorExitState.EndOfTrace:
            nb_success += 1
            logger.info('injector %s ended successfully after %d runs', injector.name,
                        inj_state.last_run)
        elif inj_state.exit is InjectorExitState.Interrupted:
            nb_interrupted += 1
            logger.warning('injector %s was interrupted after %d runs', injector.name,
                           inj_state.last_run)
        elif inj_state.exit is InjectorExitState.Error:
            nb_failure += 1
            logger.error('injector %s ended in error after %d runs', injector.name,
                         inj_state.last_run)
        else:
            nb_stillrunning += 1
            logger.critical('injection ended with injector %s still running', injector.name)

    if nb_stillrunning == 0:
        logger.info(
            'injection ended with %d successful, %d interrupted, and %d failed injectors, out of %d total injectors',
            nb_success, nb_interrupted, nb_failure, len(injectors))
    else:
        logger.info(
            'injection ended with %d successful, %d interrupted, and %d failed injectors, out of %d total injectors '
            '(%d still running)', nb_success, nb_interrupted, nb_failure, len(injectors), nb_stillrunning)

    if gen_cfg['enabled']:
        state = [injector.get_state() for injector in injectors]
        state_filepath = os.path.join(gen_cfg['statedir'],
                                      datetime.now().isoformat(sep='_', timespec='seconds') + '.genstate')
        try:
            with open(state_filepath, 'wb') as state_file:
                pickle.dump(state, state_file)
            logger.info('wrote state to "%s"', state_filepath)
        except OSError as err:
            if logger.isEnabledFor(logging.DEBUG):
                logger.critical('FAILED SAVING GENERATOR STATE TO "%s": DUMPING STATE TO CONSOLE', state_filepath,
                                exc_info=True)
            else:
                logger.critical('FAILED SAVING GENERATOR STATE TO "%s": DUMPING STATE TO CONSOLE', state_filepath, err)
            print(repr(state))


if __name__ == '__main__':
    main()
