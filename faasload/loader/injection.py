"""The workload injector (i.e., the loader).

Whether in workload injection or dataset generation mode, this is where everything happens.

The main method is :py:func:`inject()`: it orchestrates the :py:class:`ActivationMonitor` thread, used to fetch
invocation data when one invocation is finished;
and the :py:class:`Injector` threads that are responsible for actually injecting the workload.
It waits for all invocations to have been processed by the activation monitor.

In addition, it is where SIGINT and SIGTERM signal are caught to have a chance of displaying a report, and checkpointing
the loader state in dataset generation mode.
"""

# Copyright Toulouse INP / IRIT / CNRS, 2019 - 2020
# Copyright Télécom SudParis / IP Paris, 2021 - 2025
# Mathieu Bacou and other contributors
#
# mathieu.bacou@telecom-sudparis.eu
#
# This software is a computer program whose purpose is to inject a workload
# to a Function-as-a-Service platform, and to gather data about its execution.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software. You can use,
# modify and/ or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.

from __future__ import annotations

import logging
import signal
import sys
import time
from datetime import datetime
from queue import Empty, SimpleQueue
from typing import Collection

from .activation_monitor import ActivationMonitor, ActivationMonitorNotification, ActivationMonitorError
from .injector import InjectionNotificationMessage, Injector, InjectorExitState, InjectionNotification


def inject(injectors: Collection[Injector],
           act_mon: ActivationMonitor, timeout: float) -> Collection[Injector]:
    """Inject workload traces (main method).

    First, the activation monitor thread (the FaaS platform-dependent instance passed in `act_mon`) is started.
    Then, each injector thread is passed the notification queue, and is started to inject the workload it describes.
    Finally, the method waits for all injections to complete.

    The returned list of injector handles can be used to check the final state (:py:class:`InjectorState`) of each
    injector using their method :py:func:`Injector.get_state()`, and to checkpoint the injection state if needed.

    This method should not raise any exception unless something very bad happens:

    * injector threads handle the exceptions raised by their own injections;
    * the method catches SIGINT and SIGTERM to gracefully return
      * this is for the caller to display a report, and checkpoint the loader state in dataset generation mode.

    :param injectors: the injector threads to run
    :param act_mon: the configuration to use OpenWhisk

    :returns: a list of handles to the injector threads
    """
    logger = logging.getLogger('injection')

    act_mon.start()

    inj_q = SimpleQueue()

    for injector in injectors:
        injector.set_notification_queue(inj_q)

    logger.info('starting %d injectors', len(injectors))

    # I try to start all threads as close to each other as possible
    for injector in injectors:
        injector.schedule()

    # sys.exit raises SystemExit, so `finally` clauses will be executed
    # Python already stores its default SIGINT handler (but not the SIGTERM one, probably because it does not replace it
    # and uses the system's default)
    signal.signal(signal.SIGINT, lambda *_: sys.exit(0))
    old_sigterm_handler = signal.signal(signal.SIGTERM, lambda *_: sys.exit(0))

    # noinspection PyBroadException
    try:
        _wait_injection(act_mon, inj_q, injectors, logger, timeout)

        logger.info('all injectors terminated (possibly in errors, check below)')
    except SystemExit:
        logger.warning('interrupted; terminating injector threads...')

        for injector in injectors:
            injector.terminate()
            injector.join()
    except ActivationMonitorError as err:
        if logger.isEnabledFor(logging.DEBUG):
            logger.critical('error while waiting for the injectors', exc_info=True)
        else:
            logger.critical('error while waiting for the injectors: %s', err)
    except Exception:
        logger.exception('unexpected error while waiting for the injections')
        # noinspection PyBroadException
        try:
            for injector in injectors:
                injector.terminate()
                injector.join()
        except Exception:
            logger.exception('failed terminating all injectors after our unexpected error')
    finally:
        signal.signal(signal.SIGINT, signal.default_int_handler)
        signal.signal(signal.SIGTERM, old_sigterm_handler)

        if act_mon.run_event.is_set():
            act_mon.terminate()
        act_mon.join()

        return injectors


def _wait_injection(act_mon: ActivationMonitor,
                    inj_q: SimpleQueue[InjectionNotification],
                    injectors: Collection[Injector],
                    logger: logging.Logger,
                    timeout: float):
    """Wait for the workload injection to terminate.

    Blocks until all injectors are terminated, and all of their invocations have terminated, as signaled by the
    activation monitor.

    In details, this method waits for a notification from the activation monitor, indicating that an invocation
    terminated and it stored its data.
    Then, it consumes all notifications from the injectors, whether indicating that they injected a new invocation, or
    that they terminated.
    In the former case, the method keeps track of the pending invocations.
    This is how it can return when all invocations have terminated: it reconciles the pending invocations with the
    notifications sent by the activation monitor.

    The notification channels with the activation monitor and the injectors, are queues (objects with a method `get()`).

    :param act_mon: the activation monitor thread, to get notifications from
    :param inj_q: the queue where injector threads push notifications
    :param injectors: the injector threads, to monitor their states
    :param logger: a Python logger, to log messages
    """
    timeout_timestamp = None
    # Inner dicts are used as ordered sets.
    pending_acts: dict[str, dict[str, None]] = {inj.trace.user: {} for inj in injectors}
    # Wait for injection while:
    # * there is any injector thread still running
    # * or there are pending activations
    # * and there is no injector thread that ended in error
    while (
            (timeout_timestamp is None or time.time() < timeout_timestamp) and
            (any(injector.exit_state is InjectorExitState.Running for injector in injectors)
             or any(pending_acts.values())) and
            not any(injector.exit_state is InjectorExitState.Error for injector in injectors)):
        # 1. Wait for a notification from the ActivationMonitor.
        mon_notif = None
        try:
            if logger.isEnabledFor(logging.DEBUG):
                logger.debug('waiting for activations: %s', {user: [act[:8] for act in pending_acts[user]] for user in pending_acts})
                for injector in injectors:
                    logger.debug('injector %s is in %s state', injector.name, injector.exit_state.name.lower())
            mon_notif = act_mon.get_notifications()
        except ActivationMonitorError as err:
            raise ActivationMonitorError('failed fetching notifications from the activation monitor') from err
        except Empty:
            pass

        # 2. Consume potential notifications from injectors.
        try:
            while True:
                inj_notif = inj_q.get(block=False)

                if inj_notif['body']['msg'] is InjectionNotificationMessage.Injected:
                    pending_acts[inj_notif['injector'].trace.user][inj_notif['body']['activation']] = None
                    logger.debug('received notification of injection for activation %s of user %s',
                                 inj_notif['body']['activation'], inj_notif['injector'].trace.user)
                elif inj_notif['body']['msg'] is InjectionNotificationMessage.Done:
                    logger.debug('received notification of termination of injector thread %s',
                                 inj_notif['injector'].name)
                    if all(injector.exit_state != InjectorExitState.Running for injector in injectors) and not timeout_timestamp:
                        timeout_timestamp = time.time() + timeout
                        logger.debug('all injectors terminated, set injection timeout to %s (in %ds)', datetime.fromtimestamp(timeout_timestamp), timeout)
                    continue
                else:
                    logger.error('unexpected notification "%s" from injector thread %s', inj_notif['body']['msg'],
                                 inj_notif['injector'].name)
        except Empty:
            pass

        if mon_notif:
            if mon_notif['msg'] != ActivationMonitorNotification.ActivationDataFetched:
                logger.error('unexpected notification "%s" from activation monitor', mon_notif['msg'])
            else:
                logger.debug('received ActivationDataFetched notification for activation %s of user %s',
                             mon_notif['activation'].activationId, mon_notif['activation'].namespace)

                try:
                    # 3. remove activation from pending set
                    del pending_acts[mon_notif['activation'].namespace][mon_notif['activation'].activationId]
                except KeyError:
                    logger.warning(
                        'received unexpected ActivationDataFetched notification for activation %s of user %s (from a '
                        'previous run of FaaSLoad?)',
                        mon_notif['activation'].activationId, mon_notif['activation'].namespace)

    if timeout_timestamp is not None and time.time() >= timeout_timestamp:
        logger.warning('reached time out of injection %ds after all injectors terminated', timeout)
