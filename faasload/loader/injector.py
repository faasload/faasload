"""Injector thread - Inject invocations into a FaaS platform.

One injector thread is tasked with invoking its function under a given user, by following a given injection trace.

The main class is :py:class:`Injector`: the abstract base class to implement platform-dependent injector threads.

Then, the platform-dependent injector threads are:

* :py:class:`OpenwhiskInjector`: for Apache OpenWhisk
* :py:class:`KnativeInjector`: for Knative

The current state of an injector comes from its method :py:func:`Injector.get_state()` (class :py:class:`InjectorState`)
and can be used to follow its progress and health.

An injector thread notifies the main injection thread (the method :py:func:`inject()`) with an instance of
:py:class:`InjectionNotification`, that contains a member of the enumeration :py:class`InjectorNotificationMessage`
to indicate the notification type.
"""

# Copyright Toulouse INP / IRIT / CNRS, 2019 - 2020
# Copyright Télécom SudParis / IP Paris, 2021 - 2025
# Mathieu Bacou and other contributors
#
# mathieu.bacou@telecom-sudparis.eu
#
# This software is a computer program whose purpose is to inject a workload
# to a Function-as-a-Service platform, and to gather data about its execution.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software. You can use,
# modify and/ or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.

from __future__ import annotations

import logging
import time
from abc import ABC, abstractmethod
from collections.abc import Collection, Generator
from datetime import datetime, timedelta
from enum import auto as auto_enum, Enum
from queue import SimpleQueue
from typing import Any, Dict, NamedTuple, Tuple, TypedDict, Union, Mapping

from faasload.lib.timerchain import TimerChain, TimerChainParamWithArgs
from faasload.loader import DatabaseConfiguration, GeneralConfiguration, KnativeConfiguration, OpenWhiskConfiguration
from .database import FaaSLoadDatabase, FaaSLoadDatabaseException
from .generation import GeneratedInjectionTrace
from .traces import InjectionTrace


class InjectorExitState(Enum):
    """Exit states of an Injector thread.

    Exit states are checked by the loader's main method to display a report of the injection.
    They are also used as an end condition for the :py:func:`inject()` method.
    """
    Running = auto_enum()
    Interrupted = auto_enum()
    Error = auto_enum()
    EndOfTrace = auto_enum()


class InjectorState(NamedTuple):
    """The current state of an injector."""
    exit: InjectorExitState
    last_run: int
    global_last_run: int
    trace_state: Any


class InjectionNotificationMessage(Enum):
    """Notifications sent by an Injector thread.

    They are used by the main thread that runs :py:func:`inject()`.

    * :py:attr:`Injected`: sent to notify it of a pending invocation it should wait for.
    * :py:attr:`Done`: sent when the injector is no longer injecting invocations, whether because it encountered an
      erro, or it reached the end of its trace.
    """
    Injected = auto_enum()
    Done = auto_enum()


class InjectionNotificationBody(TypedDict):
    msg: InjectionNotificationMessage
    activation: str | None


class InjectionNotification(TypedDict):
    injector: 'Injector'
    body: InjectionNotificationBody


class InjectionException(Exception):
    """Injecting a trace point failed."""
    pass


class Injector(TimerChain, ABC):
    """Inject one workload trace.

    An injector is not a thread per se;
    rather, it is a chain of timer threads, i.e., of threads that are scheduled to start after a given delay, and where
    each thread schedules the next one before actually doing the injection (i.e., invoking the function).
    For historical reasons and simplicity, the whole chain of threads is seen as a thread from the outside.

    By definition of a workload trace, one injector corresponds to invoking the same function under the same user.

    The main method is :py:func:`schedule()` (from the parent class :py:class:`TimerChain`), to start the chain of timer
    threads.

    The thread chain runs until :py:func:`terminate()` is called (to cancel the pending timer thread), until it reaches
    the end of its workload trace, or until an invocation fails.
    For each successful invocation, it pushes a notification of type `InjectorNotifications.Injected` in the
    `notif_queue` to tell the main thread running :py:func:`inject()` that there are pending activations.
    However the thread terminates, a `InjectorNotifications.Done` notification is put in the queue: check the injector's
    state with :py:func:`get_state()` to determine the cause.

    It is possible to wait for the currently pending timer with :py:func:`wait_injection_point()`.
    **This does not wait for the full trace injection**, but this serves to "join" the injector "thread" after calling
    :py:func:`terminate()`.
    Also note that this does not wait for a timer that is currently executing (i.e., for an injection that is already
    triggered): you should rely on a notification from the activation monitor for that, to wait for the termination of
    the invocation.

    For each invocation, the injector inserts a partial run record into the database.
    The activation monitor eventually completes the record with the invocation data when the latter terminates.

    There is a mechanism to check that the actual time of invocation conforms to the trace.
    It is controlled by the tolerance value from configuration `general.injection.timingerror_reportthreshold`.
    In addition, there is a mechanism to compensate for the lateness of the trigger of a timer thread when scheduling
    the next one.
    """

    def __init__(self,
                 trace: Union[InjectionTrace, GeneratedInjectionTrace],
                 db: FaaSLoadDatabase,
                 notif_queue: SimpleQueue[InjectionNotification] = None,
                 prev_ids: (int, int) = (0, None),
                 timingerror_reportthreshold = timedelta(milliseconds=10)):
        """Initialize a new injector thread.

        The injector will be named "USER/FUNCTION", with USER the user invoking the function named FUNCTION (taken from
        `trace`).

        This opens a connection to the database (instantiates :py:class:`FaaSLoadDatabase`).

        :param trace: the workload trace to inject
        :param db: the connection to the database
        :param notif_queue: the queue to notify the main injection thread of invocations and termination (default: None)
        :param prev_ids: IDs of the previous run of the injector (see below)

        If `notif_queue` is None, **it is expected to be set after instance creation via**
        :py:func:`set_notification_queue()`.

        `prev_ids` can be used to restore the state of an injector; this is used in generator mode.
        The first element is considered as the ID of the previous run of the injector, so its first run will have ID
        `prev_ids[0] + 1`.
        Similarly, the second element is considered as the global ID of the previous run of the injector (i.e., the ID
        in the table of runs of all the injectors); this is only used for display purposes and in the injector state.
        It will be overwritten with the first successful injection run.
        """
        self.name = trace.user + '/' + trace.function.name

        self.trace = trace

        self.notification_queue = notif_queue

        self.db = db

        # The local run counter, increased by 1 after every invocation
        self.run_id = prev_ids[0]
        # The absolute run counter, i.e. the ID returned by the database after inserting the partial run data that
        # accounts for all runs of all Injector threads.
        self.global_run_id = prev_ids[1]

        self.timingerror_reportthreshold = timingerror_reportthreshold 

        TimerChain.__init__(self, self._generate_injection_parameters(self.run_id))

        self.exit_state = InjectorExitState.Running

        self.logger = logging.LoggerAdapter(logging.getLogger('injector'), {'injectorName': self.name})

    def _generate_injection_parameters(self, starting_run_id: int = 0) -> Generator[
        TimerChainParamWithArgs[Tuple[int, Mapping[str, Any], datetime]], None, None]:
        for trace_point_id, point in enumerate(self.trace):
            yield point.wait.total_seconds(), (
                starting_run_id + trace_point_id, point.params, datetime.now() + point.wait)

    def set_notification_queue(self, notif_queue: SimpleQueue[InjectionNotification]):
        """Set the notification queue of the injector thread.

        See the documentation of the class :py:class:`Injector` for more details.

        :param notif_queue: the queue to notify the main injection thread of invocations and termination
        """
        self.notification_queue = notif_queue

    def terminate(self):
        """Interrupt the injector thread.

        This method prevents the thread from injecting the next invocations, cancelling any pending one.
        It does not interrupts a currently running invocation.
        """
        self.logger.warning('interrupted (last good run: %d)', self.run_id)

        self._terminate_with(InjectorExitState.Interrupted)

    def _terminate_with(self, state: InjectorExitState):
        super().terminate()

        self.exit_state = state

        if self.notification_queue is None:
            self.logger.warning('notification queue is unset: cannot notify termination')
        else:
            self.notification_queue.put(InjectionNotification(
                injector=self,
                body=InjectionNotificationBody(
                    msg=InjectionNotificationMessage.Done,
                    activation=None,
                )
            ))

    def run(self, run_id: int, params: Dict[str, Any], target_time: datetime):
        """Inject one trace point, and schedule the injection of the next one.

        :param run_id: the ID of the run in the injection trace
        :param params: the parameters of the function invocation, including the function input
        :param target_time: the time at which the trace point was expected to be injected

        Note that this method catches all exceptions, so that the injector does not crash, and gracefully terminates.

        Also note that there is a mechanism to compensate any lateness in triggering a timer thread: this lateness is
        compensated from the delay of the next thread.
        """
        self.logger.info('run #%d', run_id + 1)

        now = datetime.now()

        # Compensate the lateness of this injection on the wait for the next one.
        self.schedule(interval_delta=target_time - now)

        if abs(now - target_time) > self.timingerror_reportthreshold:
            self.logger.warning('injection of trace point #%d %s by %s (greater than the maximum wait error of %s',
                                run_id + 1, "ahead" if now < target_time else "late", abs(now - target_time),
                                self.timingerror_reportthreshold)

        # noinspection PyBroadException
        try:
            if self.logger.isEnabledFor(logging.INFO):
                params_str = ', '.join(f'{name}={value}' for name, value in params.items())
                self.logger.info('invoking %s(%s)', self.trace.function.name, params_str)
            global_run_id, activation_id = self._inject_one(params)
        except InjectionException:
            self.logger.exception('failed injecting trace point #%d: %s', run_id + 1, params)

            self._terminate_with(InjectorExitState.Error)
        except Exception:
            if self.global_run_id:
                self.logger.exception(
                    'unexpected error (last good run #%d, global run #%d)', self.run_id, self.global_run_id)
            else:
                self.logger.exception('unexpected error (first run of the injector)')

            self._terminate_with(InjectorExitState.Error)
        else:
            self.run_id = run_id + 1
            self.global_run_id = global_run_id

            self.logger.debug('activated with ID %s for run #%d (global run #%d)', activation_id, self.run_id,
                              self.global_run_id)
            if self.notification_queue is None:
                self.logger.warning('notification queue is unset: cannot notify injection')
            else:
                self.notification_queue.put({
                    'injector': self,
                    'body':     {
                        'msg':        InjectionNotificationMessage.Injected,
                        'activation': activation_id,
                    },
                })

    def get_state(self) -> InjectorState:
        """Get the state.

        This state can be used to check the exit state of this injector, and to checkpoint it and its workload trace.

        :returns: the state of this injector
        """
        return InjectorState(exit=self.exit_state, last_run=self.run_id, global_last_run=self.global_run_id,
                             trace_state=self.trace.get_state())

    def _insert_partial_run(self, activation_id: str, params: Dict[str, Any]) -> int:
        try:
            rec_id = self.db.insert_partial_run({
                'function_id':   self.trace.function.id,
                'namespace':     self.trace.user,
                'activation_id': activation_id,
            })
            self.db.insert_parameters(rec_id, params)
        except FaaSLoadDatabaseException as err:
            raise InjectionException('failed storing partial run into the database') from err

        return rec_id

    def end_of_chain(self) -> None:
        """Successful end of the injection chain."""
        self.logger.info('reached end of injection trace after %d runs', self.run_id)

        self._terminate_with(InjectorExitState.EndOfTrace)

    @abstractmethod
    def _inject_one(self, params: Dict[str, Any]) -> Tuple[int, str]:
        """Inject one trace point, i.e., execute the function with given parameters.

        :param params: the parameters of the function invocation, including the function input

        :returns: a tuple of the global ID of the run as stored in the database, and the ID of the invocation (from the
        FaaS platform) for the run
        """
        raise NotImplementedError


class OpenwhiskInjector(Injector):
    """Platform-dependent implementation of the injector thread for Apache OpenWhisk."""

    def __init__(self, *args, common_cfg: GeneralConfiguration, cfg: OpenWhiskConfiguration, **kwargs):
        """Initialize a new injector thread for Openwhisk.

        :param common_cfg: the general configuration of FaaSLoad
        :param cfg: the configuration to use OpenWhisk

        For all other parameters, see parent class :py:class:`Injector`.
        """
        super().__init__(*args, **kwargs)

        self.fetchlogs_cfg = common_cfg['fetchlogs']
        self.cfg = cfg

    def _inject_one(self, params: Dict[str, Any]) -> Tuple[int, str]:
        from pywhisk.client import OpenWhiskException, ActionError
        import pywhisk.action as action

        try:
            activation_id = action.invoke(self.trace.function.name, self.trace.user, self.cfg, params,
                                          blocking=False).activationId
        except (OpenWhiskException, KeyError) as err:
            raise InjectionException('failed invoking function') from err
        except ActionError as err:
            # If the action failed for a "developer error", we try to get its logs for convenience
            if self.logger.isEnabledFor(logging.WARNING):
                app_logs = self._try_get_app_logs(
                    err.activation.activationId,
                    int(self.fetchlogs_cfg['timeout'] // self.fetchlogs_cfg['backofftime']),
                    self.fetchlogs_cfg['backofftime']
                )
                self.logger.warning('failed invoking function: application error\n' + '\n\t'.join(app_logs))
            activation_id = err.activation.activationId

        self.logger.debug(f'invoked {self.trace.function.name} under activation {activation_id}')

        return self._insert_partial_run(activation_id=activation_id, params=params), activation_id

    def _try_get_app_logs(self, activation_id: str, max_tries: int, back_off: float) -> Collection[str]:
        import pywhisk.activation as activation
        from pywhisk.client import OpenWhiskException

        for _ in range(max_tries):
            try:
                return activation.logs(activation_id, '_', self.cfg)
            except OpenWhiskException:
                time.sleep(back_off)

        return [f'logs for activation {activation_id} unavailable']


class KnativeInjector(Injector):
    """Platform-dependent implementation of the injector thread for Knative."""

    def __init__(self, *args, cfg: KnativeConfiguration, **kwargs):
        """Initialize a new injector thread for Knative.

        :param cfg: the configuration to use Knative

        For all other parameters, see parent class :py:class:`Injector`.
        """
        super().__init__(*args, **kwargs)

        self.cfg = cfg

    def _inject_one(self, params: Dict[str, Any]) -> Tuple[int, Any]:
        import pyknative.action as action
        from pyknative.models import ApiConfiguration
        try:
            activation_id = action.invoke(
                self.trace.function.name.replace("/", "-"), namespace="default", config=ApiConfiguration(
                    ip=self.cfg['ip'],
                    tracer_ip=self.cfg['tracer_ip'],
                    gateway_component_name=self.cfg['gateway_component_name'],
                )
            )
        except Exception as e:
            raise e
        return self._insert_partial_run(activation_id=activation_id, params=params), activation_id
