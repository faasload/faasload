"""TimerChain - A daisy chain of timer threads.

:py:class:`TimerChain` is a chain of `Python timer threads
<https://docs.python.org/3/library/threading.html#threading.Timer>`_, i.e., each thread schedules the next one before
executing the body.

The purpose is to reliably schedule timed operations, with the same body of code but varying parameters, and with
controllable interval.
In FaaSLoad, this is used to inject invocations, and to monitor the memory usage periodically.
"""

# Copyright Toulouse INP / IRIT / CNRS, 2019 - 2020
# Copyright Télécom SudParis / IP Paris, 2021 - 2025
# Mathieu Bacou and other contributors
#
# mathieu.bacou@telecom-sudparis.eu
#
# This software is a computer program whose purpose is to inject a workload
# to a Function-as-a-Service platform, and to gather data about its execution.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software. You can use,
# modify and/ or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.

from __future__ import annotations

import time
from abc import ABC, abstractmethod
from collections.abc import Iterable, Mapping
from datetime import timedelta
from threading import Event, Thread, Timer
from typing import Any, Optional, Tuple, Union, TypeVar

_ArgsType = TypeVar('_ArgsType')
TimerChainParamWithArgs = Tuple[float, Iterable[_ArgsType] | None]
TimerChainParamWithKwargs = Tuple[float, Iterable[_ArgsType] | None, Mapping[str, Any] | None]


class TimerChain(ABC):
    """A daisy chain of timer threads.

    Subclass this class and implement the method :py:func:`run()`, the body of all timer threads in the chain.

    The main method is :py:func:`schedule()`, which starts the chain.
    Then, to perpetuate the chain, **you must call it in your implemented body**.

    The chain lasts as long as there are delays specifying how much time to wait before executing the next thread.
    The delays (as well as the parameters sent to the threads) are given at the creation of the timer chain.
    It means that your implementation of :py:func:`run()` does not have to handle the end of the chain: this abstract
    base class does it by itself when the iterable of delays (and parameters) is exhausted.
    Nonetheless, your implementation may choose to stop scheduling the next thread, in the case of an error for example.

    At the natural end of the chain, i.e., when all delays are exhausted, the method :py:func:`end_end_of_chain()`
    is called: by default, it does nothing, but you can override it to execute some cleanup task.
    It is guaranteed to happen after the last timer thread has finished its execution.

    The chain can be terminated by an external caller with the method :py:func:`terminate()`: it prevents scheduling the
    next thread, and cancels it if it is not already executing.

    The method :py:func:`join()` can be used to join on the whole chain of timer, i.e., wait for the last one and join
    it (see :py:func:`join()` for details).
    In addition, the method :py:func:`join_one()` can be used to join only the next scheduled thread.
    """

    def __init__(self,
                 chain_params: Iterable[Union[
                     float,
                     Tuple[float],
                     TimerChainParamWithArgs,
                     TimerChainParamWithKwargs]]):
        """Initialize a new chain of timer threads.

        :param chain_params: the delays and parameters for every timer thread in the chain

        The instance iterates through `chain_params` to schedule the timer threads: when :py:func:`schedule()` is
        called, it schedules the next thread in the chain to execute after the given delay (in seconds).

        The elements in `chain_params` may be:

        * a float: the scheduling delay, in seconds
        * a tuple with only a float: a convenience interface, equivalent to just a float
        * a tuple of a float and an iterable: the scheduling delay, and the list of positional arguments to pass to the
          :py:func:`run()` method that is going to be executed in the scheduled thread
        * a tuple of a float, an iterable and a mapping: same as above, but with additional keyword arguments

        They do not have to be homogeneous (but it seems weird to do otherwise).

        A timer chain can be made infinite by giving an iterator on a callable (using `iter(callable, sentinel)`) as
        `chain_params`: when iterating on it, the callable is called to produce the next value.
        If this value equals the sentinel value, the iterator raises :py:class:`StopIteration`.
        See `the Python documentation <https://docs.python.org/3/library/functions.html#iter>`_.
        """
        self.timer = None
        self.end_of_chain_thread = None

        self.terminated = Event()
        self.terminated.clear()

        self.chain_params = iter(chain_params)

    def terminate(self):
        """Terminate the timer chain.

        Prevents scheduling the next timer thread, and cancels the next scheduled thread.
        """
        self.terminated.set()
        if self.timer is not None:
            self.timer.cancel()

    def join_one(self, timeout: Optional[float] = None):
        """Join the next scheduled timer thread, i.e., wait for the end of its execution.

        Note that you usually cannot join the currently executing thread, because it (i.e., the :py:func:`run()` method) is
        usually implemented by scheduling the next thread at the start.
        This class only keeps track of the next scheduled thread.

        :param timeout: the timeout (in seconds) for the join operation on the timer thread
        """
        if self.timer is not None:
            self.timer.join(timeout)

    def join(self, timeout: Optional[float] = None):
        """Wait until the timer chain terminates.

        This is equivalent to joining each timer successively.

        This method also waits for the method :py:func:`end_of_chain()` to terminate if it is run.
        See that method for details on the end of the chain.
        """
        die_at = time.time() + timeout if timeout is not None else None

        # Note that Timer.join() accepts negative numbers (and returns immediately).
        def get_one_timeout():
            return die_at - time.time() if die_at is not None else None

        while self.timer is not None and self.timer.is_alive():
            # Keep a local reference: it can be replaced by a new Timer by the run() method.
            joined_timer = self.timer
            joined_timer.join(get_one_timeout())
            if joined_timer.is_alive():
                # Timed out
                break
        if self.end_of_chain_thread is not None:
            self.end_of_chain_thread.join(get_one_timeout())

    def is_alive(self) -> bool:
        """Tell whether the timer chain is alive.

        The chain is alive if the next timer thread is scheduled or executing (including :py:func:`end_of_chain()`).

        :returns: `True` if the chain is alive, `False` otherwise
        """
        return (self.timer is not None and self.timer.is_alive()) or (
                self.end_of_chain_thread is not None and self.end_of_chain_thread.is_alive())

    def end_of_chain(self):
        """Execute one last thread at the end of the chain.

        Override this method if needed, by default it does nothing.

        This method is only executed if the timer chain terminates naturally, i.e., it is not terminated by a call to
        :py:func:`terminate()`, and it has been scheduled to the end of the delays given at instantiation time.
        """
        pass

    def _end_of_chain(self):
        # Wait for the last timer to terminate
        self.join_one()
        self.end_of_chain()

    @abstractmethod
    def run(self, *args: Any, **kwargs: Any):
        """The body of every timer thread in the chain.

        **To continue the chain, you must call** :py:func:`schedule()`.
        You can conditionally choose to not do so, to end the chain sooner based on any custom logic.
        Note that in this case, :py:func:`end_of_chain()` cannot be executed.

        Anyways, you do not have to manage the end of the chain in this method: simply always call :py:func:`schedule()`
        as long as you want to continue the chain.
        """
        raise NotImplementedError

    def schedule(self, interval_delta: timedelta = timedelta(microseconds=0)) -> bool:
        """Schedule the next timer thread, and return immediately.

        Starts the chain, or schedule the next timer thread in line by iterating on the delays and arguments provided at
        instantiation time.
        **You also have to call it** in your :py:func:`run()` implementation to schedule the next timer thread to
        perpetuate the chain until termination.

        `interval_delta` can be used to dynamically adjust the delay before the next timer thread: it is added to the
        scheduled delay (`interval_delta` can be negative).
        It allows to correct any sway introduced by the scheduling of the timer thread.

        If the iterator of delays (and arguments) is exhausted, run :py:func:`end_of_chain()` after the last timer
        thread has terminated.

        :returns: True if the next thread was scheduled, False otherwise (False means the natural end of the chain)
        """
        if self.terminated.is_set():
            return False

        try:
            params = next(self.chain_params)
        except StopIteration:
            self.terminated.set()
            # Run the end_of_chain() method after the last Timer is finished.
            # Run it in a thread to return immediately.
            self.end_of_chain_thread = Thread(target=self._end_of_chain)
            self.end_of_chain_thread.start()
            return False

        try:
            interval = params[0]
        except TypeError:
            # Just a float
            interval = params
            args = None
            kwargs = None
        else:
            try:
                args = params[1]
            except IndexError:
                args = None
            try:
                kwargs = params[2]
            except IndexError:
                kwargs = None

        self.timer = Timer(interval + interval_delta.total_seconds(), self.run, args, kwargs)

        self.timer.start()
        return True
