"""Library module for FaaSLoad."""

# Copyright Toulouse INP / IRIT / CNRS, 2019 - 2020
# Copyright Télécom SudParis / IP Paris, 2021 - 2025
# Mathieu Bacou and other contributors
#
# mathieu.bacou@telecom-sudparis.eu
#
# This software is a computer program whose purpose is to inject a workload
# to a Function-as-a-Service platform, and to gather data about its execution.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software. You can use,
# modify and/ or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.

import logging.config
import sys
from logging import Logger
from typing import Any, IO

import yaml

from faasload import __version__ as my_version


def prepare_and_read_configuration(f: IO) -> dict[str, Any]:
    """Wrapper around the YAML loader function to read the configuration before processing it.

    This function is used as the format reader for the function `read()` of configuration-reader, to read the
    configuration from the file.

    Remove the logging section because it is not part of FaaSLoad's configuration, and it would not play nice with the
    configuration processing done by configuration-reader.
    """
    conf = yaml.full_load(f)
    del conf['logging']
    return conf


def configure_logging(config_filepath: str):
    """Configure the logging module with the configuration read from file."""
    try:
        with open(config_filepath, 'r') as config_file:
            conf = yaml.full_load(config_file)['logging']
    except (OSError, yaml.YAMLError):
        logging.exception('failed reading logging configuration from "%s": aborting', config_filepath)
        sys.exit(1)
    except KeyError:
        logging.error('failed reading logging configuration from "%s": missing "logging" section', config_filepath)
        sys.exit(1)
    else:
        logging.config.dictConfig(conf)


def log_faasload_preamble(logger: Logger):
    """Log a preamble for any component of FaaSLoad."""
    logger.info('FaaSLoad %s', my_version)
    logger.info('Copyright Télécom SudParis / IP Paris, 2020 - 2025')
    logger.info('Written by Mathieu Bacou and others')
