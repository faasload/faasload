"""Controllable thread - A thread which execution can be controlled.

:py:class:`ControllableThread` is a thread that executes a loop.
The thread, i.e., the loop, can be paused, resumed and terminated.
"""

# Copyright Toulouse INP / IRIT / CNRS, 2019 - 2020
# Copyright Télécom SudParis / IP Paris, 2021 - 2025
# Mathieu Bacou and other contributors
#
# mathieu.bacou@telecom-sudparis.eu
#
# This software is a computer program whose purpose is to inject a workload
# to a Function-as-a-Service platform, and to gather data about its execution.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software. You can use,
# modify and/ or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.

from __future__ import annotations

from abc import ABC, abstractmethod
from threading import Event, Thread


class ControllableThread(ABC, Thread):
    """A thread that executes a loop under control for pausing, resuming and terminating.

    Subclass this class and implement the loop's body by implementing its method :py:func:`controlled_run()`.

    The thread can be paused with :py:func:`pause()`, resumed with :py:func:`resume()`, and terminated with
    :py:func:`terminate()`.
    It also terminates if the implemented loop's body returns `False`.

    In addition, a :py:func:`setup()` can be executed once at the beginning of the thread, before executing the first
    iteration of the loop.
    """

    def __init__(self, group: None = None, name: str = None, *, daemon: bool = None):
        """Intialize a controllable thread.

        :param group: thread group (superclass :py:class:`Thread`)
        :param name: thread name (superclass :py:class:`Thread`)
        :param daemon: whether the thread is a daemon thread (superclass :py:class:`Thread`)
        """
        super().__init__(group, name, daemon=daemon)

        self.pause_resume_event = Event()
        self.pause_resume_event.set()
        self.stop_event = Event()

    def run(self):
        """The body of the thread: a loop running the method :py:func:`controlled_run()`.

        The thread terminates if:

        * the method :py:func:`setup()` prevents continuing into the loop
        * the loop's body :py:func:`controlled_run()` prevents continuing to the next iteration
        * the thread is asked to terminate by the method :py:func:`terminate()`
        """
        cont = self.setup()
        if not cont:
            return

        while cont and not self.stop_event.is_set():
            self.pause_resume_event.wait()

            # The thread may have been terminated while paused.
            if not self.stop_event.is_set():
                cont = self.controlled_run()

    def setup(self) -> bool:
        """Run a one-time setup in the thread, before running the first iteration of the controlled loop.

        This method returns whether the thread should continue into the loop.

        :returns: True if the thread should continue into the controlled loop
        """
        return True

    @abstractmethod
    def controlled_run(self) -> bool:
        """The body of the controlled loop.

        This method returns whether the thread should continue to the next iteration of the loop.

        :returns: True if the controlled loop should continue
        """
        raise NotImplementedError

    def pause(self):
        """Pause the controlled thread."""
        self.pause_resume_event.clear()

    def resume(self):
        """Resume the paused controlled thread."""
        self.pause_resume_event.set()

    def terminate(self):
        """Terminate the controlled thread."""
        self.stop_event.set()
        # The thread may be waiting in paused state.
        self.resume()
