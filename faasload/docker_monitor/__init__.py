"""Docker monitor - Monitor to the FaaS platform's Docker containers that run functions.

Run the monitor as:

.. code-block::

    python -m faasload.docker_monitor

You must run one instance of the monitor on each Kubernetes worker node where function containers can be scheduled by
the FaaS platform.

Overview
--------

It has four components:

1. a Docker operations handler (class :py:class:`DockerOperationsHandler`): the monitor listens to the Docker socket for
   operations (container creation, destruction, etc.), that are reflected on the monitor threads (see next component);
2. monitor threads (class :py:class:`DockerMonitor`): for each monitored Docker container, the monitor spawns a new
   thread to monitor it;
3. a measurements server (class :py:class:`MeasurementsServer`): monitor threads store data continuously, that can be
   retrieved by programs that are clients of the monitor via the measurements server, which listens on a socket to serve
   measurements of memory usage, and performance counters;
4. a Docker operations log server (class :py:class:`DockerOperationsLogServer`): similarly, a client to the monitor can
   request a log of Docker operations.

Working scenario
----------------

The monitor as a whole works as follows:

1. its Docker operations handler listens to the Docker socket, waiting for container operations, and its measurements
   and Docker operations log server listens for queries on their sockets;
2. when a FaaS function is invoked, the FaaS platform runs a container, and thus sends a message to the Docker socket
   that is seen by the Docker operations handler;
3. the Docker operations handler spawns a new monitor thread to monitor the new container, and keeps a log of this
   operation;
4. unless paused or destroyed (see next steps), the monitor thread continuously stores memory usage, and perf counters
   data, of the target container at regular intervals;
5. after the function finishes, the FaaS platform pauses the container through the Docker socket, so the Docker
   operations handler also pauses the thread that monitors the paused container;
6. a main program using the monitor (i.e., that is a client of the monitor, like FaaSLoad's loader) will typically send
   a request to the measurements server on its socket at this point, to retrieve resource usage data about the function
   that just ran

   1. to answer the query, the measurements server fetches resource usage data from the monitor thread of the request
      Docker container

7. if a function is invoked that can use the same container, the FaaS platform resumes it through the Docker socket, and
   the Docker operations handler also resumes the monitoring thread;
8. eventually, the container is destroyed by the FaaS platform through the Docker socket, so the Docker operations
   handler destroys the monitoring thread attached to the container;
9. eventually, the main program using the monitor will finish its task, and send a request to the Docker operations log
  server on its socket, to retrieve the log of Docker operations.

Summary: at the center is the Docker operations handler, that manages monitor threads depending on container operations
sent by the FaaS platform on the Docker socket;
monitor threads store resource usage data of function containers, which can be queried via the measurements server;
the Docker operations handler also keeps a log of the Docker operations, which can be queried via the Docker operations
log server.
"""

# Copyright Toulouse INP / IRIT / CNRS, 2019 - 2020
# Copyright Télécom SudParis / IP Paris, 2021 - 2025
# Mathieu Bacou and other contributors
#
# mathieu.bacou@telecom-sudparis.eu
#
# This software is a computer program whose purpose is to inject a workload
# to a Function-as-a-Service platform, and to gather data about its execution.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software. You can use,
# modify and/ or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.

# Version of the monitor, independent to FaaSLoad's global version
__version__ = '2.1.0'

from typing import TypedDict


class DockerOperationsMonitorConfiguration(TypedDict):
    base_url: str
    cert_path: str | None
    namefilters: list[str]


class MonitorConfiguration(TypedDict):
    resolution: float
    maxage: int
    perf_counters: list[str]


# Passed to TCPServer, must be exactly a tuple (address, port)
class MonitorServerConfiguration(TypedDict):
    address: str
    port: int


# Passed to TCPServer, must be exactly a tuple (address, port)
class DockerOpsLogServerConfiguration(TypedDict):
    address: str
    port: int


class DockerMonitorConfiguration(TypedDict):
    dockeroperations: DockerOperationsMonitorConfiguration
    monitor: MonitorConfiguration
    measurementserver: MonitorServerConfiguration
    dockeropslogserver: DockerOpsLogServerConfiguration


DEFAULTS = {
    'dockeroperations':   DockerOperationsMonitorConfiguration(
        base_url='unix://var/run/docker.sock',
        cert_path=None,
        namefilters=[
            r'^/?k8s_user-action_wsk.+-invoker-[0-9][0-9]-[0-9]+-.+-.+_.+_[-0-9a-f]{36}_[0-9]+$',
            r'^wsk[0-9][0-9]_[0-9]+_.+_.+$',
        ],
    ),
    'monitor':            MonitorConfiguration(
        resolution=0.1,
        maxage=1800,
        perf_counters=[],
    ),
    'measurementserver':  MonitorServerConfiguration(
        address='0.0.0.0',
        port=11085,
    ),
    'dockeropslogserver': DockerOpsLogServerConfiguration(
        address='0.0.0.0',
        port=11086,
    ),
}
