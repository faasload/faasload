"""Monitor threads - Monitor memory usage and performance counters of Docker containers.

The Docker operations handler manages monitor threads (instances of the class :py:class:`DockerMonitor`).

Actually, a monitor thread is a couple of

1. a thread to monitor the memory usage (class :py:class:`MemoryThread`)
2. a thread to run `perf <https://perf.wiki.kernel.org/index.php/Main_Page>`_ to monitor performance counters (class
   :py:class:`PerfThread`)

The class :py:class:`DockerMonitor` provides a unified interface over the two threads for control, and to query
measurements.
"""

# Copyright Toulouse INP / IRIT / CNRS, 2019 - 2020
# Copyright Télécom SudParis / IP Paris, 2021 - 2025
# Mathieu Bacou and other contributors
#
# mathieu.bacou@telecom-sudparis.eu
#
# This software is a computer program whose purpose is to inject a workload
# to a Function-as-a-Service platform, and to gather data about its execution.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software. You can use,
# modify and/ or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.

from __future__ import annotations

import logging
import signal
import subprocess
import time
from collections import deque, namedtuple
from collections.abc import Generator, MutableSequence, Sequence
from datetime import datetime
from math import ceil
from typing import Optional, Collection

import pycgroupv2 as cg
import pycgroupv2.error
from pycgroupv2 import from_path as cgroup_from_path

from faasload.lib.controllable_thread import ControllableThread
from faasload.lib.timerchain import TimerChain
from . import MonitorConfiguration

Measurement = namedtuple('Measurement', ['date', 'counter', 'value'])


class DockerMonitor:
    """Monitor thread.

    Historically a thread, now more a wrapper over two threads: instances of the class :py:class:`MemoryThread` and
    :py:class:`PerfThread`.

    Provides an unified interface over the two threads to control them (pausing, resuming, terminating), and to query
    measurements.

    The perf thread is only created if there are performance counters to monitor, i.e., performance counters were
    specified in the configuration.

    Sub-threads (the perf thread and the memory monitor thread) write their measurements directly to a shared deque,
    i.e., a queue optimized for access on both ends;
    it is used with a `maxlen` to automatically limit the number of kept measurements.
    """

    def __init__(self, container_id: str, k8s_id: str, cfg: MonitorConfiguration):
        """Create a new monitor thread.

        :param container_id: the ID of the container to monitor
        :param k8s_id: the ID of the Kubernetes pod parent of the container to monitor
        :param cfg: the configuration for the monitor thread

        The Kubernetes pod ID and the Docker container ID are composed to build the path to the directory of the
        container's control group.
        """
        self.container_id = container_id

        self.resolution = cfg['resolution']
        self.perf_counters = cfg['perf_counters']

        # Measurements for a given container are deques used as circular lists: new measurements are added with
        # `append`, and will replace the oldest ones when maxlen is reached.
        # The measurement server will fetch measurements in a given interval simply by iterating over the deque, and
        # no measurement is ever removed (only replaced by newer ones).
        self.measurements = deque(maxlen=ceil(cfg['maxage'] / self.resolution))

        self.perf_thread = PerfThread(self.container_id, k8s_id, self.resolution, self.measurements,
                                      self.perf_counters) if self.perf_counters else None
        self.memory_poller_thread = MemoryThread(self.container_id, k8s_id, self.resolution, self.measurements)

        self.logger = logging.getLogger('monitor')

    def start(self):
        """Start the monitor thread, i.e., start its perf thread if enabled, and its memory monitor thread.

        :raises RuntimeError: if started more than once (like Python threads)
        """
        if self.perf_thread is not None:
            self.perf_thread.start()
        self.memory_poller_thread.start()

    def pause(self):
        """Pause the monitoring thread: no more measurements are stored until resumed."""
        if self.perf_thread is not None:
            self.perf_thread.pause()
        self.memory_poller_thread.pause()

    def resume(self):
        """Resume the paused monitoring thread."""
        if self.perf_thread is not None:
            self.perf_thread.resume()
        self.memory_poller_thread.resume()

    def terminate(self):
        """Terminate the monitoring thread."""
        self.logger.debug('terminating monitoring thread for container %s', self.container_id)
        if self.perf_thread is not None:
            self.perf_thread.terminate()
        self.memory_poller_thread.terminate()

    def join(self, timeout: Optional[float] = None):
        """Wait for termination of the monitoring thread.

        Waits for the perf thread if enabled, then waits for the memory monitor thread.

        :param timeout: abort the join after this number of seconds

        The timeout is passed directly to the join operation on each sub-thread, so this method's timeout can actually
        be twice `timeout` if the perf thread is enabled.
        """
        if self.perf_thread is not None:
            self.perf_thread.join(timeout)
        # Technically, should substract the time spent joining the perf_thread...
        self.memory_poller_thread.join(timeout)

    def is_alive(self) -> bool:
        """Tell whether the monitor thread is alive.

        The monitor thread is alive if any of the perf thread or the memory monitor thread are alive.

        :returns: `True` if the thread is alive, `False` otherwise
        """
        return (self.perf_thread is not None and self.perf_thread.is_alive()) or self.memory_poller_thread.is_alive()

    def get_measurements(self, start: int, end: int) -> Sequence[Measurement]:
        """Get measurements for the specified timeslice.

        Get all measurements (class :py:class:`Measurement`) produced by the perf thread and the memory thread between
        `start` and `end`.

        This method computes the expected number of measurements is should return if all measurements were done by the
        threads, and emits a warning if there are missing measurements.

        :param start: timestamp (in seconds) of the beginning of the requested timeslice
        :param end: timestamp (in seconds) of the end of the requested timeslice

        :returns: the list of measurements

        The returned list is not guaranteed to be sorted by date;
        there is a great chance it is, due to measurements being stacked on a deque, but concurrency effects may
        disorder them.
        """
        ret = [m for m in self.measurements if start <= m.date <= end]

        expected = ((self.perf_thread.produced_measurements if self.perf_thread is not None else 0) +
                    self.memory_poller_thread.produced_measurements) * ((end - start) // self.resolution)
        missing = expected - len(ret)
        if expected > 0 and missing > 0:
            self.logger.warning(
                'missing %d/%d measurements (%f%%) over queried time period [%s, %s] for container %s', missing,
                expected, 100 * missing / expected, datetime.fromtimestamp(start).isoformat(),
                datetime.fromtimestamp(end).isoformat(), self.container_id)

        return ret


class PerfThread(ControllableThread):
    """Perf thread - A monitor thread to collect performance counters using perf.

    This thread spawns a subprocess for the perf command, and then loops reading from its output to read and
    parse perf's output into measurements stacked in the deque shared with the main monitor thread.

    This is a :py:class:`ControllableThread`, so its "loop" body is defined by :py:func:`controlled_run`.
    It stacks measurements parsed from the perf subprocess.
    The method :py:func:`parse_stat` is responsible for the parsing.
    It is a generator that yields measurements, and returns after about 1s worth of measurements have been parsed.
    This is to respect :py:func:`ControllableThread`'s contract, to give a chance to terminate the thread graciously.
    """

    def __init__(self, container_id: str, k8s_id: str, resolution: float, output_queue: MutableSequence[Measurement],
                 counters: Collection[str]):
        """Initialize a new perf thread.

        :param container_id: the ID of the container to monitor
        :param k8s_id: the ID of the Kubernetes pod parent of the container to monitor
        :param resolution: the resolution of the measurements, i.e., the period between two measurements (in seconds)
        :param output_queue: the queue where to push measurements
        :param counters: the list of counters to monitor using perf
        """
        super().__init__(name='monitor-' + container_id + '-perf', daemon=True)

        self.container_id = container_id
        self.k8s_id = k8s_id
        self.resolution = resolution * 1000
        self.output_queue = output_queue

        self.counters = counters

        self.perf_process = None
        self.perf_subproc_start_date = None

        self.missed = 0

        self.produced_measurements = len(counters)

        self.logger = logging.getLogger('monitoringthread.perf')

    def setup(self) -> bool:
        """One-time setup of the thread: start the perf subprocess.

        :returns: `True` is the subprocess was created successfully, and the thread can continue executing.
        """
        try:
            self.start_process()
            return True
        except OSError:
            self.logger.exception('failed starting performance monitoring process')
            return False

    def start_process(self):
        """Start the perf subprocess.

        The following command is run to execute perf:

            perf stat -I <period> -e <metrics> --cgroup=docker/<idc>

        Plus `--no-big-num` to disable printing with thousands separators, and ";" used as the field separator.

        Metric names are as displayed by `perf list`. The list of available counters depends on the host platform.
        """
        cgroup_path = (f'kubepods.slice/kubepods-burstable.slice/kubepods-burstable-pod{self.k8s_id}.'
                       f'slice/docker-{self.container_id}.scope')

        perf_cmd = [
            'perf', 'stat',
            '-I', str(round(self.resolution)),
            '-e', ','.join(self.counters),
            '--cgroup=' + cgroup_path,
            '--no-big-num',
            '--field-separator', ';',
        ]

        self.logger.debug('starting performance monitoring subprocess: %s', perf_cmd)

        # perf stat outputs on stderr by default
        self.perf_process = subprocess.Popen(perf_cmd, stderr=subprocess.PIPE, text=True)
        self.perf_subproc_start_date = round(time.time(), 3)

    def parse_stat(self, max_measurements_per_counter) -> Generator[Measurement]:
        """Parse stderr of the perf command for a given count of measurements per counter.

        This is a generator that yields measurements one by one for all the counters.

        The limit imposed by `max_measurements_per_counter` makes the function return after some time.
        This is to allow the controlled loop to evaluate the stop and pause events.

        :returns: a generator yielding measurements
        """
        nb_measurements = 0
        bad_lines = 0

        # perf stat outputs on stderr by default
        for line in self.perf_process.stderr:
            # Expected: DATE VALUE UNIT COUNTER RUN_TIME PERCENT_RUN_TIME METRIC_VALUE METRIC_UNIT
            line = line.strip().split(';')
            try:
                date = line[0]
                value = line[1]
                counter = line[3].split(':')[0]
            except IndexError:
                self.logger.warning('malformed or unexpected line in perf output: %s', line)
                bad_lines += 1
                if nb_measurements >= len(self.counters) * max_measurements_per_counter:
                    break
                continue

            if counter not in self.counters:
                self.logger.warning('unexpected counter in perf output: %s', counter)
                # Just ignore it, don't increment nb_measurements
                continue

            # At this point, it is necessarily a good line for an expected counter.
            nb_measurements += 1

            date = float(date)
            try:
                value = float(value)
            except ValueError:
                # It can only come from trying to convert value to float
                if value == '<not counted>':
                    value = None
                else:
                    self.logger.warning('failed parsing value for counter %s: %s', counter, value)
                    continue

            self.logger.debug('parsed perf line on date %s: %s=%s', self.perf_subproc_start_date + date, counter, value)

            yield Measurement(date=self.perf_subproc_start_date + date, counter=counter, value=value)

            if nb_measurements >= len(self.counters) * max_measurements_per_counter:
                break

    def controlled_run(self) -> bool:
        """The loop body of the thread.

        It stacks measurements for about 1s at a time, before returning to evaluate the exit condition of the controlled
        loop (see parent class :py:class:`ControllableThread`).

        :returns: `True`, to always continue executing.
        """
        # Yield after one second worth of measurements
        self.output_queue.extend(list(self.parse_stat(max_measurements_per_counter=ceil(1000 / self.resolution))))

        return True

    def terminate(self):
        self.logger.debug('terminating perf monitoring thread for container %s', self.container_id)

        super().terminate()

        if self.perf_process is not None:
            self.perf_process.terminate()
            self.perf_process.communicate()

    def pause(self):
        if self.perf_process is not None:
            self.perf_process.send_signal(signal.SIGSTOP)

        super().pause()

    def resume(self):
        if self.perf_process is not None:
            self.perf_process.send_signal(signal.SIGCONT)

        super().resume()


class MemoryThread(TimerChain):
    """Memory thread - A monitor thread to collect memory usage using the cgroup interface.

    The monitoring is done by reading usage stats from the container's cgroup ("control groups", a feature of the Linux
    kernel).
    A measurement is the memory usage at a given time (in bytes).
    It is the sum of the resident set size (RSS) and of the total mapped file size.

    It is a daisy chain of timer threads, see the parent class :py:class:`TimerChain`.
    The chain is infinite, always scheduling the next timer thread after a delay determined by the given resolution.
    """

    def __init__(self, container_id: str, k8s_id: str, resolution: float, output_queue: MutableSequence[Measurement]):
        """Initialize a new memory monitor thread.

        :param container_id: the ID of the container to monitor
        :param k8s_id: the ID of the Kubernetes pod parent of the container to monitor
        :param resolution: the resolution of the measurements, i.e., the period between two measurements (in seconds)
        :param output_queue: the queue where to push measurements
        """
        self.container_id = container_id
        self.k8s_id = k8s_id
        self.resolution = resolution
        self.output_queue = output_queue

        super().__init__(iter(lambda: self.resolution, -1))

        self.missed = 0
        self.total_measurements = 0
        # Used to suppress missed measurement check after resuming
        self.just_resumed = True
        self.last_measurement = None

        self.failing = False

        # Take reference to the cgroup, only for monitoring, no control (it is Docker's)
        self.cgroup = cgroup_from_path(
            f'/sys/fs/cgroup/kubepods.slice/kubepods-burstable.slice/kubepods-burstable-pod{self.k8s_id}.slice/'
            f'docker-{self.container_id}.scope')

        self.produced_measurements = 1

        self.logger = logging.getLogger('monitoringthread.memory')

    def run(self, *_) -> None:
        """Body of the thread.

        See the parent method :py:func:`TimerChain.run()`.

        This implementation always schedules the next timer thread (it is always monitoring).
        """
        now = round(time.time(), 3)

        self.schedule()

        # check that we did not miss measurements
        if self.just_resumed:
            self.just_resumed = False
        elif self.last_measurement is not None:
            previous = self.last_measurement.date
            skipped = now - previous
            if skipped > 2 * self.resolution:
                missed = skipped // self.resolution
                self.missed += missed
                self.total_measurements += missed

                self.logger.warning('missed %d measurements between %f and %f (total missed: %d/%d, %.1f%%)', missed,
                                    previous, now, self.missed, self.total_measurements,
                                    100 * self.missed / self.total_measurements)

        try:
            self.output_queue.append(Measurement(now, 'memory', self.cgroup.memory.current))

            self.total_measurements += 1

            self.failing = False
        except cg.error.ControllerReadError as err:
            if not self.failing:
                if self.logger.isEnabledFor(logging.DEBUG):
                    self.logger.error('failed reading memory usage of container %s', self.container_id, exc_info=True)
                else:
                    self.logger.error('failed reading memory usage of container %s', self.container_id, err)
                self.failing = True

    def start(self):
        """Start the thread, i.e., schedule the first thread in the chain.

        This is to implement a unified thread-like interface.
        """
        self.schedule()

    def pause(self) -> None:
        """Pause the thread, i.e., stop scheduling threads in the chain.

        This is to implement a unified thread-like interface.

        Technically, does the same thing as :py:func:`terminate()` due to its nature of a timer chain.
        """
        self.terminate()

    def resume(self) -> None:
        """Resume the paused thread, i.e., schedule again a thread in the chain.

        This is to implement a unified thread-like interface.
        """
        self.schedule()

    def terminate(self):
        """Terminate the thread, i.e., stop scheduling threads in the chain.

        This is to implement a unified thread-like interface.
        """
        self.logger.debug('terminating memory monitoring thread for container %s', self.container_id)

        super().terminate()
