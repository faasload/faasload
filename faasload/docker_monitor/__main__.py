"""Entrypoint to run the Docker monitor.

The function :py:func:`main()` handles:

1. reading the configuration

   * see the module :py:mod:`docker_monitor` for the definition of the configuration
   * the configuration is read from *~/.config/faasload/monitor.yml*

2. start the measurements server and the Docker operations log server
3. start and block on the Docker operations handler
"""

# Copyright Toulouse INP / IRIT / CNRS, 2019 - 2020
# Copyright Télécom SudParis / IP Paris, 2021 - 2025
# Mathieu Bacou and other contributors
#
# mathieu.bacou@telecom-sudparis.eu
#
# This software is a computer program whose purpose is to inject a workload
# to a Function-as-a-Service platform, and to gather data about its execution.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software. You can use,
# modify and/ or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.

import logging
import logging.config
import os
import signal
import sys
from threading import Thread

from configuration_reader import read as read_configuration

from faasload.lib import configure_logging, log_faasload_preamble, prepare_and_read_configuration
from . import __version__ as my_version, DEFAULTS, DockerMonitorConfiguration
from .docker_operations import DockerOperationsHandler, DockerOperationsLogServer, DockerOperationsLogQueryHandler
from .measurements_server import MeasurementsQueryHandler, MeasurementsServer


def main():
    """Main function of the monitor."""
    configure_logging(os.path.expanduser('~/.config/faasload/monitor.yml'))
    logger = logging.getLogger('main')

    conf: DockerMonitorConfiguration = read_configuration(
        os.path.expanduser('~/.config/faasload/monitor.yml'),
        prepare_and_read_configuration,
        default_mask=DEFAULTS)

    log_faasload_preamble(logger)
    logger.info('FaaSLoad monitor %s', my_version)

    logger.debug('Whisk Docker Monitor global configuration: %s', conf)

    # sys.exit raises SystemExit, so `finally` clauses will be executed
    # Python already stores its default SIGINT handler (but not the SIGTERM one, probably because it does not replace it
    # and uses the system's default)
    signal.signal(signal.SIGINT, lambda *_: sys.exit(0))
    old_sigterm_handler = signal.signal(signal.SIGTERM, lambda *_: sys.exit(0))

    try:
        dockerops_handler = DockerOperationsHandler(conf['monitor'], conf['dockeroperations'])

        with MeasurementsServer(dockerops_handler, conf['measurementserver'], MeasurementsQueryHandler) as meas_srv:
            meas_srv_thread = Thread(target=meas_srv.serve_forever)
            # as a daemon, the measurements server thread is destroyed when the main thread terminates
            meas_srv_thread.daemon = True
            meas_srv_thread.start()

            with DockerOperationsLogServer(dockerops_handler, conf['dockeropslogserver'],
                                           DockerOperationsLogQueryHandler) as log_srv:
                log_srv_thread = Thread(target=log_srv.serve_forever)
                log_srv_thread.daemon = True
                log_srv_thread.start()

                # blocks on processing Docker operation events
                dockerops_handler.handle_docker_operations()
    except SystemExit:
        # Was caught in the DockerOperationsHandler to terminate its monitor workers, and reraised by it for us here.
        logger.warning('interrupted')

        log_srv.shutdown()
        log_srv_thread.join()

        meas_srv.shutdown()
        meas_srv_thread.join()
    finally:
        signal.signal(signal.SIGINT, signal.default_int_handler)
        signal.signal(signal.SIGTERM, old_sigterm_handler)


if __name__ == '__main__':
    main()
