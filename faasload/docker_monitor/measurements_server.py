"""Measurements server - Serve resource usage and performance counters of Docker containers that run FaaS functions.

The class :py:class:`MeasurementsServer` is a `TCP server
<https://docs.python.org/3/library/socketserver.html#socketserver.TCPServer>`_ that responds to queries about resource
usage and performance counters of a Docker container that ran an invocation for a requested timeslice.

Actually, the important code is in the query handler, the class :py:class:`MeasurementsQueryHandler`.
"""

# Copyright Toulouse INP / IRIT / CNRS, 2019 - 2020
# Copyright Télécom SudParis / IP Paris, 2021 - 2025
# Mathieu Bacou and other contributors
#
# mathieu.bacou@telecom-sudparis.eu
#
# This software is a computer program whose purpose is to inject a workload
# to a Function-as-a-Service platform, and to gather data about its execution.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software. You can use,
# modify and/ or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.

from __future__ import annotations

import json
import logging
from collections.abc import Sequence
from datetime import datetime
from json import JSONDecodeError
from socketserver import StreamRequestHandler, TCPServer
from typing import Any, Dict

from . import MonitorServerConfiguration
from .docker_operations import DockerOperationsHandler
from .monitor import Measurement


class MeasurementsServer(TCPServer):
    """Server for queries about measurements of resource usage and performance counters of Docker containers.

    The main processing is done in the request handler :py:class:`MeasurementsQueryHandler`.
    The server is only customized to store a reference to the Docker operations handler, to fetch measurements from the
    monitor threads.
    """

    def __init__(self,
                 docker_operations_handler: DockerOperationsHandler,
                 cfg: MonitorServerConfiguration,
                 *args, **kwargs):
        self.docker_operations_handler = docker_operations_handler

        super().__init__((cfg['address'], cfg['port']), *args, **kwargs)


class MeasurementsQueryHandler(StreamRequestHandler):
    """Request handler for the measurements server.

    Handle measurements queries made by a client program to fetch resource usage and performance counters of Docker
    containers used by a FaaS platform to run functions.

    Query format
    ------------

    Queries of measurements are expected as small JSON objects:

        {"container": CONTAINER_ID,
         "start": INVOCATION_START,
         "end": INVOCATION_END}

    where CONTAINER_ID is the container ID, and INVOCATION_START and INVOCATION_END are the start and end timestamps (in
    seconds, precision up to the millisecond) of the invocation.
    In other words, the server will return all the measurements about the specified container between those two dates.

    Response format
    ---------------

    The response is a JSON object like the following:

        {
            "error": ERROR_MSG,
            "measurements": MEASUREMENTS
        }

    ERROR_MSG may be an error message describing the error if the server cannot fulfil the query.
    The key is always present, but may be `None`, indicating that no error occurred.

    Then MEASUREMENTS is a JSON list of objects, each object being one measurement record (instances of the class
    `:py:class:Measurement`, converted as dicts) like the following:

        {
            "date": TIMESTAMP,
            "counter": COUNTER,
            "value": VALUE
        }

    where TIMESTAMP is the absolute date of the measurement (in seconds, rounded to the millisecond), COUNTER is the name of
    the counter, and VALUE its value at this date.

    Memory is always monitored, so as a special case, there is always a record with the counter name "memory".

    Note that for any counter, "value" may be null (Python `None`), indicating that the measurement failed at this date.
    A common occurrence is that the container is destroyed before the Docker operations handler terminates the monitor
    thread, thus producing such failed measurements.
    In the case of performance counters, the value may be null, if the measurement failed at this date, or the perf command
    returned no value at this date.
    """

    def __init__(self, request, client_address, server: MeasurementsServer):
        self.meas_server = server
        self.logger = logging.getLogger('measurementserver')

        super().__init__(request=request, client_address=client_address, server=server)

    def handle(self):
        try:
            query = json.load(self.rfile)
        except JSONDecodeError as err:
            self.logger.error('malformed JSON format for notification: %s', err)
            return

        result = {
            'error':        None,
            'measurements': None
        }

        # noinspection PyBroadException
        try:
            result['measurements'] = [m._asdict() for m in self._handle_query(query)]
        except KeyError:
            err_msg = f'malformed query:\n{query}'

            self.logger.error(err_msg)
            result['error'] = err_msg
        except ValueError as err:
            self.logger.error(err)
            result['error'] = str(err)
        except Exception:
            self.logger.exception('unexpected error while handling measurements query')
            result['error'] = 'unexpected error'
        finally:
            self.wfile.write(json.dumps(result).encode())

    def _handle_query(self, query: Dict[str, Any]) -> Sequence[Measurement]:
        """Handle the measurements query by fetching measurements in the given timeslice for the queried container.

        :param query: the query for measurements

        :returns: the list of measurements

        :raises ValueError: the queried container is unknown
        """
        start = query['start']
        end = query['end']
        cid = query['container']

        self.logger.info('requested for container %s between %s and %s', cid,
                         datetime.fromtimestamp(start).isoformat(),
                         datetime.fromtimestamp(end).isoformat())

        try:
            return self.meas_server.docker_operations_handler.monitor_workers[cid].get_measurements(start, end)
        except KeyError:
            raise ValueError(f'unknown container {cid}') from None
