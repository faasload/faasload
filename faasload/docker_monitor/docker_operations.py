"""Docker operations - Monitor Docker operations to manage monitor threads.

The class :py:class:`DockerOperationsHandler` is in a way the main body of the Docker monitor: it reads Docker
operations sent by the FaaS platform on the Docker socket, and manages its monitor threads accordingly.
For example, it creates a new monitor thread when a container is created.

It keeps references to the monitor threads, so the :py:class:`MeasurementsServer` can request measurements from them.

It also keeps a log of all Docker operations it sees.
The class :py:class:`DockerOperationsLogServer` is then a server for this log.
"""
import json
import logging
import re
import time
from collections import namedtuple, defaultdict
from datetime import datetime
from enum import Enum
from json import JSONDecodeError
from socketserver import TCPServer, StreamRequestHandler
from threading import Timer
from typing import Sequence, Dict, Any

import docker

from . import DockerOperationsMonitorConfiguration, MonitorConfiguration, DockerOpsLogServerConfiguration
from .monitor import DockerMonitor

# Copyright Toulouse INP / IRIT / CNRS, 2019 - 2020
# Copyright Télécom SudParis / IP Paris, 2021 - 2025
# Mathieu Bacou and other contributors
#
# mathieu.bacou@telecom-sudparis.eu
#
# This software is a computer program whose purpose is to inject a workload
# to a Function-as-a-Service platform, and to gather data about its execution.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software. You can use,
# modify and/ or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.

# Docker operations the handler is interested in.
DOCKER_OPERATIONS_FILTERS = {
    'event': [
        'start',
        'kill',
        'pause',
        'unpause',
    ],
}


class DockerOperation(Enum):
    """Mapping of Docker operations handled by the monitor's Docker operations handler."""
    RUN = 'start'
    RM = 'kill'
    PAUSE = 'pause'
    RESUME = 'unpause'


DockerOperationLogEntry = namedtuple('DockerOperationLogEntry', ['date', 'docker_name', 'operation'])


class DockerOperationsHandler:
    """Handler for Docker operations coming from the FaaS platform.

    Its main method is :py:func:`handle_docker_operations`: it blocks reading Docker events from the Docker socket, and
    manages monitor threads.

    When it starts, it scans currently running containers to find the ones it has to monitor but were created before.

    The handler only acts on Docker events listed in :py:data:`DOCKER_OPERATIONS_FILTERS`.
    In addition, it only monitors Docker containers which names match any regex from the configuration.

    When a Docker container is deleted, its monitor thread is not destroyed immediately, but is scheduled for deletion:
    this is to allow for a client to the Docker monitor to query measurements.
    """

    def __init__(self, monitor_cfg: MonitorConfiguration, dockeroperations_cfg: DockerOperationsMonitorConfiguration):
        """Initialize a new Docker operations handler.

        :param monitor_cfg: the configuration of the monitor (passed to the monitor threads)
        :param dockeroperations_cfg: the configuration of the Docker operations handler
        """
        self.monitor_cfg = monitor_cfg
        self.dockeroperations_cfg = dockeroperations_cfg

        self.name_filters = [re.compile(nf_pat) for nf_pat in dockeroperations_cfg['namefilters']]

        self.monitor_workers = {}
        # When a container is killed by Docker, its monitoring thread is terminated but lives on to hold its
        # measurements for serving.
        # The DockerOperationsHandler starts a timer to finally join the thread after some time to clean up.
        # The timer is kept around, so it can be canceled when the DockerOperationsHandler is terminated.
        self.deletion_timers = {}

        self.docker_operations_timeline = []
        self.docker_operations_per_container = defaultdict(dict)

        self.logger = logging.getLogger('dockeroperations')

    def handle_docker_operations(self):
        """Handle Docker operation events indefinitely.

        Indefinitely handle operations by the FaaS platform, caught from Docker's socket, about its Docker container
        operations (run, rm, pause, resume).

        Monitor threads are managed by this handler:

        * container creation: spawn monitor thread;
        * container pause: pause monitor thread;
        * container resuming: resume monitor thread;
        * container destruction: terminate monitor thread.

        When starting, current Docker containers are scanned as if a run event was received for them, to spawn monitor
        threads for them.
        Then, it waits for Docker events.
        """
        if self.dockeroperations_cfg['cert_path'] is not None:
            import os
            client_cert = os.path.join(self.dockeroperations_cfg['cert_path'], 'cert.pem')
            client_key = os.path.join(self.dockeroperations_cfg['cert_path'], 'key.pem')
            ca_cert = os.path.join(self.dockeroperations_cfg['cert_path'], 'ca.pem')
            tls = docker.tls.TLSConfig(
                ca_cert=ca_cert,
                client_cert=(client_cert, client_key),
                verify=True,
                assert_hostname=False
            )
        else:
            tls = False

        docker_client = docker.APIClient(base_url=self.dockeroperations_cfg['base_url'], tls=tls)

        self.logger.debug('found %d containers already running, checking for monitorable containers',
                          len(docker_client.containers()))
        # Scan currently running containers for containers that should be monitored.
        # It can happen that we are run after a container was created: we obviously missed the creation event.
        for container in docker_client.containers():
            if any(any(nf.match(cont_name) for cont_name in container['Names'])
                   for nf in self.name_filters):
                self.logger.debug('found monitorable container %s (%s) already running, starting a monitor',
                                  container['Names'][0], container['Id'])
                try:
                    self._handle_docker_operation(
                        DockerOperation.RUN,
                        container['Labels']['io.kubernetes.pod.name'],
                        container['Id'],
                        container['Labels']['io.kubernetes.pod.uid'].replace('-', '_'))
                except ValueError:
                    self.logger.exception('failed starting monitor for already running monitorable container')
        try:
            for event in docker_client.events(filters=DOCKER_OPERATIONS_FILTERS, decode=True):
                self.logger.debug('received Docker operation: %s', event)

                if any((nf.match(event['Actor']['Attributes']['name']) for nf in self.name_filters)):
                    try:
                        self._handle_docker_operation(
                            DockerOperation(event['Action']),
                            event['Actor']['Attributes']['io.kubernetes.pod.name'],
                            event['Actor']['ID'],
                            event['Actor']['Attributes']['io.kubernetes.pod.uid'].replace('-', '_'))
                    except ValueError:
                        self.logger.exception('failed handling Docker operation')
                else:
                    self.logger.debug('Docker operation ignored because of name filters')
        except SystemExit:
            for monitor_worker in self.monitor_workers.values():
                monitor_worker.terminate()
                monitor_worker.join()
            for deletion_timer in self.deletion_timers.values():
                deletion_timer.cancel()
            raise

    def _handle_docker_operation(self, operation: DockerOperation, docker_name: str, docker_id: str, k8s_id: str):
        """Handle the notification by managing monitor threads.

        :param operation: the Docker operation to handle
        :param docker_name: the name of the Docker container
        :param docker_id: the ID of the Docker container targeted by the operation
        :param k8s_id: the ID of the pod parent of the container

        :raises ValueError: the operation is invalid (most often, unknown container)

        Depending on the notified action, create, terminate, pause or resume the thread monitoring the specified
        container.
        Also, manage the list of monitor threads, scheduling them for deletion on a rm event.

        Update the log of Docker operations on this Kubernetes node.
        """
        if operation is DockerOperation.RUN:
            if docker_name in self.monitor_workers:
                raise ValueError(f'monitor for container {docker_name} already running')

            monitor_thread = DockerMonitor(docker_id, k8s_id, self.monitor_cfg)
            # When matching activation ID with container, the logs only give the Docker name, so we use that instead of
            # the Docker ID, which is only used by the monitor itself (its MemoryThread monitor in particular).
            self.monitor_workers[docker_name] = monitor_thread
            monitor_thread.start()

            self.logger.info('run monitor for container %s', docker_name)
        elif operation is DockerOperation.RM:
            # Terminate the monitor now.
            try:
                self.monitor_workers[docker_name].terminate()
            except KeyError:
                raise ValueError(f'cannot find container {docker_name} for deletion') from None
            # Schedule the deletion for some time in the future, tied to the maximum age of the measurements that we
            # keep, so that a client has time to fetch the measurements even if the container is deleted immediately by
            # the FaaS platform.
            deletion_timer = Timer(self.monitor_cfg['maxage'], self._delete_container, (docker_name,))
            self.deletion_timers[docker_name] = deletion_timer
            deletion_timer.start()

            self.logger.info('scheduled deletion of monitor for container %s in %.3fs', docker_name,
                             self.monitor_cfg['maxage'])
        elif operation is DockerOperation.PAUSE:
            try:
                self.monitor_workers[docker_name].pause()
            except KeyError:
                raise ValueError(f'cannot find monitor for container {docker_name} for pause') from None

            self.logger.info('paused container %s', docker_name)
        elif operation is DockerOperation.RESUME:
            try:
                self.monitor_workers[docker_name].resume()
            except KeyError:
                raise ValueError(f'cannot find monitor for container {docker_name} for resuming') from None

            self.logger.info('resumed monitor for container %s', docker_name)
        else:
            raise ValueError(f'unrecognized action "{operation}"')

        log_entry = DockerOperationLogEntry(date=round(time.time(), 3), docker_name=docker_name, operation=operation)
        self.docker_operations_timeline.append(log_entry)
        self.docker_operations_per_container[docker_name][operation] = round(time.time(), 3)

    def _delete_container(self, docker_name: str):
        """Delete the monitor thread of the container.

        This method is scheduled using a timer thread to clean up some time after the destruction of the monitor
        container.
        This is to allow for a client to query measurements.

        :param docker_name: the name of the Docker container

        :raises ValueError: the container is unknown
        """
        try:
            monitor_thread = self.monitor_workers.pop(docker_name)
            monitor_thread.join(timeout=2)
            del self.deletion_timers[docker_name]
            if monitor_thread.is_alive():
                self.logger.warning('monitor for container %s is now zombie', docker_name)
            else:
                self.logger.info('deleted monitor for container %s', docker_name)
        except KeyError:
            raise ValueError(f'cannot find container {docker_name} for deletion') from None

    def get_operations_log(self, start: int, end: int) -> Sequence[DockerOperationLogEntry]:
        """Query the log of Docker operations for the given timeslice.

        :param start: the beginning of the queried timeslice
        :param end: the end of the queried timeslice

        :returns: a list of Docker operations

        The return list includes all events between `start` and `end`.
        However, it also includes events that happened before `start`, about containers that have events in the queried
        timeslice.
        This is to help in counting the current number of containers at a given time.
        For example, if a container has any event between `start` and `end`, then all its creation, pause and resume
        events before `start` are also included.
        """
        ret = []

        # Include container creation, pause and resume operations if they happened before start and the container was
        # not deleted before start.
        for entry in [entry for entry in self.docker_operations_timeline if entry.date < start]:
            try:
                if self.docker_operations_per_container[entry.docker_name][DockerOperation.RM] <= start:
                    continue
                else:
                    ret.append(entry)
            except KeyError:
                # If there is no RM operation on this container, then it is still alive, so the operation must appear.
                ret.append(entry)

        ret.extend([entry for entry in self.docker_operations_timeline if start <= entry.date <= end])

        return ret


class DockerOperationsLogServer(TCPServer):
    """Server for queries about Docker operations.

    The main processing is done in the request handler :py:class:`DockerOperationsLogQueryHandler`.
    The server is only customized to store a reference to the Docker operations handler, to fetch log entries from it.
    """
    def __init__(self, docker_operations_handler: DockerOperationsHandler, cfg: DockerOpsLogServerConfiguration, *args,
                 **kwargs):
        self.docker_operations_handler = docker_operations_handler

        super().__init__((cfg['address'], cfg['port']), *args, **kwargs)


class DockerOperationsLogQueryHandler(StreamRequestHandler):
    """Request handler for the Docker operations log server.

    Handle log queries made by a client program to fetch the log of operations done by the FaaS platform on its Docker
    containers.

    Query format
    ------------

    Queries of measurements are expected as small JSON objects:

        {
            "start": START,
            "end": END
        }

    where START and END are the start and end timestamps (in seconds, precision up to the millisecond) of the queried
    timeslice.

    Response format
    ---------------

    The response is a JSON object like the following:

        {
            "error": ERROR_MSG,
            "log": LOG,
        }

    ERROR_MSG may be an error message describing the error if the server cannot fulfil the query.
    The key is always present, but may be `None`, indicating that no error occurred.

    Then LOG is a JSON list of objects, each object being one record of a Docker operation (instances of the class
    `:py:class:DockerOperationLogEntry`, converted as dicts) like the following:

        {
            "date": TIMESTAMP,
            "docker_name": DOCKER_NAME,
            "operation": OPERATION
        }

    where TIMESTAMP is the absolute date of the measurement (in seconds, rounded to the millisecond), DOCKER_NAME is the
    name of the Docker container of the event, and OPERATION is the operation of the event (the value of the
    corresponding member of the enum :py:class:`DockerOperation`).

    The server may return operations that happened before START, see the method
    :py:func:`DockerOperationsHandler.get_operations_log()` for more information.

    """
    def __init__(self, request, client_address, server: DockerOperationsLogServer):
        self.dockeropslog_server = server
        self.logger = logging.getLogger('dockeropslogserver')

        super().__init__(request=request, client_address=client_address, server=server)

    def handle(self):
        def to_serializable(obj: Any) -> str:
            if isinstance(obj, Enum):
                return obj.name
            else:
                raise TypeError(f'Object of type {type(obj)} is not JSON serializable')

        try:
            query = json.load(self.rfile)
        except JSONDecodeError as err:
            self.logger.error('malformed JSON format for request of Docker operations log: %s', err)
            return

        result = {
            'error': None,
            'log':   None
        }

        # noinspection PyBroadException
        try:
            result['log'] = [m._asdict() for m in self._handle_query(query)]
        except KeyError:
            err_msg = f'malformed query:\n{query}'

            self.logger.error(err_msg)
            result['error'] = err_msg
        except ValueError as err:
            self.logger.error(err)
            result['error'] = str(err)
        except Exception:
            self.logger.exception('unexpected error while handling Docker operations log query')
            result['error'] = 'unexpected error'
        finally:
            self.wfile.write(json.dumps(result, default=to_serializable).encode())

    def _handle_query(self, query: Dict[str, Any]) -> Sequence[DockerOperationLogEntry]:
        start = query['start']
        end = query['end']

        self.logger.info('requested Docker operations between %s and %s',
                         datetime.fromtimestamp(start).isoformat(),
                         datetime.fromtimestamp(end).isoformat())

        return self.dockeropslog_server.docker_operations_handler.get_operations_log(start, end)
