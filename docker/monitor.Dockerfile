# FaaSLoad: Docker monitor
#
# FaaSLoad is made of two running components: the loader and the monitor.
# The monitor is deployed as a pod on every Kubernetes node of the FaaS platform.
#
# This Dockerfile uses multi-stage build, to build:
#
# * a capability wrapper for perf;
# * the perf binary;
# * the image of FaaSLoad's monitor, picking up from the two previous stages.
#
# Arguments kernel_release and docker_group_id are required; build from the repository's root with:
#
# docker build . --file docker/monitor.Dockerfile --tag faasload/monitor:local\
#     --build-arg kernel_release="$(uname -r | cut -d'-' -f1)"\
#     --build-arg docker_group_id="$(getent group docker | cut -d":" -f3)"

# (the name after --tag may be customized)
#
# * kernel_release: the Linux kernel release where the Docker monitor runs, i.e., the Linux kernel release of the
#   Kubernetes nodes.
# * docker_group_id: the Docker monitor reads Docker operations from Docker's socket, so it must have access rights to
#   it, which are given by running as this given group ID, i.e., the ID of the group docker on the Kubernetes nodes.


# Capability wrapper for perf
#############################
#
# perf needs the CAP_SYS_ADMIN capability to be permitted when invoked.
# Compile a wrapper to take care of this; see perf_cap_wrapper.c for details.

FROM debian:bullseye AS perf_cap_wrapper

RUN apt-get update && apt-get install -y \
    gcc libcap-dev \
 && rm -rf /var/lib/apt/lists/*

COPY docker/perf_cap_wrapper.c /

RUN gcc -static perf_cap_wrapper.c -lcap -o perf_cap_wrapper

#############################

# perf
######
#
# perf is tightly coupled to the kernel version. It must then be recompiled for
# the host's kernel version.
# This stage will build perf as a static binary at path "/perf".

FROM debian:bullseye AS perf

# perf has optional build dependencies to enable additional features.
# In the context of FaaSLoad, only the following ones look useful.
ARG PERF_OPT_DEPS="systemtap-sdt-dev libcap-dev"

RUN apt-get update && apt-get install -y \
	wget xz-utils \
	gcc make bison flex elfutils python3 \
	$PERF_OPT_DEPS \
	libelf-dev libdw-dev libaudit-dev \
 && rm -rf /var/lib/apt/lists/*

# perf is tied to the kernel release of the host (without distribution-specific
# version number), which must be specified at build time as `kernel_release`.
# It can be obtained with `uname -r | cut -d'-' -f1`.
ARG kernel_release

RUN : "${kernel_release:?Build argument \"kernel_release\" must be set and non empty.}"

# Building the URL does not work with kernel versions which revision number is 0: remove the revision number.
RUN wget -O linux-source.tar.xz https://cdn.kernel.org/pub/linux/kernel/v"$(echo $kernel_release | cut -d'.' -f1)".x/linux-"$kernel_release".tar.xz --progress=dot:mega \
 && tar -xJf linux-source.tar.xz && rm linux-source.tar.xz

RUN cd linux-"$kernel_release"/tools/perf \
 && make clean && make LDFLAGS=-static NO_JEVENTS=1 NO_LIBTRACEEVENT=1 \
 && cp perf / \
 && cd / && rm -rf linux-"$kernel_release"

######

# Monitor
#########
#
# This step builds the actual image, picking up the perf binary and its capability wrapper from the previous steps.

FROM python:3.10.4-slim-bullseye

RUN apt-get update && apt-get install -y \
    git \
    gcc libpq-dev \
    libcap2-bin \
&& rm -rf /var/lib/apt/lists/*

# Add the capability wrapper for perf in a folder in the PATH before the actual perf binary.
COPY --from=perf_cap_wrapper /perf_cap_wrapper /usr/local/bin/perf
# Add the perf binary.
COPY --from=perf /perf /usr/bin/

# Set up the capabilities to use perf, via a wrapper.
# Set the capabilities on the actual perf binary!
# The "perf" on the path is only a Bash script.
# And also set the capabilities so the wrapper can activate the capability when invoked
RUN setcap cap_sys_admin+ei /usr/bin/perf \
 && setcap cap_sys_admin+eip /usr/local/bin/perf

# The ID of the group "docker" on the host, to put the normal user of the container in this group so it can read
# Docker's socket mounted in the running container.
# It can be obtained with `getent group docker | cut -d":" -f3`.
ARG docker_group_id

RUN : "${docker_group_id:?Build argument \"docker_group_id\" must be set and non empty.}"

# Create a group docker inside the container, with the ID of the host's group docker.
# Create a normal user faasload, and put it in the group docker.
RUN groupadd -g $docker_group_id docker \
 && useradd --create-home faasload \
 && usermod -aG docker faasload

# Install the monitor as the normal user.
USER faasload

WORKDIR /home/faasload

ENV PATH="/home/faasload/.local/bin:$PATH"

RUN PIP_DISABLE_PIP_VERSION_CHECK=1 pip install --upgrade pip pipenv

# Copy the metadata files to install with pipenv.
COPY --chown=faasload:faasload Pipfile.lock .

RUN pipenv sync

# Copy only the code.
# The COPY instruction is retarded: giving a directory as source will copy its content and not the directory itself.
COPY --chown=faasload:faasload faasload faasload

ENTRYPOINT ["pipenv", "run", "python3", "-u"]
CMD ["-m", "faasload.docker_monitor"]

#########
