/* Capabilities wrapper for perf
 *
 * perf needs the CAP_SYS_ADMIN (in the future, CAP_PERFMON) capability to be permitted when invoked.
 * This wrapper takes care of this.
 * Compile and link against libcap, for instance:
 *     gcc perf_cap_wrapper.c -lcap -o perf_cap_wrapper
 * To use, set cap_sys_admin+ei on the real perf binary, and set cap_sys_admin+eip on this wrapper.
 * For instance:
 *     sudo setcap cap_sys_admin+ei /usr/lib/linux-tools-$(uname -r)/perf
 *     sudo setcap cap_sys_admin+eip perf_cap_wrapper
 */

#include <stdio.h>
#include <unistd.h>

#include <sys/capability.h>

#define CAP_TEXT "cap_sys_admin+ip"
#define PERF_EXE "/usr/bin/perf"

int main(int argc, char* argv[], char* envp[]) {
    cap_t old_cap = cap_get_proc();

    cap_t added_cap = cap_from_text(CAP_TEXT);
    if(cap_set_proc(added_cap) < 0) {
        fprintf(stderr, "cannot set capability %s\n", CAP_TEXT);
        return -1;
    }

    execve(PERF_EXE, argv, envp);

    if(cap_set_proc(old_cap) < 0) {
        fprintf(stderr, "cannot restore capability\n");
        return -1;
    }

    return 0;
}
