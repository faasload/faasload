# Resetting FaaSLoad

Between independent runs of FaaSLoad, you need to reset its database and the checkpointed state of the dataset generator mode.
You may also want to reset the FaaS platform.

## Database

As part of _load-wsk-assets.py_ operations, action data is loaded to FaaSLoad's database by **creating or replacing** the table `functions`.
If FaaSLoad was run previously, it created its other tables that reference the table `functions` via foreign key constraints.
In consequence, replacing the table fails with the errors you are seeing.

The solution is to manually `DROP` the tables `results`, `parameters`, `resources`, `dockeroperations` and `runs` **in this order** (to satisfy other foreign key constraints between `runs` and the other two tables).
Of course this means **losing all run data from executing FaaSLoad**, so extract and store them safely beforehand!

Note that in workload injector mode, FaaSLoad wipes the tables by itself.

## Generator checkpointed state

In generator mode, FaaSLoad scans the directory specified as `generator:statedir` in _loader.yml_, and loads the file named with the most recent date to restore the generator state (see ["Generation state checkpointing"](./generator/checkpointing.md)).
If it encounters any inconsistency between it and its database, FaaSLoad fails at start up to avoid corrupting or losing data in the database, suggesting that you loaded a wrong generator state.

Depending on your situation, you may want to delete the more recent state files so that the one you actually want to restore state from is the most recent;
or if you actually want to generate the dataset from scratch, remove (or move away) all state files in the state directory.

## Complete reset

If you do not intend on running the same experiment, you should also reset the table `functions` of the database, by `DROP`ping it.

As part of _load-wsk-assets.py_ operations, functions and users are created in OpenWhisk.
This has no consequence for further runs, but you might want to clean that up (with the `--unload` flag of _load-wsk-assets.py_).

Be mindful of the behavior of your FaaS platform: most have a mechanism to keep warm runtimes for a given amount of time, in an effort to reduce invocation latency by avoiding the initialization of a runtime.
Depending on your experiment, you may want to wait for the warm runtimes to wear off and be collected by the platform.