# Generate a dataset of cold starts

FaaS platforms generally manage runtime instances to execute functions.
They keep instances warm to improve the latency of function invocations by skipping the initialization phase of a runtime.
If the platform does not have a warm runtime available to serve an invocation, it must initialize a new one, which is a cold start.

The distinction of cold and warm starts in FaaS platforms is the most significant behavior when studying the performance of applications deployed in a FaaS environment.

FaaSLoad cannot decide if an invocation will be warm or cold, because this is an internal decision of the FaaS platform.
It can only report in its database if the invocation was warm or cold [1].

The most straightforward way of forcing only cold invocations is to modify the behavior of the platform, which probably involves a code change.
But there can be alternative tricks that only require a proper configuration.

[1] In FaaSLoad's database, a `NULL` value in the column `init_ms` of a run indicates a warm start, i.e., there was no initialization phase. See ["Output database structure"](../dataset.md) for more information.

## Apache OpenWhisk

To force OpenWhisk to only do cold invocations, you need at least two different functions per invoker.

Set the memory available to the invoker to run user functions to exactly the memory limit of the functions.
This way, the invoker first invokes function A that takes all the memory.
Then, FaaSLoad also invokes function B, but the invoker does not have enough free memory to serve the invocation, so it kills the runtime of function A after it terminates, and initialize the runtime for function B.
FaaSLoad will then invoke function A again, which will also be a cold start, and so on.

The setting is `whisk:containerPool:userMemory`, it can be set in the file _mycluster.yaml_ that is read by the deployment scripts from [FaaSLoad's repository of platform configurations](https://gitlab.com/faasload/platform-configurations).

## Knative

_unknown_