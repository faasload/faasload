# Dataset generator mode

In dataset generator mode, **FaaSLoad activates actions for every input** listed in its configuration, to produce a dataset of action activations.
This mode is similar to the workload injector mode: the difference is that FaaSLoad builds fake workload traces that make it run every action on every input (that is valid for this function, see the documentation about writing actions for your FaaS platform, e.g., ["FaaSLoad actions"](../platforms/openwhisk/functions.md)).
Then, FaaSLoad runs those fake traces to invoke actions in OpenWhisk, and monitors their execution time, and more.
It uses the loader component to invoke actions in OpenWhisk;
the monitor can also be enabled to store resource usage and performance counters of function invocations, and logs of Docker operations.
An addition in generator mode however, is the capability **to checkpoint the dataset generation process**, to restart or prolong it reproducibly; see ["Generation state checkpointing"](./checkpointing).

Sections below explain:

1. how to configure FaaSLoad
2. how to prepare the FaaS platform
3. and finally how to run the loader

["Output database structure"](../dataset.md) gives a description of the resulting data as stored in the database.
A part of the output data is also the generator state, that can be used to continue generating a dataset after it was stopped;
see "Execution restart" below.

If you have the specific need to generate a dataset of only cold start invocations, see ["Generate a dataset of cold starts"](./cold-starts-only.md).

See ["Troubleshooting"](../troubleshooting.md) if something fails.

To go further in understanding how FaaSLoad works, have a look at ["Architecture of FaaSLoad"](../dev/architecture.md).

## Configuration

You can find a complete configuration file that uses the default values at [_config/loader.yml_](../../config/loader.yml).
The loader is configured by the file at _~/.config/faasload/loader.yml_.

You are advised to use this complete file, so start by copying the default configuration file to this location:

```shell
mkdir -p ~/.config/faasload
cp config/loader.yml ~/.config/faasload/loader.yml`
```

Most configuration settings can be left to their default values.
This guide will tell you what settings are mandatory.

Alternatively, here is an example of a minimal configuration for FaaSLoad's loader targeting OpenWhisk, in generation mode with the monitor enabled (omitting the `logging` section):

```yaml
generator:
  enabled: true
  # Complete with needed input kinds.
  nbinputs:
    image: 10

database:
  host: localhost

# Specific to OpenWhisk
openwhisk:
  host: "localhost:31001"
  authkeys: "./users"
```

The default execution mode of FaaSLoad is the workload injector mode, so for generation mode, first you must enable the mode by setting `generation:enabled` to `true`.

It is also mandatory to set the number of inputs to use in generating the dataset, for each input kind.
So, for example:

```yaml
generation:
  enabled: true
  nbinputs:
    image: 10
    audio: 10
    video: 10
    none: 10
```

Here, FaaSLoad will activate each action that processes images 10 times, each action that do not process any input file 10 times, etc.
See ["Preparing input data for the generator mode"](input-data.md) to learn how to prepare the input files for FaaSLoad's generator mode.

The use of FaaSLoad's monitor is enabled by default in the configuration.
So if you followed the instructions to run FaaSLoad's monitor in FaaSLoad's README at ["FaaSLoad's monitor"](../../README.md), then FaaSLoad will request performance data, resource usage and Docker operations logs when running the dataset generation.
If you want to disable it (e.g., you are running on a kind cluster where the monitor is not available), set the configuration key `monitor:enabled` to `false`.

## Preparing the FaaS platform

The specifics depend on the platform.
The best integrated one for FaaSLoad is OpenWhisk.

In general, it involves writing functions for the platform that are compatible with FaaSLoad, and preparing user accounts by loading them in the platform.
See the documentation specific to your platform in the [platforms documentation](../platforms).
You also need to write a manifest file to declare the functions for FaaSLoad, see ["FaaSLoad manifest of functions"](../manifest.md).

### Preparing OpenWhisk

First, you must write **actions that are compatible with FaaSLoad**.
The actions should follow the specification given in ["FaaSLoad actions"](../platforms/openwhisk/functions.md).

Now use the script _load-wsk-assets.py_ in generator mode with the flag `--generator`/`-g` to prepare OpenWhisk to generate the dataset of your functions' executions.
Refer to ["Assets setup"](../platforms/openwhisk/setup-assets.md) to understand what the script does.

Example usage of _load-wsk-assets.py_ with [FaaSLoad's example actions](https://gitlab.com/faasload/actions):

```shell
# Mind the path to the directory of actions.
scripts/load-wsk-assets ../actions/manifest.yml --generator\
        --param-file ../actions/swift_parameters.json
```

The usage of the script _load-wsk-assets.py_ is as follows (also see `scripts/load-wsk-assets --help`):

* the first argument is always the path to the manifest file describing the actions used by the workload generator;
* `--generator` tells the script to prepare OpenWhisk for dataset generation;
* `--param-file ...` is optional and gives the path to a file specifying parameters to bind to the OpenWhisk package of the actions deployed by the script, this is similar in usage to OpenWhisk's tool wskdeploy.

You need to know that the script wrote authentication tokens of OpenWhisk users to the folder _users_ (configuration key `openwhisk:authkeys`).

In case _load-wsk-assets.py_ fails, you will need to revert what has been successfully executed.
The solution is to revert all operations made by the previous run of the script.
This is achieved by **running again with the same arguments and the additional `--unload`/`-U` flag** to unload all assets.

Example usage of _load-wsk-assets.py_ to unload all assets:

```shell
# Mind the path to the directory of actions.
# Notice how the command is exactly the same as when loading the assets, except for the --unload flag.
scripts/load-wsk-assets ../actions/manifest.yml --unload --generator\
        --param-file ../actions/swift_parameters.json
```

The script may display errors because it tries to unload everything even if it previously failed before the end, which is normal behavior.

You can now fix the initial problem and run _load-wsk-assets.py_ again.

## Execution

Run FaaSLoad's loader Python module:

```shell
# Assuming the pipenv virtualenv is activated.
python -m faasload.loader
```

Recall that the configuration for the loader is read from _~/.config/faasload/loader.yml_.

FaaSLoad will invoke functions in the FaaS platform with the given inputs and parameter values chosen at random (see ["Preparing input data for the generator mode"](../generator/input-data.md)), and store invocation data to its database (e.g., activation ID, parameters…).
When an invocation is done, FaaSLoad will request full data (e.g., invocation duration, byte size of the output…) and complete the invocation data.
If the monitor is enabled, it will also store resource usage and performance counters, as well as logs of Docker operations.

In case of failure, or user interruption with Ctrl-C, the generator writes its checkpointed state to the directory _states_.

### Execution monitoring

Logging is based on the `logging` module of the Python standard library.
The configuration is given by the section `logging` in _loader.yml_;
refer to [the logging module documentation](https://docs.python.org/3/library/logging.html#module-logging) for more information.
By default, it is set to log to stderr.

For both the loader and the monitor, set `logging:root:level` to `DEBUG` to increase logging verbosity, or to `WARNING` to decrease it, from the default level of `INFO`.

### Data extraction and usage

FaaSLoad stores all the data in its PostgreSQL database server.
The data extraction process, the dataset format, and tips on how to exploit the data, are found in ["Output database structure"](../dataset.md).

### Execution restart

To restart the generation at the point where it was interrupted, simply run the loader again with the same command line.
FaaSLoad will find and use the latest checkpoint file in the directory _states_; see ["Generation state checkpointing"](./checkpointing).
Dataset generation will restart at the same point, with the same random state, to guarantee reproducibility of the dataset.

If this behavior is unwanted, see ["Resetting FaaSLoad"](../resetting-faasload.md).
