# Preparing input data for the generator mode

_As of now, FaaSLoad is made to work with a [Swift](https://docs.openstack.org/swift/latest/) remote storage endpoint.
From there comes the identification of input files as couples of input container (a directory) and object filename._

Functions invoked by FaaSLoad can expect custom parameters that FaaSLoad's generator must provide.
It generates them randomly for each invocation, based on their definitions from the [manifest file](../manifest.md).

Functions are also provided with common parameters that are described in the [platform specific documentation](../platforms) on writing functions.
For example, see for OpenWhisk ["FaaSLoad functions"](../platforms/openwhisk/functions.md).

In particular, in generator mode, FaaSLoad builds input data filenames of the form `INPUT_ID.INPUT_EXT`.
For example, an image processing function will receive as the parameter `object` the filename _1.jpg_ for the first invocation, then on the next invocation the filename _2.jpg_, etc.
The object container's name, provided by the parameter `incont` to the action, is exactly the action's input kind, typically `image`, `audio`, etc.

_You can customize the extension associated with an input kind by modifying the dict `INPUT_EXTENSIONS` in the Python code, in the module `faasload.loader.generator`._

To prepare the input data, load to the object store, under the object container named after the input kind (e.g., _image_ for images), as many input objects as specified for the input kind in the configuration file _loader.yml_ under `generation:nbinputs`.
See [an example script](https://gitlab.com/faasload/actions/-/blob/master/load_sample_inputs_swift.sh) in FaaSLoad's repository of sample OpenWhisk functions.