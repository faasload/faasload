# Generation state checkpointing

The method `faasload.injection.inject()` (the main method for workload injection) catches `SIGINT` and `SIGTERM` when
waiting for termination.
This allows **to save the checkpointed state** upon interruption by the user.
The method also catches any exception while waiting for the termination of all injector threads, for the same reason.
Note however that injector threads handle their own exceptions themselves, so they do not disrupt other threads.

This checkpointed state is used to restore the state of the dataset generator.
In dataset generation mode and before running the injection, the `main` method of the loader tries **to load a
checkpointed state**.
It is the file matching the pattern _*.genstate_ in the folder given in the configuration file at `generation:statedir`, and with
the youngest date in ISO format in its filename.

When a checkpointed state has been successfully loaded, the injection traces built for dataset generation are rebuilt **by
restoring the RNG state from the checkpointed state**. Additionally, corrupted data, i.e., runs that were pending when the
loader was interrupted and the state checkpointed, are cleaned from the database.
Notice that there is a security in place that will prevent deletion from the database of too many records when
recovering, to prevent data loss due to loading the wrong state.

You may be prevented from re-running a failed or interrupted dataset generation because of this mechanism.
The solution is to move away or remove the state file.
See ["Resetting FaaSLoad"](../resetting-faasload.md) for more information about running FaaSLoad again.