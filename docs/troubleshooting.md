# Troubleshooting

This document lists common troubles encountered with FaaSLoad, and their resolution.
Also see [platform-specific troubleshooting guides](./platforms).

## FaaSLoad installation problems

### Python-related problems

If FaaSLoad fails to start, check the pipenv environment where you should have installed everything (see [the project README](../README.md)).

For instance, you can try the following commands:

```shell
# Navigate to FaaSLoad's installation directory
cd faasload
# Spawn a new shell in pipenv's virtual environment
pipenv shell
# Check the installed dependencies to verify none is missing (note the command "pip", not "pipenv")
pip list
# Run Python interactively
python
```

Now you are in Python's REPL.
For example, you can try to import FaaSLoad' modules:

```python
from faasload import docker_monitor
from faasload import loader

print(loader)
print(docker_monitor)
```

## FaaSLoad setup problems

### _load-wsk-assets.py_ fails because of a foreign key error

**Solution:** `DROP` the tables `results`, `parameters`, `resources`, `dockeroperations` and `runs`.

See ["Resetting FaaSLoad: Database"](./resetting-faasload.md).

### _load-wsk-assets.py_ fails because assets already exist

**Solution:** run the same command with the flag `-U` to clean up, and then retry.

If you ran the script _load-wsk-assets.py_ to prepare OpenWhisk, and it failed for some reason, then you cannot run the script again because the assets it tries to create in OpenWhisk already exist;
the script is tuned to not override anything.

The solution is to revert all operations made by the previous run of the script, and then run it again to load the assets.
This is achieved by **running again with the same arguments and the additional `--unload`/`-U` flag** to unload all assets.

Example usage of _load-wsk-assets.py_ to unload all assets (in injector mode):

```shell
# Notice how the command is exactly the same as when loading the assets, except for the --unload flag
load-wsk-assets --unload /home/faasload/actions/manifest.yml\
        --param-file /home/faasload/actions/swift_parameters.json\
        --injector /home/faasload/faasload/injection-traces/samples
```

Now, you can run _load-wsk-assets_ normally (i.e., without `--unload`) again.

This feature works by computing the operations that the script would do to prepare OpenWhisk, and then reverting their predicted results (e.g., deleting users and actions).
Thus, it may output many errors such as "action not found", which is expected if the previous execution of _load-wsk-assets.py_ failed midway.
It also means that **it may unload more than the script actually did** (but no more than the script would have done).
For instance, if trying to create users A, B and C in this order, but user B already exists, the step fails after having created user A.
However, it planned to create users B and C afterwards, so when reverting, users B and C are also deleted after user A.

## FaasLoad runtime problems

### FaaSLoad in generator mode fails at start up, suggesting "wrong state loaded"

**Solution:** move away the obsolete state files in _states/_.

See ["Resetting FaaSLoad: Generator checkpointed state"](./resetting-faasload.md).

### FaasLoad hangs after loading the workload / generating the dataset

**Solution:** be patient, manually check the invocations with the FaaS platform.

FaaSLoad signals that all injectors "reached end of injection trace", but then seems to be stalled.
Actually, FaaSLoad's activation monitor thread is waiting for invocations to terminate, before requesting complete data about them from OpenWhisk.
It may be that invocations are still pending.

You should check their state manually with the FaaS platform.

### The loader fails fetching resource usage

FaaSLoad may fail fetching resource usage from its monitor.
The problems sources may be:

* a connection problem between FaaSLoad and its monitors
  * check that the configuration is correct
* a monitor failure
  * check the monitor's logs with `kubectl -n faasload logs monitor-XXX` (fill `XXX` with the monitor pod you want to check)
* a misconfiguration of the monitor
  * check the monitor's logs (see previous point) for anything outstanding, for example, ignored Docker operations due to name filtering
