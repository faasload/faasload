# Documentation

This is the documentation for FaaSLoad.

## For users

If not done yet, start with [the project's README](../README.md) to install and use FaaSLoad.
It will direct you to the documentation for the [generator mode](./generator) or for the [injector mode](./injector) when needed.

["FaaSLoad manifest of functions"](./monitor.md) explains how to write the manifest of your functions to run with FaaSLoad.

In addition, see ["Resetting FaaSLoad"](./resetting-faasload.md) on how to prepare for a new run with FaaSLoad;
you can use it as a checklist before running an experiment with FaaSLoad.

["Docker monitor"](./monitor.md) gives more details on FaaSLoad's Docker monitor (its deployment and usage is already described in [the project's README](../README.md)).

For help on common problems, see ["Troubleshooting"](./troubleshooting.md).
You can also find help on platform-specific problems in [the platform documentation](./platforms), as well as other platform-specific information such as how to prepare the functions to run.

## For developers

The developers' documentation lives in [the dev directory](./dev).
It guides you on how to test FaaSLoad, and gives a more precise architecture of the project.

It also explains how the current platforms are implemented, and how to implement new ones.