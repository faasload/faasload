# User specifications to build workload injection traces

This document explains how to write user specifications that are read by the script _trace-builder.py_ to build workload injection traces for FaaSLoad's injector mode.

_There is [a sample user specifications file](../../injection-user-specs/sample.yml).
Injection trace files built from this sample file (with a duration of 300s and the random seed `20191108`) can be found in _injection-traces/sample__. 

## Overview

User specifications are written in a file that describes all workload injection that will take place in a run of FaaSLoad.
They model virtual users of the FaaS platform, including a memory usage profile for each of them, the input files available to them to invoke functions, as well as the functions they invoke and how they invoke them.

## Prerequisites

User specifications reference functions through a manifest file that describes them.
To read more about this file, refer to ["FaaSLoad manifest of functions"](../manifest.md).

In particular, the memory size of a function should be set **exactly to the amount of memory that the function requires** in all cases.
That is to say, it shouldn't include a safety margin that is typically added "just in case" one invocation needs more memory than expected.
This will be the amount of memory for the `smart` memory usage profile (see below), and is used as a base for computing the memory allocation under the `average` memory usage profile.

You may get this memory size by any available mean, but [FaaSLoad's dataset generator mode](../generator) may prove useful for this task.

## Specifications

User specifications for one set of workload injection traces are written as a YAML file.

The root of the YAML content is a dictionary with the main key `users`.
Under this key is a dictionary mapping usernames to user specifications.

### User specification

A user specification is a dictionary that always includes the following keys:

* `memory-profile`: a string describing the memory usage profile of this user, this is any of these three values:
  * `maximum`: the memory allocation is the maximum allocation permitted by the FaaS platform (defined as a constant in the script _trace-builder.py_ as 3072 MiB),
  * `smart`: the memory allocation is exactly the amount specified in the manifest file for each action, which should be the smart amount that tightly fits any invocation without any safety margin,
  * `average`: the memory allocation is how much an average user of a FaaS platform would specify, which is 1.6 times the `smart` amount specified in the manifest file for each function (capped at the `maximum` memory amount);
* `nb-inputs`: a dictionary mapping input kinds (as found in the manifest file) to the number of available input files to invoke the user's functions, may be empty (but **it must exist**) if no action takes an input file;
* `actions`: either a dictionary describing how to select functions for the user, or an exact list of functions (see following sections).

The key `actions` [1] of a user specification may either give clauses to match functions that the user will invoke, or give an exact list of functions.
Both action specification types are detailed below.

`nb-inputs` must match available input data, see ["Preparing input data for the injector mode"](./input-data.md) for more information about preparing the input data for your functions.

[1] OpenWhisk, historically FaaSLoad's first supported platform, calls the functions "actions".

#### Functions specification: matching

The key `actions` of a user specification may be a dictionary with the two following keys:

* `number`: the total number of Functions to match for the user, functions are randomly sampled if more are selected by the match specifications (see below; it is an error to match fewer functions, so the sum of the `number` keys of match specifications must be greater or equal to this number);
* `matches`: the specifications to match functions (a list of dictionaries).

Each match specification in `matches` is a dictionary describing how to select a few functions and how to generate inter-arrival times for them.
It must contain the following keys:

* `number`: the number of functions to match for this match specification, functions are randomly sampled if more are selected (and it is an error to match fewer functions);
* `distribution`: the type of random distribution of inter-arrival times (IATs) for the matching functions, this is any of these string values:
  * `uniform`: a regular distribution, the functions are invoked at a fixed rate given by the key `rate` after an initial random wait time (different for each function),
  * `poisson`: an exponential distribution of IATs resulting in a number of invocations per second that follows a Poisson distribution;
* `rate`: the rate (as invocations per second) of invocations, fed to the chosen `distribution` of IATs;
* `match`: the set of match clauses, this is a dictionary with any combination of the following keys (see below to understand how they are interpreted):
  * `package`: match the package name (the manifest file may define several packages),
  * `name`: match the name of the function, as found in the keys of the manifest file,
  * `runtime`: match the runtime of the function, this is exactly the key found in the manifest file (if absent then the clause does not match),
  * `docker`: match the name of the Docker image of the function, this is exactly the key found in the manifest file (if absent then the clause does not match),
  * `annotations`: match the annotations of the function, all annotations given in the clause must be present and match (see below for details on matching annotations).

Optionally, the match specification may contain the following key:

* `activity-window`: the activity window of the matched functions, a dictionary with the keys `from` and `to` specifying the bounds (in seconds) of the activity window.

Match clauses are combined with the "and" operator, i.e., **they must all match for a function to be selected**.
They are evaluated as regular expressions when compared to the values found in the manifest file.
When matching annotations, keys are evaluated as fixed string (i.e., not as regular expressions), and values are evaluated as regular expressions.
Note that only plain string annotation values are supported; for example, matching the annotation `parameters` in the manifest file is not supported.

Note: a given function may be selected by multiple match specifications, in which case it will ultimately be selected only once at the scale of the user.
It is unspecified which match specification is considered for it in the end, which has an impact on the `activity-window`, `distribution` and `rate` that are used for this action.
Thus, **match specifications should select disjointed sets of actions**.

#### Functions specification: exact list

The key `actions` of a user specification may be a list of dictionaries specifying exact functions to invoke as well as their inter-arrival times.

Every function specification must contain the following key:

* `name`: the fullname of the function as defined by the FaaS platform, e.g. for OpenWhisk, its package name and its name joined by a slash `/`, as they are found in the manifest file.

Every function specification must also contain either the key `times`, or the couple of keys `distribution` and `rate`:

* `times`: the exact list of inter-arrival times (in seconds, from 0 or from the beginning of the activity window, see below);
* `distribution` and `rate`:
  * `distribution`: the type of random distribution of inter-arrival times (IATs) for the function, this is any of these string values:
    * `uniform`: a regular distribution, the function is invoked at a fixed rate given by the key `rate` after an initial random wait time (different for each function),
    * `poisson`: an exponential distribution of IATs resulting in a number of invocations per second that follows a Poisson distribution;
  * `rate`: the rate (as invocations per second) of invocations, fed to the chosen `distribution` of IATs;

Optionally, the function specification may contain the following key:

* `activity-window`: the activity window of the function, a dictionary with the keys `from` and `to` specifying the bounds (in seconds) of the activity window.
