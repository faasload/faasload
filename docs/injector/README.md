# Workload injector mode

In workload injector mode, FaaSLoad runs traces describing serverless functions invocations of many users in OpenWhisk, and monitors their execution time, and more.
It reads the traces, and uses the loader component to invoke actions in OpenWhisk;
the monitor can also be enabled to store resource usage and performance counters of function invocations, and logs of Docker operations.

Sections below explain:

1. how to configure FaaSLoad
2. how to get injection traces
3. how to prepare OpenWhisk
4. and finally how to run the injector

["Output database structure"](../dataset.md) gives a description of the resulting data as stored in the database.

See ["Troubleshooting"](../troubleshooting.md) if something fails.

To go further in understanding how FaaSLoad works, have a look at ["Architecture of FaaSLoad"](../dev/architecture.md).

## Configuration

You can find a complete configuration file that uses the default values at [_config/loader.yml_](../../config/loader.yml).
The loader is configured by the file at _~/.config/faasload/loader.yml_.

You are advised to use this complete file, so start by copying the default configuration file to this location:

```shell
mkdir -p ~/.config/faasload
cp config/loader.yml ~/.config/faasload/loader.yml`
```

Most configuration settings can be left to their default values.
This guide will tell you what settings are mandatory.

Alternatively, here is an example of a minimal configuration for FaaSLoad's loader targeting OpenWhisk, in injection mode with the monitor enabled (omitting the `logging` section):

```yaml
injector:
  tracedir: "injection-traces/sample"

database:
  host: localhost

# Specific to OpenWhisk
openwhisk:
  host: "localhost:31001"
  authkeys: "./users"
```

The default execution mode of FaaSLoad is the workload injector mode, so there is nothing to do to enable it.

It is mandatory to set the directory where FaaSLoad will read the injection traces under `injector:tracedir`.
This is must be directory where the _*.faas_ files are located: FaaSLoad does not descend into the subdirectories.

The use of FaaSLoad's monitor is enabled by default in the configuration.
So if you followed the instructions to run FaaSLoad's monitor in FaaSLoad's README at ["FaaSLoad's monitor"](../../README.md), then FaaSLoad will request performance data, resource usage and Docker operations logs when running the workload injection.
If you want to disable it (e.g., you are running on a kind cluster where the monitor is not available), set the configuration key `monitor:enabled` to `false`.

## Injection traces

The most important input to FaaSLoad's injector mode is workload injection traces.

First, you must write functions for the target FaaS platform that are compatible with FaaSLoad.
See the documentation specific to your platform in the [platforms documentation](../platforms).
You also need to write a manifest file to declare the functions for FaaSLoad, see ["FaaSLoad manifest of functions"](../manifest.md).

From those actions and their manifest, injection traces can be built using the script _trace-builder.py_, which creates synthetic traces from user specifications.
See ["User specifications to build workload injection traces"](trace-builder-user-specs.md) for the documentation on user specifications.
You can also find a sample user specification file at _injection-user-specs/sample.yml_, from which sample injection traces were built in _injection-traces/sample_ with a duration of 300s and the random seed `20191108`.

Example usage of _trace-builder.py_:

```shell
scripts/trace-builder /home/faasload/actions/manifest.yml\
        --user-specs /home/faasload/faasload/injection-user-specs/sample.yml\
        --duration 300 --random-seed 20191108
```

The usage of the script _trace-builder.py_ is as follows:

* the first argument is always the path to the manifest file describing the actions used by the injection traces;
* `--user-specs` is mandatory, and gives the path to the user specification file;
* `--duration` is mandatory, and specifies the maximum duration of the injection trace;
* `--random-seed` is optional, and specified a seed for the random number generator to make trace building reproducible.

The script writes traces in a folder named after the user specification filename (here, _sample_), in the folder _injection-traces_.

If you wish to skip using _trace-builder.py_, for example to convert existing traces to FaaSLoad's injection trace format, refer to ["Injection traces"](injection-traces.md) to read more about their format.

Finally, see ["Preparing input data for the injector mode"](./input-data.md) to prepare the input data to match your injection traces.

## Preparing the FaaS platform

The specifics depend on the platform.
The best integrated one for FaaSLoad is OpenWhisk.

In general, it involves writing functions for the platform that are compatible with FaaSLoad and their manifest (already done above), and preparing user accounts by loading them in the platform.
Again, see the documentation specific to your platform in the [platforms documentation](../platforms).

### Preparing OpenWhisk

Use the script _load-wsk-assets.py_ in injector mode with the flag `--injector`/`-i` to prepare OpenWhisk to inject your workload.
Refer to ["Assets setup"](../platforms/openwhisk/setup-assets.md) to understand what the script does.

Example usage of _load-wsk-assets.py_ with [FaaSLoad's example actions](https://gitlab.com/faasload/actions):

```shell
# Mind the path to the directory of actions.
scripts/load-wsk-assets ../actions/manifest.yml\
        --injector injection-traces/sample\
        --param-file ../actions/swift_parameters.json
```

The usage of the script _load-wsk-assets.py_ is as follows:

* the first argument is always the path to the manifest file describing the actions used by the injection traces;
* `--injector ...` tells the script to prepare OpenWhisk for workload injection, and specifies the directory containing the workload traces;
* `--param-file ...` is optional and gives the path to a file specifying parameters to bind to the OpenWhisk package of the actions deployed by the script, this is similar in usage to OpenWhisk's tool wskdeploy.

You need to know that the script wrote authentication tokens of OpenWhisk users to the folder _users_ (configuration key `openwhisk:authkeys`).

In case _load-wsk-assets.py_ fails, you will need to revert what has been successfully executed.
The solution is to revert all operations made by the previous run of the script.
This is achieved by **running again with the same arguments and the additional `--unload`/`-U` flag** to unload all assets.

Example usage of _load-wsk-assets.py_ to unload all assets:

```shell
# Mind the path to the directory of actions.
# Notice how the command is exactly the same as when loading the assets, except for the --unload flag.
scripts/load-wsk-assets ../actions/manifest.yml --unload\
        --injector injection-traces/sample\
        --param-file ../actions/swift_parameters.json
```

The script may display errors because it tries to unload everything even if it previously failed before the end, which is normal behavior.

You can now fix the initial problem and run _load-wsk-assets.py_ again.

## Execution

Run FaaSLoad's loader Python module:

```shell
# Assuming the pipenv virtualenv is activated.
python -m faasload.loader
```

Recall that the configuration for the loader is read from _~/.config/faasload/loader.yml_.

FaaSLoad will read workload traces from the directory specified at `injector:tracedir`.
Following the traces, it will invoke functions in the FaaS platform as requested, with the inputs and parameters given by the traces, and store invocation data to its database (e.g., activation ID, parameters…).
When an invocation is done, FaaSLoad will request full data (e.g., invocation duration, byte size of the output…) and complete the invocation data.
If the monitor is enabled, it will also store resource usage and performance counters, as well as logs of Docker operations.

Note that, in opposition to the dataset generator mode, **there is no checkpointing**: it makes no sense because the actual state of the FaaS platform would be lost anyway.

### Execution monitoring

Logging is based on the `logging` module of the Python standard library.
The configuration is given by the section `logging` in _loader.yml_;
refer to [the logging module documentation](https://docs.python.org/3/library/logging.html#module-logging) for more information.
By default, it is set to log to stderr.

For both the loader and the monitor, set `logging:root:level` to `DEBUG` to increase logging verbosity, or to `WARNING` to decrease it, from the default level of `INFO`.

### Data extraction and usage

FaaSLoad stores all the data in its PostgreSQL database server.
The data extraction process, the dataset format, and tips on how to exploit the data, are found in ["Output database structure"](../dataset.md).

### Execution restart

See ["Resetting FaaSLoad"](../resetting-faasload.md).

Depending on your experiment, you may want to restart the FaaS platform entirely to make sure it is "cold".
In that case, preparing it again (e.g., with `load-wsk-assets.py` for OpenWhisk) may be necessary.
Troubleshooting"](troubleshooting.md).
