# Preparing input data for the injector mode

_As of now, FaaSLoad is made to work with a [Swift](https://docs.openstack.org/swift/latest/) remote storage endpoint.
From there comes the identification of input files as couples of input container (a directory) and object filename._

An injection trace file typically only specifies an object filename as the input of an invocation, without any object container.
FaaSLoad considers that the name of the input object's container is the name of the user for this trace.
This allows to tailor input files for each user.

For example, for the following trace:

```
user001	dataset_gen/sharp_blur	1024
0.326	3.jpg	sigma:56.256303360401
2.75	5.jpg	sigma:39.14279952944534
0.061	1.jpg	sigma:72.80070853800676
```

The action _dataset_gen/sharp_blur_ will be invoked with the object container _user001_, and successively with the object filenames _3.jpg_, _5.jpg_ and _1.jpg_.

To prepare the input data, load to the object store, under the object container named after the username (e.g., _user001_), the input objects listed by the injection trace.
See [an example script](https://gitlab.com/faasload/actions/-/blob/master/load_sample_inputs_injection_swift.sh) in FaaSLoad's repository of sample OpenWhisk functions.