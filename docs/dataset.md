# Output database structure

FaaSLoad, whether in injector or generator mode, produces data in its database.
This document explains how to get the data, and describes how to use it.

## Data extraction

FaaSLoad stores all the data in its PostgreSQL database server.
To extract them, use PostgreSQL's `pg_dump` command to dump the tables to a file.

For example, using the default configuration from FaaSLoad:

```shell
# Will prompt for the user's password (`database:password` in loader.yml).
pg_dump -h localhost -U faasload faasload -f faasload.psql
```

Description (with configuration paths in "$HOME/.config/faasload/loader.yml"):

* `localhost`: host of the database server (`database:host`)
* `-U faasload`: user to connect to the database (`database:user`)
* `faasload`: name of the database (`database:database`)

Refer to the manual of pg_dump for more information.

## Overview of the produced data

Whether in injection or generation mode, the resulting dataset is stored in a database in 6 interlinked tables:

1. `runs`: records of the runs, the "main" table of the dataset;
2. `functions`: records of the functions run to generate the database (referring to actions in the FaaS platform via
   their names);
3. `results`: raw results returned by the function invocations;
4. `parameters`: parameters of function invocations in the runs;
5. `resources`: resource usage measurements of the runs;
6. `dockeroperations`: logs of Docker operations.

The tables `resources` and `dockeroperations` are filled with data from the Docker monitor component if enabled (the default).

## Exploitation

To explore the dataset, you can start with a run from the table `runs`.
A row includes the ID of the function (foreign key to `functions`) under the field `function`.
The field `namespace` gives the name of the user that ran the function.

Note that the function stored in the table `functions` is a generic representation not linked to the user that invoked
it.
You must request the FaaS platform to retrieve information about the actual function that was invoked.
For instance with OpenWhisk, run: `wsk -u "$(cat users/USER_NAME)" action get FUNCTION_NAME`.
See [the troubleshooting page for OpenWhisk](./platforms/openwhisk/troubleshooting.md) for more information.

You can get the values of the parameters passed to this function invocation by selecting rows in `parameters`, by
matching the run ID with the field `run`.
Same goes with the values of the results returned by the function invocations, by selecting rows in `results` and
matching the run ID with the field `run`.

If available (i.e., if the monitor was enabled) you can match the run ID with the field `run` in `resources` to select
rows of resource usage measurements for this invocation.
There are several rows for a given timestamp date, one per performance counter measured by the monitor.

Finally, if available (like the `resources` table), you can query the table `dockeroperations` to get events of Docker
operations.
The fields `address` and `port` identify the exact instance of monitor that reported the event, i.e., they can be used
to identify the Kubernetes
node where it happened.
The kind of the event is given by the field `op`, indicating if the container was created, destroyed, etc.

See below the list of tables and their fields, for more information.

## Tables

The fields of all the tables are detailed below. The formal table definitions can be found as SQL statements
named `CREATE_*` in [scripts/load-wsk-assets.py](../scripts/load-wsk-assets.py) for the `functions` table, and
in [loader/database](../faasload/loader/database.py) for the five other tables.

### Table `functions`

| Name       | Type                                    | Description                                               | Referenced by | Reference to |
|------------|-----------------------------------------|-----------------------------------------------------------|---------------|--------------|
| id         | integer                                 | index                                                     | runs.function |              |
| name       | string                                  | function name as declared in the FaaS platform            |               |              |
| runtime    | string                                  | runtime name as declared in the FaaS platform (see below) |               |              |
| image      | string                                  | name of the Docker image of the function (see below)      |               |              |
| parameters | string                                  | definition of function parameters (YAML string)           |               |              |
| input_kind | enum('audio', 'image', 'video', 'none') | kind of input                                             |               |              |

Notes:

* For OpenWhisk, the name of the function is matched by ignoring any namespace or package.
  This is because FaaSLoad generates and manages its own users (i.e., namespaces) and packages (
  via [the script load-wsk-assets.py](../scripts/load-wsk-assets.py).
* For OpenWhisk, `runtime` is the name of the function's generic runtime, or else `image` is the Docker image name if
  the function's runtime is "blackbox" (otherwise `image` is `NULL`).

### Table `runs`

| Name          | Type    | Description                                                                                             | Referenced by                              | Reference to |
|---------------|---------|---------------------------------------------------------------------------------------------------------|--------------------------------------------|--------------|
| id            | integer | index                                                                                                   | parameters.run, resources.run, results.run |              |
| function      | integer | function index                                                                                          |                                            | functions.id |
| namespace     | string  | user owner of the function                                                                              |                                            |              |
| activation_id | string  | internal activation ID in the FaaS platform                                                             |                                            |              |
| failed        | boolean | whether the run failed                                                                                  |                                            |              |
| start_ms      | integer | start date of the run as a timestamp in milliseconds                                                    |                                            |              |
| end_ms        | integer | end date of the run as a timestamp in milliseconds                                                      |                                            |              |
| wait_ms       | integer | wait time of the run between invocation and execution start in milliseconds                             |                                            |              |
| init_ms       | integer | initialization time of the runtime for the run in milliseconds if it was a cold start, `NULL` otherwise |                                            |              |
| error_msg     | string  | error message of the activation, may be `NULL`                                                          |                                            |              |

Note:

* `error_msg` is filled with the value of the optional field `error_msg` of the optional field `faasload` in the result
  dict of the function invocation, otherwise it is `NULL`.
  In other words, `error_msg` is a FaaSLoad-specific error message that can be emitted by a function.

### Table `parameters`

| Name  | Type    | Description                      | Referenced by | Reference to |
|-------|---------|----------------------------------|---------------|--------------|
| id    | integer | index                            |               |              |
| run   | integer | run index                        |               | runs.id      |
| name  | string  | name of the parameter            |               |              |
| value | string  | generated value of the parameter |               |              |

### Table `results`

| Name  | Type    | Description               | Referenced by | Reference to |
|-------|---------|---------------------------|---------------|--------------|
| id    | integer | index                     |               |              |
| run   | integer | run index                 |               | runs.id      |
| name  | string  | name of the result field  |               |              |
| value | string  | value of the result field |               |              |

### Table `resources`

| Name         | Type      | Description                                                 | Referenced by | Reference to |
|--------------|-----------|-------------------------------------------------------------|---------------|--------------|
| id           | integer   | index                                                       |               |              |
| run          | integer   | run index                                                   |               | runs.id      |
| timestamp_ms | timestamp | timestamp of the resource usage measurement in milliseconds |               |              |
| counter      | string    | name of the measured counter                                |               |              |
| value        | float     | value of the measured counter at this date                  |               |              |

Note:

* Values in `counter` directly reference the performance counters that can be listed in "
  $HOME/.config/faasload/monitor.yml" under `monitor:perf_counters`.
* The memory usage is always monitored, and figures under the counter name `memory`.

### Table `dockeroperations`

| Name         | Type                                  | Description                                                    | Referenced by | Reference to |
|--------------|---------------------------------------|----------------------------------------------------------------|---------------|--------------|
| id           | integer                               | index                                                          |               |
| address      | string                                | IP address of the monitor instance that reported the operation |               |
| port         | integer                               | port of the monitor instance that reported the operation       |               |
| timestamp_ms | integer                               | timestamp of the operation                                     |               |
| docker_name  | string                                | name of the Docker container subject of the operation          |               |
| op           | enum('RUN', 'RM', 'PAUSE', 'UNPAUSE') | kind of operation                                              |               |

Notes:

* The tuple of `address` and `port` should be used to identify a monitor instance, in case the instances are not exposed through their Kubernetes worker node's IP address, but through a cluster IP.
* Events for a specific Kubernetes worker node can be selected by matching a given tuple of `address` and `port`.
* `op` can be:
  * `RUN`: the container was created;
  * `RM`: the container was destroyed;
  * `PAUSE`: the container was paused by the FaaS platform to save on resources;
  * `UNPAUSE`: the container was resumed by the FaaS platform to serve an invocation.