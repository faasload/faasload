# FaaSLoad manifest of functions

FaaSLoad's helper scripts to build workload injection traces and load OpenWhisk assets, rely on manifest files to describe the actions.
Manifest files are specified by OpenWhisk for [their deployment tool wskdeploy](https://github.com/apache/openwhisk-wskdeploy), but FaaSLoad expects some additional metadata.
At the same time, it is parsed manually by FaaSLoad's scripts, so not all features are supported.
A commented sample of a manifest is given here: ["sample_manifest.yml"](./_res/sample_manifest.yml), and a complete working example can be found in the [repository of example actions for FaaSLoad](https://gitlab.com/faasload/actions).

## Actions' artifacts

Note that the manifest refers to actions' code, so **they must have been built** beforehand if they need to.
For example, run `make` in the repository of example actions for FaaSLoad mentioned just before to package NodeJS actions and built Python runtime Docker images.

## Memory size

One item of an action specification in the manifest is the "memory size".
It is interpreted by OpenWhisk as **the maximum memory allocation to an instance of the action**.

In FaaSLoad's generator mode, it is used as-is by the script "load-wsk-assets.py": this is the value given to OpenWhisk when creating the actions.

However, in injector mode when building injection traces with the script "trace-builder.py", this amount is interpreted differently depending on the memory usage profile of users in the user specifications.
In short, the amount should be **exactly the amount of memory that the action requires** in all cases, without any overprovisionning that adds a safety margin (the `smart` memory usage profile).
Refer to ["User specifications to build workload injection traces"](./injector/trace-builder-user-specs.md) (section "User specification") for full information about how the memory size is used.

## Input kind

Specifically for FaaSLoad, the manifest file **must specify the kind of inputs accepted by the action**, such as `image`, `audio`, or `none`.
Indeed, when building either synthetic workload injection traces for workload injector mode, or fake traces for the dataset generator mode, FaaSLoad must choose the appropriate kind of inputs to pass to the action.

From FaaSLoad's point of view, the value for the input kind is arbitrary, however in generator mode, it must map to a container in the storage: the value of the input kind is passed as the parameter `incont` to the actions (see "Common parameters" above).
In addition, there are maps from input kinds to file extensions in some places that you may need to adjust (before rebuilding FaaSLoad's Docker images):

* in "scripts/trace-builder.py": to build input filenames in injection traces
* in "faasload/loader/generation.py": to build input filenames in generation traces

Finally, the special input kind `none` is recognized by FaaSLoad to mean that the action does not use any input file from storage.
In this case, **the parameter `object` is not passed to the action** (however `incont` is always passed).

The input kind is specified in the manifest YAML file as a string under the annotation `input_kind`.

Example:

```yaml
annotations:
  input_kind: none
```

## Action parameters

Specifically for FaaSLoad, the manifest file **must describe action-specific parameters**.
Indeed, when building either synthetic workload injection traces for workload injector mode, or fake traces for the dataset generator mode, FaaSLoad must produce random but valid values for an action's parameters.

Parameters are described in the annotations of an action, under the key `parameters`.
When you build workload injection traces, the script "trace-builder.py" reads this annotation;
when you use FaaSLoad in dataset generator mode, it gets this annotation from its database, where it was stored by "load-wsk-assets.py".

In the manifest YAML file, `parameters` is a list of dictionaries, each describing one action-specific parameter.
Recall from above **the parameters `incont`, `object` and `outcont` are reserved by FaaSLoad**, and thus should not appear in the specification (for historical reasons, `object` may appear but will be ignored).

Any parameter dictionary must include the following fields (other fields are ignored, you may include a field `description` for documentation purpose):

* `name`: name of the parameter
* `type`: type of the parameter; any value of
    * `range_int`: the parameter is an integer in a range
    * `range_float`: the parameter is a float in a range
    * `ensemble`: the parameter can take any specified value

Depending on `type`, **other fields are expected**:

* for `range_int` and `range_float`: fields `min` and `max` (integers or floats) to specify the range of valid values (included)
* for `ensemble`: field `values`, the list of valid values to chose from

Example:

```yaml
annotations:
  parameters:
    - name: sigma
      type: range_float
      description: sigma blurring coefficient
      min: 0.3
      max: 100
    - name: width
      type: range_int
      description: target width
      min: 100
      max: 1920
    - name: format
      type: ensemble
      description: target format
      values: [webp, tiff, png]
```

Thus, parameter values are produced randomly (uniform distributions).
In injector mode, when building traces with "trace-builder.py", the random number generator (RNG) is initialized with the seed specified by the flag `--random-seed`/`-r`;
in generator mode, the RNG is initialized with the seed specified in "loader.yml" under `generation:seed`, and its RNG state is restored when restarting from a checkpointed state.
