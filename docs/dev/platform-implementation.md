# Implementing a new platform for FaaSLoad

This document explains how to write classes specific to a new FaaS platform to make FaaSLoad compatible, and then how to use them.

It is best to first read about [FaaSLoad's architecture](../dev/architecture.md), and particularly about the injector and the activation monitor, which are the two components with platform-dependent implementations.

You can also look at the examples of the OpenWhisk and the Knative implementations in `faasload.loader.injector` and `faasload.loader.activation_monitor`, as well as their specific architecture at ["Implemented platforms"](../dev/platforms.md) in the developer documentation.

## Platform-dependent classes

To implement a new FaaS platform for FaaSLoad, you need to write two platform-dependent classes:

1. `Injector`, from the module `faasload.loader.injector`;
2. `ActivationMonitor`, from the module `faasload.loader.activation_monitor`.

### Injector

An injector is a daisy chain of timer threads (see [FaaSLoad's architecture](../dev/architecture.md)) that invoke a function consecutively.

Your class should inherit from the abstract base class `faasload.loader.injector.Injector`, and implement the abstract method `_inject_one()`.
This method should **asynchronously** request **one invocation** of the injector's function, with the parameters provided as the argument `params` (a dict mapping parameters names to their values).
It is important that the method does not block (it may only wait for the acknowledgement of the invocation request from the FaaS platform).

It must return a tuple of:

1. the ID in the database, of the partial run record created for the invocation (see next);
2. the invocation ID returned by the FaaS platform.

You should get the first value of the return tuple (the ID of the partial record in the database) by calling the method `Injector._insert_partial_run()`, that creates this partial record and returns the record ID.
As a reminder, the injector creates a partial record of the running invocation, and then the activation monitor will complete this record when the invocation terminates.

The method may raise `faasload.loader.injector.InjectionException` to indicate that requesting the invocation failed.
It has also access to `self.logger`, a logger (see [the documentation about Python's logging module](https://docs.python.org/3/library/logging.html)) to output messages.

### Activation Monitor

An activation monitor is a thread that receives events of termination of function invocations, and:

* completes the invocation record in the database;
* fetches and stores resource usage and performance data to the database (handled by the base class);
* signals the main injector thread that the invocation is terminated.

Your class should inherit from the abstract base class `faasload.loader.activation_monitor.ActivationMonitor`, and implement the abstract method `_run()`.
The `ActivationMonitor` is a thread that runs a polling loop whose body is the method `_run()`.
**You should arrange for your implementation of `_run()` to return periodically, and not run as fast as possible**.
For instance, implement a timing pause before returning.
It is important to return so the activation monitor can check its termination flags and exit gracefully when all the injection is done.

The role of your `_run()` method is to eventually call `ActivationMonitor._update_db()` with a complete `Activation` instance (described in the next section) for every invocation that terminated during your polling round.
It will complete the partial database record of the invocation written by the injector (see above).

Fetching and storing resource usage and performance data to the database is already implemented in the base class: you do not need to do anything about it.

More importantly, **you must notify the main injector thread that the invocation is terminated**.
For each invocation that terminated in your polling round, send a message `faasload.loader.activation_monitor.ActivationMonitorNotification.ActivationDataFetched` in the notification queue to the injector, carrying the ID of the terminated invocation.

For example:

```python
self.notification_queue.put({
    'msg':        ActivationMonitorNotification.ActivationDataFetched,
    'activation': act_id,       # the ID of the invocation
})
```

Forgetting about the notification will result in the main injector thread stalling after all injections are done, indefinitely waiting for their termination.

## Activation

The class `Activation` defines an invocation in FaaSLoad [1].
It is independent from any platform: **do not implement a platform-specific `Activation` class** [2].
Just use the existing class.

This is the data structure that your `ActivationMonitor` should produce and store in the database using `ActivationMonitor._update_db()`.

Here is a description of its fields (you can find its declaration at [_faasload/loader/activation_monitor.py_](../../faasload/loader/activation_monitor.py)).
You are free to fill them with any information relevant to your FaaS platform.

* `activationId`: ID of the invocation (a string)
* `dockerId`: ID of the Docker container where the invocation was executed (a string)
  * this field is important: the method `ActivationMonitor._update_db()` uses this field to request from FaaSLoad's monitor the resource usage and performance data to store to the database, it must match the ID of the Docker container that was monitored by FaaSLoad's monitor.
* `namespace`: canonically, the name of the user that did the invocation (a string)
* `response`: a `faasload.loader.activation_monitor.ActivationResponse`, containing the result of the function executed in the invocation (can be anything), and a plain boolean that is `False` if the execution failed (as defined by the FaaS platform)
  * here, the execution failed if the FaaS platform was successful in running the function, but the function itself encountered a problem and failed to produce its result
* `start` and `end`: timestamps of the beginning and end of the invocation
  * the definition of the beginning and end of an invocation is platform-dependent (e.g. because of cold starts)
* `act_error_msg`: an optional error message directed by the function to FaaSLoad explicitely
  * this can be platform-dependant, but it canonically defined to be a field in the result of the function (e.g., the field `error_msg` of the dict under the field `faasload` in the result dict, see OpenWhisk's implementation of the activation monitor)
* `annotations`: a list of `faasload.loader.activation_monitor.ActivationResponse` (just dicts with a field `key` and a field `value`), this could be free-form, to store additional, platform-specific information, however **FaaSLoad will consider two annotations** in a special way if present:
  * the annotation with the key `waitTime` is the time spent in the FaaS platform before scheduling the function (independent of cold starts)
  * the annotation with the key `initTime` is the time spent initializing the invocation's runtime, i.e., the latency induced by a cold start
    * for a warm start, this annotation may not be present (or `None`), and this signals that the invocation was a warm start

[1] OpenWhisk, the first platform supported by FaaSLoad, call the invocations "activations".

[2] Admittedly, the class `Activation` is defined based on Openwhisk's definition of an invocation.

## Integration

This section details how to effectively use your platform-dependent classes and make FaaSLoad compatible with your platform.
It also shows how to configure FaaSLoad for your platform.

### Using the platform-dependent classes

The platform-dependent classes are selected in the startup sequence of FaaSLoad, [in the method `main()`](../../faasload/loader/__main__.py) of the loader.

#### Selecting the platform

FaaSLoad selects the platform by reading the environment variable `FAASLOAD_PLATFORM` [3].
When this variable is empty, FaaSLoad targets Apache OpenWhisk, because it was the first platform supported.
It also emits a warning about this behavior.

FaaSLoad "converts" the string in the environment variable `FAASLOAD_PLATFORM` into an member of the enumeration `FaaSPlatform` as its checks its validity.
So to use your platform, define a new member of the enum `FaaSPlatform` (located in the [init module of the loader](../../faasload/loader/__init__.py));
the value of the member is the string that will be expected in the environment variable `FAASLOAD_PLATFORM` to target your platform.
Do not forget to also fill the dict `FAASPLATFORM_PRETTY_NAMES` in the same module, to pair the enumeration member of your platform with a display name.

#### Initializing the platform-dependent classes

Coming back to the [main function of the loader](../../faasload/loader/__main__.py), FaaSLoad simply branches depending on the value of the variable `platform` that contains the enumeration member representing the targeted platform.

Simply write a new branch matching the enumeration member of your platform.
The code in this branch probably has to do three tasks:

1. read the platform-specific configuration (see next section);
2. instantiate your platform-dependent subclass of `ActivationMonitor`;
3. call the method `inject()`, passing to its parameter `injector_class` your platform-dependent subclass of `Injector`.

See the current examples for more details.

### Configuration

This section explains how to use custom configuration fields of FaaSLoad for your platform, in the loader's configuration file [loader.yml](../../config/loader.yml).

FaaSLoad's configuration structure is defined in the [init module of the loader](../../faasload/loader/__init__.py).

To define your platform's specific configuration:

1. define a new [`TypedDict`](https://docs.python.org/3/library/typing.html#typing.TypedDict) class for your platform, that lists the expected fields and their types
   * you can use sub-`TypedDict`s at your own discretion
2. define the default dict for your platform-specific configuration

The definition of the default dict (step 2) uses the library [configuration-reader](https://git.bacou.me/maba/configuration-reader) [4] to provide complex processing of the configuration, its validity and for filling in defaults.

Here is a primer on configuration-reader:

* the default dict must match the structure of your configuration: names of keys, structure of sub-elements (dicts, lists, plain keys...);
  * the types of the default dict's plain keys must match the expected types;
* to make a key mandatory and non empty, use `ConfigurationRequired.RequiredNonEmpty` as the default value;
  * to make it only mandatory, but may be empty (empty string or empty list of plain values), use `ConfigurationRequired.Required`,
* to specify default values for dict elements of lists (instead of specifying a default value for a list of plain values), use configuration-reader's class `ListDefault`;
* you can give a callable as the default value, to express complex custom behavior, see the docstring of configuration-reader's `read()` function;
  * for example, this can be used to express the dependency of a configuration key on another configuration key, or to pre-process a key to output a computed value.

See the examples of platform-specific configuration.
Also, see the [docstrings of configuration-reader](https://git.bacou.me/maba/configuration-reader/src/branch/master/configuration_reader.py) for more advanced configuration cases, including caveats on callable defaults and defaults for optional dicts.

Finally, to read the platform-specific configuration during the initialization of the loader (see above) from FaaSLoad's loader configuration file, you can use the following code:

```python
# Replace FaasloadOpenwhiskConfiguration and DEFAULTS_OPENWHISK by your platform configuration objects.
# read_configuration is the method to read and process the configuration from the library configuration-reader.
conf: FaasloadOpenwhiskConfiguration = read_configuration(
    os.path.expanduser('~/.config/faasload/loader.yml'),
    prepare_and_read_configuration, DEFAULTS_OPENWHISK,
    keep_unexpected_keys=True)
```

Replace `FaasloadOpenwhiskConfiguration` by the `TypedDict` of your platform-specific configuration, and `DEFAULTS_OPENWHISK` by the default dict of your platform (from the previous steps 1 and 2).

[3] The name of the environment variable that defines the platform targeted by FaaSLoad is actually defined by the global variable `FAASLOAD_PLATFORM_ENVAR` in the main module of the loader.

[4] An independent code project by FaaSLoad's main author.
