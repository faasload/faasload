# Architecture of FaaSLoad

This document gives pointers to the developer, to understand how FaaSLoad runs.
For a high-level overview, see the project's [README](../README.md).

## Detailed architecture diagram

Find below a detailed architecture diagram, similar to the one from the project's README, but showing the internals of FaaSLoad's trace loader and Docker monitor components, and how FaaSLoad interfaces with the FaaS platform.

![Detailed architecture](../_fig/detailed-architecture.png)

It can be seen that FaaSLoad has weak ties to the actual FaaS platform, because it uses its public facing API, or generic lower-level Docker interfaces and cgroups.

## Architecture of the loader (workload injector)

The loader is the entry point to FaaSLoad.
It reads traces that describe the workload to inject.
In the dataset generator mode, it builds workload traces according to its configuration.

### Injector threads

For each trace, it spawns an **injector thread** that invokes multiple times one function under one OpenWhisk user, following the loaded trace.
This is done so that each thread keeps its own timing to invoke its function.
Actually, an injector thread is a daisy chain of [Python Timer threads](https://docs.python.org/3/library/threading.html#threading.Timer): when an invocation is
triggered, it also schedules the next invocation in the trace.
So, starting an injector simply schedules the first invocation.
From the outside, the chain of an injector's timers can be seen as an injector thread.

The timers in the thread share some state:

* the injection trace
* the injector's progress state (invocation ID in the trace, and state of the injector, e.g., error, terminated, etc.)

The timer threads also share a connection to the database, as well as the notification queue to notify the main injection thread.

The actual implementation of the injector thread is platform-dependent.

### Waiting for termination

The **main loader thread** blocks until all activations are terminated.
To do so:

1. injector threads send the main thread activation IDs as their function is invoked
2. the main thread blocks until it receives notifications of completion for all activations from the **activation monitor**

The activation monitor [1] is a thread used to handle the asynchronicity of the injection: as the injector schedule
invocations of functions, **the activation monitor handles terminations of invocations**.
**The main injection thread reconciles the notifications from both sides to wait for the termination of the injection**.

Moreover, when an invocation terminates, the activation monitor fetches the invocation's information from the FaaS
platform.
Injector threads write partial records of invocation data to FaaSLoad's database.
When an invocation terminates, the activation monitor **completes the partial invocation record** in the database, with
the activation data from the FaaS platform.
Finally, the activation monitor also fetches the performance data from FaaSLoad's monitor if enabled, and stores it to
the database.

The actual implementation of the activation monitor is platform-dependent.

[1] The name "activation" is how OpenWhisk (historically the first platform supported by FaaSLoad) calls invocations.

#### Code

The loader lives in the package `faasload.loader`.
It is an executable Python module, with a _\_\_main\_\_.py_ file, and the entrypoint to run FaaSLoad.

The main loader thread runs the method `inject()`.
The abstract base class for injectors is `Injector`, in `faasload.loader.injector`.
Platform-specific implementations are also in this module.

The abstract base class for the activation monitor is `ActivationMonitor`, in in `faasload.loader.activation_monitor`.
Platform-specific implementations are also in this module.

## Architecture of the Docker monitor

When the Docker Monitor is executed, its main process spawns three threads:

1. the measurements server
2. the Docker operations log server
3. the Docker operations monitor

Note that one monitor is spawned on every Kubernetes node where the FaaS platform is deployed.

The **measurements server** can be queried for measurements of a given Docker container during a specified time slice;
this is what the loader's activation monitor does after each termination of invocation.

The **Docker operations log server** is similar: it can be queried for logs of Docker operations (container creation, destruction, etc.) during a specified time slice.
This is done by the loader's activation monitor once, when it terminates (i.e., when all activations have terminated).

The **Docker operations monitor** monitors events of container operations from the FaaS platform.
The server acts upon them by creating, destroying, etc. **monitor threads**, one for each Docker container.
A monitor thread periodically stores measurements of its container's resource usage directly in a shared structure owned by the **measurements server**.
Actually, a monitor thread is two threads: one runs the perf command to get performance counters, and the other one runs a polling loop to read the memory usage of the container.

##### Code

The monitor lives in the package `faasload.docker_monitor`.
It is an executable Python module, with a _\_\_main\_\_.py_ file.

There, the main process spawns the measurements and Docker operations log servers (TCP servers from Python's standard library).
They are implemented respectively by the class `MeasurementsServer` in the module `faasload.docker_monitor.measurements_server` (the interesting code is actually in the class `MeasurementsQueryHandler` in the same module), and by the class `DockerOperationsLogServer` in the module `faasload.docker_monitor.docker_operations` (the interesting code is actually in the class `DockerOperationsLogQueryHandler`).

The Docker operations monitor is implemented by the class `DockerOperationsHandler` in the module `faasload.docker_monitor.docker_operations`.
It runs indefinitely when run from its method `handle_docker_operations()`.

Monitor threads are implements by the class `DockerMonitor` in the module `faasload.docker_momnitor.monitor`.
They actually wrap two threads: the first one is implemented by the class `PerfThread` (an actual thread that runs the command `perf`), and the second one is implemented by the class `MemoryThread` (a daisy chain of timer threads, similar to injector threads, that read memory usage from the container's cgroup).

## Interactions between the loader and the Docker monitor

There are two parts to FaaSLoad: the loader, that loads workload traces to invoke functions, and the Docker monitor that monitors Docker containers of running functions.

**The central component is the loader**, because it sends invocations requests;
but also because it is the one requesting performance data of the functions, and Docker operations logs from the Docker monitor, and it is the one that stores them to FaaSLoad's database.
In this picture, the loader sees the monitor as a server for measurements collected from Docker containers.
The monitor always monitors OpenWhisk's containers, and is actually controlled by "notifications" received from OpenWhisk's invoker component.
As a reminder, there is actually one Docker monitor per Kubernetes node: the loader simply iterates through all of them according to its configuration, when it needs performance data and Docker operations logs. 

As such, the Docker monitor is independent of FaaSLoad and its workload trace loading feature.