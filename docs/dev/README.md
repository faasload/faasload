# Development guide to FaaSLoad

This is a guide for developers to work on FaaSLoad in general.
There are also tips to work only on the Docker monitor.

See ["Implementing a new platform for FaaSLoad"](./platform-implementation.md) to implement a new platform for FaaSLoad;
the document ["Implemented platforms"](./platforms.md) explains how the existing platforms are implemented.

## Generalities

FaaSLoad injects a workload to a FaaS platform, and it is strongly advised to **start with learning about the platform itself** before burdening yourself with FaaSLoad's components and deployment process:

* [learn OpenWhisk](https://github.com/apache/openwhisk)
* [learn Knative](https://knative.dev/docs/)

FaaSLoad is written in Python and managed using [pipenv](https://pipenv.pypa.io/en/latest/), so a good understanding of this tool is preferable, although its day-to-day usage is not that hard and its learning curve, smooth.

In addition, FaaS platforms and FaaSLoad's monitor component are deployed using [Kubernetes](https://kubernetes.io/), so here again, a good understanding of this tool is preferable.

Finally, you should get familiar with FaaSLoad's architecture and components as presented in the [main README](../README.md);
you may also want to have a look at the more detailed ["Architecture"](architecture.md) document.

## Deploy development environment

_If you only intend to work on the monitor, skip below to "Working on the monitor"._

As a development and test environment, install and deploy FaaSLoad in generator mode: follow the section ["Installation and usage"](../README.md) of the main README, and then ["Dataset generator mode"](../generator/README.md).

As for sample actions to use in generator mode, see next.

## Test deployment

Two repositories of actions developed to use with FaaSLoad are provided:

* for OpenWhisk: ["OpenWhisk actions for FaaSLoad"](https://gitlab.com/faasload/actions)
* for Knative: ["Knative actions for FaaSLoad"](https://gitlab.com/faasload/actions-knative)

Follow either README to deploy the actions for FaaSLoad in generator mode, but in essence, you have to do the following steps:

```shell
# Clone the appropriate repository.
git clone URL
pushd actions*

# Build all actions.
make

# Deploy Swift remote storage, locally.
docker run --name swift\
    --detach --tty\
    --env SWIFT_USERNAME=test:tester --env SWIFT_KEY=testing
    --publish 12345:8080\
    --volume swift_storage:/srv\
    fnndsc/docker-swift-onlyone
# Load the remote storage with sample input files.
# Requires the commands swift (the Swift client: https://docs.openstack.org/python-swiftclient/latest/index.html), jq.
./load_sample_inputs_swift.sh

popd

# For OpenWhisk:
# Load actions to FaaSLoad's database and to OpenWhisk to be used in generator mode.
# You may want to use a shortened manifest file.
# Don't forget to enable the Python virtual environment if you didn't yet (pipenv shell).
scripts/load-wsk-assets actions/manifest.yml --generator --param-file /home/faasload/actions/swift_parameters.json
```

There are a lot of actions in the repository of OpenWhisk actions, so **it is strongly suggested to only deploy a few of them**: edit the manifest file _actions/manifest.yml_ to remove actions before loading them to FaaSLoad (last step above).

## Test usage

At this point, you can start FaaSLoad in generator mode.

You must set `generator:enabled` to `true` to enable the generation mode, and fill in `generator:nbinputs` with the number of inputs of each kind, loaded by _load_sample_inputs_swift.sh_ previously:

```yaml
nbinputs:
  image: 10
  audio: 10
  video: 10
  none: 10
```

You may want to set the loader's and monitor's logging level to `DEBUG`, respectively in _loader.yml_ and _monitor.yml_.

## Working on the monitor

To work on the Docker monitor component of FaaSLoad, you don't need to setup FaaSLoad in generator mode.
Simply setup the monitor, as instructed in ["Dataset generator mode: Monitor"](../generator/README.md).

Now, you can invoke actions manually using `wsk` and the monitor will gather measurements, even if not registered with FaaSLoad.

The section ["Docker monitor: Querying measurements"](../monitor.md), explains how you can use the script _docker-monitor-client.py_ to read measurements gathered by the monitor.

In addition, you can read the monitor's logs with `kubectl -n faasload logs monitor-XXX`, with `XXX` replaced by the ID of the monitor's pod for which you want to read the logs.