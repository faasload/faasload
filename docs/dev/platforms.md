# Implemented platforms

This documents how platforms are implemented as targets for FaaSLoad.

For a guide on how to implement a new platform, see ["Implementing a new platform for FaaSLoad"](./platform-implementation.md).
Use the implemented platforms as reference examples.

## Openwhisk

This is the first platform supported by FaaSLoad, and most of the internal architecture reflects this fact.

FaaSLoad uses the public interfaces of OpenWhisk to work, so there isn't a lot of platform-dependent mechanisms:

* the activation monitor regularly polls the list of activations of every user to get the notifications of termination
  * it gets the ID of the Docker container of an activation, by parsing the logs of the pod of the instance 0 of OpenWhisk's invoker
    * it means **FaaSLoad does not work with more than one invoker instance**
* the injector uses the public REST API to activate an action
  * if the activation fails because of an application error ("developer error"), it tries to fetch and display the activation logs as a warning

Most of the platform-dependent machinery is implemented in the library [pyWhisk](https://gitlab.com/Thrar/pywhisk).

## Knative

This platform relies more on Kubernetes native services (in comparison with OpenWhisk and its many custom components).

* the activation monitor regularly polls the list of invocations of every user to get the notifications of termination
  * it gets the ID of the Docker container by identifying the user container in the Knative platform
  * it uses the optional logging component Jaeger
* the injector uses the public REST API to invoke a function
  * it controls the activation ID to trace it through the platform and find the invocation data afterwards in the activation monitor

Most of the platform-dependent machinery is implemented in the library [pyKnative](https://gitlab.com/faasload/pyknative).