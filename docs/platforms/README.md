# Platforms supported by OpenWhisk

This is the platform-specific documentation.

For developers that want to learn how to implement a new platform for FaaSLoad, see ["Implementing a new platform for FaaSLoad"](../dev/platform-implementation.md).

The currently supported platforms are:

* [Apache OpenWhisk](./openwhisk) (link to the documentation)
* Knative (no specific documentation)

For OpenWhisk, this documentation describes [how to write functions](./openwhisk/functions.md) for this platform to use with OpenWhisk, and gives [tips to troubleshoot OpenWhisk](./openwhisk/troubleshooting.md) when running with FaaSLoad.