# Troubleshooting

If FaaSLoad runs fine, but fails activating actions, it may be related to either a non-working OpenWhisk installation, or to non-working actions.

## Methods

### Testing actions

To test how FaaSLoad connects to OpenWhisk, in order to invoke actions for example, you must provide the `wsk` command with the authentication token of the user, using its flag `-u`:

```shell
# Most OpenWhisk deployments use self-signed certificates, so accept insecure connection.
wsk --insecure --auth "$(cat users/USER_TOKEN)" action invoke ACTION --param ...
```

### Getting logs

Similarly, you can get the activations done under a specific user with:

```shell
wsk --insecure --auth "$(cat users/USER_TOKEN)" activation list
```

And to get an activation's logs:

```shell
wsk --insecure --auth "$(cat users/USER_TOKEN)" activation logs ACTIVATION_ID
```

## Problems

### Invocation failure because "the server is under maintenance"

**Solution:** lower the activation execution time, fix the action.

When there are too many activations going at once, and OpenWhisk cannot accept anymore, it will fail with an indication suggesting that "the server is under maintenance".

The solution os to make the actions execute faster, so that the nodes can handle the node by having resources freed quicker.
Sometimes, it also comes from the actions failing or timing out, thus not freeing runtimes to serve the incoming activations.

You could also scale the OpenWhisk platform by deploying more invokers, however **FaaSLoad does not work with multiple invokers** (see ["Implemented platforms: OpenWhisk"](../../dev/platforms.md)).

### FaaSLoad gets "unauthorized access" errors from the activation monitor

**Solution:** delete the irrelevant authentication tokens in _users/_.

The activation monitor works by polling activations for **all users for which it has an authentication token**.
Authentication tokens are read from all the files in the directory defined by the configuration at `openwhisk:authkeys`.

It means that if you have leftover authentication tokens in this directory, the activation monitor will try to monitor them even if they are not relevant to the current execution.
This is not harmful in general, but if OpenWhisk has been reset since the authentication token were generated, then the latter are now invalid, thus the error.

The solution is to delete the irrelevant authentication tokens in the directory specified by `openwhisk:authkeys`.