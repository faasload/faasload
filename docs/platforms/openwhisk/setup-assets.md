# Assets setup

_This is a low-level documentation only relevant to developers: users should use the script_ load-wsk-assets.py _to set up OpenWhisk (refer to the documentation appropriate to the injector or generator mode)._

This document explains **how to set OpenWhisk up** with relation to the traces.
The document ["Preparing input data for the generator mode"](../../generator/input-data.md) explains how input data should be loaded predictably to conform to FaaSLoad's generation traces;
and ["Preparing input data for the injector mode"](../../injector/input-data.md) explains the same topic for FaaSLoad's workload injection traces.

The steps described below are implemented by the script _load-wsk-assets.py_.

There are three main steps:

1. load actions to FaaSLoad's database
2. create OpenWhisk users to invoke actions
3. create actions in OpenWhisk under each user

The first step is to store data about the actions in FaaSLoad's database for its usage.
It can be summarized as reading the manifest file (see ["FaaSLoad manifest of functions"](../../manifest.md)) and storing the most important information about the actions.
It is written to the database table `functions`: this is the base for building generation traces (see the function `faasload.loader.generation.build_traces()`).

The second step is to set OpenWhisk up by creating the users to invoke the actions.
Users should have predictable names of the form `user-ACTION`, with `ACTION` being the basename of the action (its name without any package).
An OpenWhisk user can be created using wskadmin, an OpenWhisk executable (located in the same directory as its main command wsk):

```shell
wskadmin user create USER_NAME > users/USER_NAME
```

This command creates the user called `USER_NAME`, and writes its authentication token (i.e., authentication information required to execute OpenWhisk operations as this user) to _users/`USER_NAME`_.
FaaSLoad uses this token when invoking the user's actions: set the configuration setting `openwhisk:authkeys` in _loader.yml_ to the directory which contains the tokens (_users_ in the command above).

The third and last step, is to set OpenWhisk up by loading the actions under each user.
This entails calling `wsk` with the authentication token of the corresponding user through its flag `--auth`/`-u`, to create the packages and load the actions one by one:

```shell
# You may want to add global parameters here, such as how to reach the data store.
wsk -u $(cat users/USER_NAME) package create ...
# Recall that the memory amount given by an injection trace must be set at action creation.
wsk -u $(cat users/USER_NAME) action create ... --memory MEMORY
```

About packages: **in dataset generation mode, FaaSLoad ignores packages**.
While OpenWhisk's manifest files require to have one, FaaSLoad selects actions in its database by basename only.
FaaSLoad retrieves fullnames thanks to the actions' fullnames being stored in the database.