# Docker monitor

The Docker monitor component is an independent service that continuously monitors Docker containers used by the FaaS platform to execute the cloud functions.

One instance of the monitor runs on every Kubernetes worker node where the FaaS platform can schedule cloud functions.

Its usage as part of FaaSLoad is described in the [repository's README](../README.md).

## Architecture

The monitor's architecture is mainly described in its [_\_\_init\_\_.py_](../faasload/docker_monitor/__init__.py) file.

In summary, the monitor has four components:

1. a notification server, that receives notifications of Docker container-related operations from the FaaS platform;
2. monitor threads, that are spawned by the notification server to monitor the resources and performance counters of new containers;
3. a measurements server, that serves the monitor's data, i.e., that clients can request to get the resources and performance counters of a container;
4. a Docker operations log server, similar to the measurements server, but that serves logs of Docker operations (creation, destruction, etc.).

## Configuration

### perf counters

The monitor relies on [perf](https://perf.wiki.kernel.org/index.php/Main_Page) to collect various performance counters (excluding memory usage, that is measured using control groups via [pycgroup](https://gitlab.com/Thrar/pycgroupv2)).
The monitored counters are configured in "monitor.yml" under `monitor:perf_counters`.
This field expects a list of performance counters;
the complete list of available fields is listed by `perf list`.

For example:

```yaml
monitor:
  perf_counters:
    - cpu-clock
    - cache-misses
    - context-switches
```

### Measurements and Docker operations log server

A monitor instance listens to measurements queries on the IP address and port configured in "monitor.yml" under `measurementserver`.
For a standard deployment using the automated script from [FaaSLoad's repository of platform configurations](https://gitlab.com/faasload/platform-configurations), this should not be changed.

On the other side, FaaSLoad's loader (or any client) will connect to the monitor instance through the IP address and port of the monitor instance's pod exposed by Kubernetes.

The same information apply to the Docker operations log server, except it is configured under `dockeropslogserver`;
obviously, it should use a port different from the measurements server's one.

## Querying measurements

### Configuration to reach the measurements servers

During normal FaaSLoad operations, the loader requests measurements from the monitor.
For the loader, measurements servers are configured in "$HOME/.config/faasload/loader.yml" under `dockermonitor:measurementservers`, which is a list of objects specifying the addresses and ports of all measurements server instances that should be queried.
Note that this list should indicate addresses and ports under which the measurements servers are exposed by the Kubernetes cluster;
by default, the deployment script uses the same ports, and the addresses should be the Kubernetes worker nodes' ones.
This is because monitor pods are exposed using NodePorts.

### Requesting measurements from the server

The details can be found in the docstring the class [`MeasurementsQueryHandler` of the Docker monitor module](../faasload/docker_monitor/measurements_server.py).

In summary, the body of the TCP message should look like the following JSON object:

```json
{
  "container": CONTAINER_ID,
  "start": INVOCATION_START,
  "end": INVOCATION_END
}
```

CONTAINER_ID is the container ID, and INVOCATION_START and INVOCATION_END are the start and end timestamps (in seconds, precision up to the millisecond) of the invocation.

In other words, the server will return all the measurements about the specified container between those two dates.

The response is a JSON object like the following:

```json
{
    "error": ERROR_MSG,
    "measurements": MEASUREMENTS
}
```

ERROR_MSG may be an error message describing the error if the server cannot fulfil the query.
The key is always present, but may be `None`, indicating that no error occurred.

Then MEASUREMENTS is a JSON list of objects, each object being one measurement record (instances of the class
`:py:class:Measurement`, converted as dicts) like the following:

```json
{
    "date": TIMESTAMP,
    "counter": COUNTER,
    "value": VALUE
}
```

where TIMESTAMP is the absolute date of the measurement (in seconds, rounded to the millisecond), COUNTER is the name of the counter, and VALUE its value at this date.

As an example and for testing purposes, the script ["docker-monitor-client.py](../scripts/docker-monitor-client.py) can be used to send measurement requests to the server.
For instance, assuming the Docker monitor is running, for an OpenWhisk deployment:

```shell
# Activate an action (without the flag -u, this uses the default configured user "_").
wsk action invoke [...]
# Grab the activation ID (run this command twice to have the latest activation, this is an OpenWhisk bug)
wsk activation get -l
# Fetch the measurements (assuming the default configured user "_").
scripts/docker-monitor-client.py measurements _ ACTIVATIONID
# Fetch the log of Docker operations
scripts/docker-monitor-client.py operationslog
```

## Querying logs of Docker operations

### Configuration to reach the measurements servers

This is similar to the measurements server (see above), except it is configured under `dockermonitor:dockeropslogservers`.

### Requesting Docker operations log from the server

The details can be found in the docstring the class [`MeasurementsQueryHandler` of the Docker monitor module](../faasload/docker_monitor/measurements_server.py).
Requests and responses work similarly as for the measurements server (but with a different format of course).

A notable caveat, is that beside all events between the queried start and end, the response also includes events that happened before the start, about containers that have events in the queried timeslice.
This is to help in counting the current number of containers at a given time.
For example, if a container has any event between the start and  the end, then all its creation, pause and resume events before the start are also included.