Contributions to FaaSLoad are happily accepted.
They can take the form of:

* issues
  * about bugs, documentation improvements, features
* merge requests
  * it is probably a good idea to open an issue before, to submit your
    contribution to discussion
* citations (FaaSLoad has not been published yet, 2022/02)
  * please cite FaaSLoad in your papers or projects when you use it!
* messages to discuss freely about FaaSLoad and its goals
  * you can also invite me to talk about FaaSLoad ;)

For motivated developers, please have a look at the documentation:
"docs/develop.md".